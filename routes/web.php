<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CRM\RoutesController;
use App\Http\Controllers\CRM\SignController;
use App\Http\Controllers\CRM\TestController;
use App\Http\Controllers\CRM\CalendarController;
use App\Http\Controllers\CRM\ProfileController;
use App\Http\Controllers\CRM\LeadsController;

use App\Http\Controllers\WEB\MailController;
use App\Http\Controllers\WEB\LoginController;
use App\Http\Controllers\WEB\ProfileController as WebProfileController;
use App\Http\Controllers\WEB\RegisterController;
use App\Http\Controllers\WEB\VerifiedController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



    Route::get("", function () {
        return view("web.index");
    });
   Route::get("/home", function () {
      return view("web.home");
 })->name('home');

Route::get('/home',[App\Http\Controllers\WEB\HomeController::class,'home'])->name('home');
Route::get('/entreprise', [\App\Http\Controllers\WEB\HomeController::class, 'Entreprise'])->name('Entreprise');
Route::get('/agence', [\App\Http\Controllers\WEB\HomeController::class, 'Agence'])->name('Agence');
Route::get('/collectivites', [\App\Http\Controllers\WEB\HomeController::class, 'Collectivites'])->name('Collectivites');

Route::get('/actuality', [\App\Http\Controllers\ArticleController::class, 'getarticles'])->name('actuality');

    Route::get("agenda", function () {
        return view("web.agenda");
    })->name('agenda');

    Route::get("calendar", function () {
        return view("web.calendar");
    })->name('calendar');

    Route::get("contact", function () {
        return view("web.contact");
    })->name('contact');

Route::get("about", function () {
    return view('web.about_us');
})->name('about');

    Route::get("connexion", [LoginController::class,"login_page"])->name('connexion');
    Route::post("connexion_trait", [LoginController::class,'trait_login'])->name('connexion_trait');

    Route::get("registre", [RegisterController::class,'reg_page'])->name('registre');
    Route::post("registre_trait", [RegisterController::class,'trait_reg'])->name('registre_trait');

    Route::get('subevention', function () {
        return view('web.subvention');
    })->name('subevention');

    Route::get('login/google', [LoginController::class, 'redirectToGoogle'])->name('login.google');
    Route::get('login/google/callback', [LoginController::class, 'handleGoogleCallback']);


    Route::get('login/facebook', [LoginController::class, 'redirectToFacebook'])->name('login.facebook');
    Route::get('login/facebook/callback', [LoginController::class, 'handleFacebookCallback']);


    Route::get('login/linkedin', [LoginController::class, 'redirectToLinkedin'])->name('login.linkedin');
    Route::get('login/linkedin/callback', [LoginController::class, 'handleLinkedinCallback']);

Route::get('profile',[\App\Http\Controllers\WEB\ProfileController::class,'profile_page'])->name('profile');


Route::get('/edit_profile',[\App\Http\Controllers\WEB\ProfileController::class,'edit_profile_page']);
Route::post('updt_profile',[\App\Http\Controllers\WEB\ProfileController::class,'updt_profile'])->name('updt_profile');

    Route::get('testemail',[VerifiedController::class,"sendSignupEmail"]);

    Route::get('deconnexion', [\App\Http\Controllers\WEB\ProfileController::class, 'deconnexion'])->name('déconnexion');

    Route::get('a_propos', function () {
        return view('web.about_us');
    })->name('a_propos');

    Route::get('map', function () {
        return view('web.map');
    });

    Route::get('pop',function ()
    {
        return view('web.popup');
    });

    Route::get('mot_passe_oubliee',function (){
        return view('web.forgot_p');
    });

    Route::prefix("mail")->group(function(){
        Route::get("confirm/{token}/{cid}", [MailController::class,"confirmMeet"]);
        Route::get("change/{token}", [MailController::class,"changeMeet"]);
        Route::get("change/events/all", [MailController::class,"getCalendarEvents"]);
        Route::post("change/events/update", [MailController::class,"updateCalendarEvent"]);
    });


Route::get('teste_eligibilité',function (){
    return view('web.tester_eligiblilité');
})->name('Tester_votre_eligibilité');



    // Route::any('/{any}', [App\Http\Controllers\CRM\FrontCrmController::class, 'routesFrontCrm'])->where('any', '^(?!api).*$');


Route::get("Normandie", [\App\Http\Controllers\RegionController::class, "Normandie_page"])->name('Normandie');
