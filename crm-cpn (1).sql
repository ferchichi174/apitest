-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2021 at 10:21 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crm-cpn`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `name`, `description`) VALUES
(1, 'Agriculture', NULL),
(2, 'Agroalimentaire', NULL),
(3, 'Ameublement', NULL),
(4, 'Art', NULL),
(5, 'Assurance', NULL),
(6, 'Automobile', NULL),
(7, 'Banque', NULL),
(8, 'Batiment', NULL),
(9, 'Bois', NULL),
(10, 'BTP', NULL),
(11, 'Carton', NULL),
(12, 'Chimie', NULL),
(13, 'Commerce', NULL),
(14, 'Communication', NULL),
(15, 'Distribution', NULL),
(16, 'Economie Sociale', NULL),
(17, 'Edition', NULL),
(18, 'Electricité', NULL),
(19, 'Electronique', NULL),
(20, 'Etudes et conseils', NULL),
(21, 'Fabrication', NULL),
(22, 'Habillement / Chaussure', NULL),
(23, 'Habitat social', NULL),
(24, 'Hotellerie', NULL),
(25, 'Imprimerie', NULL),
(26, 'Industrie', NULL),
(27, 'Industrie pharmaceutique', NULL),
(28, 'Informatique', NULL),
(29, 'Logistique', NULL),
(30, 'Machines et équipements', NULL),
(31, 'Magasins', NULL),
(32, 'Matériaux de construction', NULL),
(33, 'Métallurgie', NULL),
(34, 'Multimédia', NULL),
(35, 'Mutualité', NULL),
(36, 'Négoce', NULL),
(37, 'Papier', NULL),
(38, 'Parachimie', NULL),
(39, 'Plastique / Caoutchouc', NULL),
(40, 'Presse', NULL),
(41, 'Productions', NULL),
(42, 'Propreté', NULL),
(43, 'Protection sociale', NULL),
(44, 'Restauration', NULL),
(45, 'Santé', NULL),
(46, 'Services aux entreprises', NULL),
(47, 'Télécoms', NULL),
(48, 'Textile', NULL),
(49, 'Transports', NULL),
(50, 'Travail du métal', NULL),
(51, 'Coiffure, instituts de beauté', NULL),
(52, 'Construction', NULL),
(53, 'Sport', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `activities_categories`
--

CREATE TABLE `activities_categories` (
  `id` int(11) NOT NULL,
  `activities_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `desc` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `contacts_id` int(11) DEFAULT NULL,
  `companies_id` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `users_id`, `contacts_id`, `companies_id`, `address`, `region`, `city`, `zipcode`, `department`, `country`) VALUES
(7, 3, 7, NULL, 'undefined Rue de Paris', 'Hauts-de-France', 'Compiègne', '60200', 'Oise', 'France'),
(9, 5, 9, NULL, '78 Route de Lens', 'Hauts-de-France', 'Loison-sous-Lens', '62680', 'Pas-de-Calais', 'France'),
(10, 6, 10, NULL, '11 Rue du Chêne', 'Ile-de-France', 'Combs-la-Ville', '77380', 'Seine-et-Marne', 'France'),
(12, 4, 12, NULL, 'undefined Rue Blaise Pascal', 'Nouvelle-Aquitaine', 'Tours', '33127', 'Gironde', 'France'),
(13, 3, 13, NULL, 'undefined Rue Jean Joseph Etienne Lenoir', 'Hauts-de-France', 'Bruay-la-Buissière', '62700', 'Pas-de-Calais', 'France'),
(14, 6, 14, NULL, '35 Rue du Château Fort', 'Ile-de-France', 'Lagny-sur-Marne', '77400', 'Seine-et-Marne', 'France'),
(15, 3, 15, NULL, 'undefined Rue Victor Hugo', 'Hauts-de-France', 'Albert', '80300', 'Somme', 'France'),
(16, 5, 16, NULL, '2 Avenue de Fontainebleau', 'Ile-de-France', 'Le Kremlin-Bicêtre', '77930', 'Seine-et-Marne', 'France'),
(19, 3, 19, NULL, 'undefined Rue François Courtin', 'Hauts-de-France', 'Liévin', '62800', 'Pas-de-Calais', 'France'),
(20, 4, 20, NULL, '10 Rue de Minfeld', 'Ile-de-France', 'Limours', '91470', 'Essonne', 'France'),
(21, 4, 21, NULL, 'undefined Sousse', 'Ile-de-France', 'Tunis', '93', 'Seine-Saint-Denis', 'Tunisie'),
(22, 6, 22, NULL, '59 Avenue du Petit Bois', 'Hauts-de-France', 'Charleville-Mézières', '8000', 'Somme', 'France'),
(23, NULL, 23, NULL, 'undefined Avenue des Bois Clairs', 'Ile-de-France', 'Sainte-Geneviève-des-Bois', '91700', 'Essonne', 'France'),
(24, 6, 24, NULL, '31 Rue Jaillant Deschainets', 'Grand Est', 'Troyes', '10000', 'Aube', 'France'),
(25, 1, 25, NULL, '31 Rue Peclet', 'Ile-de-France', 'Paris', '78', 'Yvelines', 'France'),
(26, 6, 26, NULL, '31 Rue Jaillant Deschainets', 'Grand Est', 'Troyes', '10000', 'Aube', 'France'),
(27, 4, 27, NULL, 'undefined Avenue des Bois Clairs', 'Ile-de-France', 'Sainte-Geneviève-des-Bois', '91700', 'Essonne', 'France'),
(28, 6, 28, NULL, '31 Rue Peclet', 'Ile-de-France', 'Paris', '78', 'Yvelines', 'France'),
(29, 1, 29, NULL, '31 Rue Peclet', 'Ile-de-France', 'Paris', '78', 'Yvelines', 'France'),
(30, 4, 30, NULL, '43 Rue Limogeanne', 'Nouvelle-Aquitaine', 'Périgueux', '33127', 'Gironde', 'France'),
(31, NULL, 31, NULL, 'undefined Rue Jean Robert', 'Ile-de-France', 'Paris', '91', 'Essonne', 'France'),
(32, 6, 32, NULL, '22 Rue Jean-Baptiste Thierry Solet', 'Grand Est', 'Nancy', '54000', 'Meurthe-et-Moselle', 'France'),
(33, 4, 33, NULL, '64 Rue Philippe Lebel', 'Hauts-de-France', 'Portbail', '60700', 'Oise', 'France'),
(34, 5, 34, NULL, '41 Avenue Sainte-Victoire', 'Hauts-de-France', 'Aix-en-Provence', '59400', 'Nord', 'France'),
(35, 3, 35, NULL, '38B Avenue de Liège', 'Hauts-de-France', 'Valenciennes', '59300', 'Nord', 'France'),
(36, NULL, 36, NULL, '129 Rue Paul Baudoin', 'Hauts-de-France', 'Villeselve', '60640', 'Oise', 'France'),
(37, 5, 37, NULL, '129 Rue Paul Baudoin', 'Hauts-de-France', 'Villeselve', '60640', 'Oise', 'France'),
(38, 3, 38, NULL, '13 Avenue de la Bourdonnais', NULL, 'Paris', '75007', NULL, 'France'),
(39, 3, 39, NULL, '6 Rue À Heroc', 'Hauts-de-France', 'Amigny-Rouy', '02700', 'Aisne', 'France'),
(40, 8, 40, NULL, '8 Rue du Docteur Leroy', 'Ile-de-France', 'Le Mans', '95100', 'Val-d’Oise', 'France'),
(41, 5, 41, NULL, '7 Rue de Chantraine', 'Hauts-de-France', 'Rougeries', '02140', 'Aisne', 'France'),
(42, 3, 42, NULL, '17 Rue Gabriel Péri', 'Hauts-de-France', 'Saint-Quentin', '02100', 'Aisne', 'France'),
(43, 3, 43, NULL, '17 Rue Gabriel Péri', 'Hauts-de-France', 'Saint-Quentin', '02100', 'Aisne', 'France'),
(44, 8, 44, NULL, '88 Rue de la Papeterie', 'Ile-de-France', 'Éragny', '95610', 'Val-d’Oise', 'France'),
(45, 6, 45, NULL, '59 Avenue du Petit Bois', 'Hauts-de-France', 'Charleville-Mézières', '8000', 'Somme', 'France'),
(46, 2, 46, NULL, '22 Rue Jean-Baptiste Thierry Solet', 'Grand Est', 'Nancy', '54000', 'Meurthe-et-Moselle', 'France'),
(47, 3, 47, NULL, '1234 Bellevue Avenue', 'Occitanie', 'Los Angeles', '12345', 'Aveyron', 'États-Unis'),
(48, 3, 48, NULL, '31 Rue Peclet', 'Bourgogne-Franche-Comté', 'Paris', '21', 'Cote-d’Or', 'France'),
(49, 4, 49, NULL, '32 Rue Gambetta', 'Grand Est', 'Vénissieux', '51100', 'Marne', 'France'),
(50, 5, 50, NULL, '41 Boulevard du Maréchal Juin', 'Hauts-de-France', 'Saint-Quentin', '02100', 'Aisne', 'France'),
(51, 4, 51, NULL, '5 Avenue du Président Edouard Herriot', 'Hauts-de-France', 'Séné', '02100', 'Aisne', 'France'),
(52, 5, 52, NULL, 'undefined Rue Antoine Parmentier', 'Hauts-de-France', 'Saint-Quentin', '02100', 'Aisne', 'France'),
(53, 5, 53, NULL, '153 Rue de Mulhouse', 'Hauts-de-France', 'Saint-Quentin', '02100', 'Aisne', 'France'),
(54, 5, 54, NULL, '153 Rue de Mulhouse', 'Hauts-de-France', 'Saint-Quentin', '02100', 'Aisne', 'France'),
(55, 5, 55, NULL, '51 Rue des Aulnoyes', 'Nouvelle-Aquitaine', 'Domptin', '2310', 'Creuse', 'France'),
(56, 5, 56, NULL, '51 Rue des Aulnoyes', 'Nouvelle-Aquitaine', 'Domptin', '2310', 'Creuse', 'France'),
(57, 8, 57, NULL, '3 Chemin À Dieu', 'Ile-de-France', 'Bonnelles', '78803', 'Yvelines', 'France'),
(58, 8, 58, NULL, '88 Rue de la Papeterie', 'Ile-de-France', 'Éragny', '95610', 'Val-d’Oise', 'France'),
(59, 8, 59, NULL, '8 Rue du Docteur Leray', 'Ile-de-France', 'Argenteuil', '95100', 'Val-d’Oise', 'France'),
(60, 5, 60, NULL, '7 Rue de Chantraine', 'Bourgogne-Franche-Comté', 'Rougeries', '2140', 'Cote-d’Or', 'France'),
(61, 5, 61, NULL, 'undefined Route de Charleville', 'Grand Est', 'Lumes', '08000', 'Ardennes', 'France'),
(62, 5, 62, NULL, 'undefined Grand Rue', 'Hauts-de-France', 'Attilly', '02490', 'Aisne', 'France'),
(63, 6, 63, NULL, '145 Rue du Château', 'Grand Est', 'Flêtre', '51210', 'Marne', 'France'),
(64, 3, 64, NULL, '8 Rue du Docteur Leray', 'Ile-de-France', 'Argenteuil', '95100', 'Val-d’Oise', 'France'),
(65, 3, 65, NULL, '8 Rue du Docteur Leray', 'Ile-de-France', 'Argenteuil', '95100', 'Val-d’Oise', 'France'),
(66, 1, 66, NULL, '31 Rue Peclet', 'Bourgogne-Franche-Comté', 'Paris', '89', 'Yonne', 'France'),
(67, 3, 67, NULL, '8 Rue du Docteur Leray', 'Ile-de-France', 'Argenteuil', '95100', 'Val-d’Oise', 'France'),
(68, 3, 68, NULL, '8 Rue du Docteur Leray', 'Ile-de-France', 'Argenteuil', '95100', 'Val-d’Oise', 'France'),
(69, 2, 69, NULL, 'undefined Rue du Docteur Leray', 'Ile-de-France', 'Argenteuil', '95', 'Val-d’Oise', 'France'),
(70, 5, 70, NULL, 'undefined Square des Rougeries', 'Hauts-de-France', 'Saint-Denis-d\'Anjou', '02140', 'Aisne', 'France'),
(71, 2, 71, NULL, 'undefined Boulevard de Magenta', 'Ile-de-France', 'Paris', '75010', 'Paris', 'France'),
(72, 5, 72, NULL, '41 Avenue de la Victoire', 'Hauts-de-France', 'Cambrai', '59400', 'Nord', 'France'),
(73, 3, 73, NULL, '9 Rue Saint-Géry', 'Hauts-de-France', 'Valenciennes', '59300', 'Nord', 'France'),
(74, 3, 74, NULL, 'undefined Rue Godot de Mauroy', 'Ile-de-France', 'Paris', '75009', 'Paris', 'France'),
(75, 3, 75, NULL, '30 Rue Godot de Mauroy', 'Ile-de-France', 'Paris', '75009', 'Paris', 'France'),
(76, 3, 76, NULL, '9 Rue Saint-Géry', 'Hauts-de-France', 'Valenciennes', '59300', 'Nord', 'France'),
(77, 2, 77, NULL, '185 Avenue Carnot', 'Grand Est', 'Charleville-Mézières', '08', 'Ardennes', 'France'),
(78, 3, 78, NULL, 'undefined Rue Jules Ferry', 'Hauts-de-France', 'Loos', '59120', 'Nord', 'France'),
(79, 6, 79, NULL, '54 Pass Avenue', 'Ile-de-France', 'Thirroul', '75002', 'Paris', 'Australie'),
(80, 6, 80, NULL, '54 Passage des Panoramas', 'Ile-de-France', 'Paris', '75002', 'Paris', 'France'),
(81, 6, 81, NULL, '78 Avenue de Suffren', 'Ile-de-France', 'Paris', '75015', 'Paris', 'France'),
(82, 3, 82, NULL, '41 Avenue de la Victoire', 'Hauts-de-France', 'Cambrai', '59400', 'Nord', 'France'),
(83, 5, 83, NULL, '17 Rue Letellier', 'Ile-de-France', 'Paris', '75116', 'Paris', 'France'),
(84, 5, 84, NULL, '17 Rue Letellier', 'Ile-de-France', 'Paris', '75116', 'Paris', 'France'),
(85, 5, 85, NULL, '17 Rue Letellier', 'Ile-de-France', 'Paris', '75116', 'Paris', 'France'),
(86, 5, 86, NULL, '5 Rue Jean Mermoz', 'Ile-de-France', 'Paris', '75008', 'Paris', 'France'),
(87, 5, 87, NULL, '15 Boulevard Garibaldi', 'Ile-de-France', 'Paris', '75015', 'Paris', 'France'),
(88, 5, 88, NULL, '13 Rue Lapérouse', 'Ile-de-France', 'Pantin', '93500', 'Seine-Saint-Denis', 'France'),
(89, 5, 89, NULL, '14 Avenue de Gargan', 'Ile-de-France', 'Aulnay-sous-Bois', '95500', 'Val-d’Oise', 'France'),
(90, 6, 90, NULL, '85 Rue des Rosiers', 'Ile-de-France', 'Saint-Ouen', '93400', 'Seine-Saint-Denis', 'France'),
(91, 4, 91, NULL, '6 Rue Pierre Girard', 'Ile-de-France', 'Paris', '75019', 'Paris', 'France'),
(92, 6, 92, NULL, '65 Rue Ramey', 'Ile-de-France', 'Paris', '75018', 'Paris', 'France'),
(93, 5, 93, NULL, '1 Avenue Jean-Pierre Sabatier', 'Hauts-de-France', 'Frouzins', '02000', 'Aisne', 'France'),
(94, 5, 94, NULL, '1 Promenade de la Couloire', 'Hauts-de-France', 'Laon', '02000', 'Aisne', 'France'),
(95, 5, 95, NULL, '66 Rue Gabriel Péri', 'Hauts-de-France', 'Gentilly', '02100', 'Aisne', 'France'),
(96, 6, 96, NULL, '1 Place Boieldieu', 'Ile-de-France', 'Paris', '75002', 'Paris', 'France'),
(97, 5, 97, NULL, '11 Rue de la Guiblette', 'Ile-de-France', 'Cormeilles-en-Parisis', '95240', 'Val-d’Oise', 'France'),
(98, 9, 98, NULL, '31 Rue Esquirol', 'Occitanie', 'Paris', '30', 'Gard', 'France'),
(99, 5, 99, NULL, '110 Rue Pierre Brossolette', 'Ile-de-France', 'Yerres', '91330', 'Essonne', 'France'),
(100, 4, 100, NULL, '10 Rue Sauffroy', 'Ile-de-France', 'Paris', '75017', 'Paris', 'France'),
(101, 4, 101, NULL, '10 Rue des Nordets', 'Ile-de-France', 'Méry-sur-Oise', '95540', 'Val-d’Oise', 'France'),
(102, 5, 102, NULL, 'undefined Rue de la Guiblette', 'Ile-de-France', 'Cormeilles-en-Parisis', '95240', 'Val-d’Oise', 'France'),
(103, 5, 103, NULL, '112 Rue Jacques Schlosser', 'Ile-de-France', 'Chelles', '77500', 'Seine-et-Marne', 'France'),
(104, 6, 104, NULL, '1 Allée des Saules', 'Ile-de-France', 'Saint-Denis', '93200', 'Seine-Saint-Denis', 'France'),
(105, 5, 105, NULL, '13 Rue Molière', 'Ile-de-France', 'Paris', '94200', 'Val-de-Marne', 'France'),
(106, 5, 106, NULL, '112 Rue Jacques Schlosser', 'Ile-de-France', 'Chelles', '77500', 'Seine-et-Marne', 'France'),
(107, 3, 107, NULL, '4 Avenue de Flore', NULL, 'Istres', '95500', NULL, 'France'),
(108, 3, 108, NULL, '10 Rue des Grillons', 'Occitanie', 'Saint-Estève', '66240', 'Pyrénées-Orientales', 'France'),
(109, 3, 109, NULL, '4 Avenue Flore', 'Ile-de-France', 'Le Thillay', '95500', 'Val-d’Oise', 'France'),
(110, 4, 110, NULL, '10 Rue Sauffroy', 'Ile-de-France', 'Paris', '75017', 'Paris', 'France'),
(111, 3, 111, NULL, 'undefined Pariser Platz', 'Occitanie', 'Berlin', '12345', 'Aveyron', 'Allemagne'),
(112, 2, 112, NULL, '10 Rue Mesnil', 'Ile-de-France', 'Paris', '75116', 'Paris', 'France'),
(113, 3, 113, NULL, '7 Rue Evariste Galois', 'Ile-de-France', 'Paris', '75020', 'Paris', 'France'),
(114, 5, 114, NULL, '45 Rue du Faubourg Montmartre', 'Ile-de-France', 'Paris', '75009', 'Paris', 'France'),
(115, 3, 115, NULL, '7 Rue Evariste Galois', 'Ile-de-France', 'Paris', '75020', 'Paris', 'France'),
(116, 2, 116, NULL, '24 Boulevard Jean Jaurès', 'Ile-de-France', 'Clichy', '92110', 'Hauts-de-Seine', 'France'),
(117, 3, 117, NULL, '11 Rue de la Guiblette', 'Ile-de-France', 'Cormeilles-en-Parisis', '77500', 'Seine-et-Marne', 'France'),
(118, 3, 118, NULL, '6 Avenue Carnot', 'Ile-de-France', 'Paris', '75017', 'Paris', 'France'),
(119, 3, 119, NULL, '6 Avenue Carnot', 'Ile-de-France', 'Paris', '75017', 'Paris', 'France'),
(120, 3, 120, NULL, '5 Rue Jacquemont', 'Ile-de-France', 'Paris', '75017', 'Paris', 'France'),
(121, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(122, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(125, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(126, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(131, 22, NULL, NULL, 'Chi An Lorell, Sector 2, Aerohub Business Park, St. Mawgan', NULL, NULL, NULL, NULL, NULL),
(132, 23, NULL, NULL, 'Chi An Lorell, Sector 2, Aerohub Business Park, St. Mawgan', NULL, NULL, NULL, NULL, NULL),
(133, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(134, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(135, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(136, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(137, 28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(138, 29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(139, 30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(140, 31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(141, 32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(142, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(143, 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(144, 35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(145, 36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(146, 37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(147, 38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(148, 39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(149, 40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(150, 41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(151, 42, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(152, 43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(153, 44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(154, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(155, 46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(156, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(157, 48, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(158, 49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(159, 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(160, 51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(161, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(162, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(163, 54, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(164, 55, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(165, 56, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(166, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(167, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(168, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(169, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `desc` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `verified_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `desc`, `created_at`, `verified_at`) VALUES
(1, 'Lorem ipsum dolor sit', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', '2021-09-08 00:08:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `calendar_events`
--

CREATE TABLE `calendar_events` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `contacts_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date` datetime NOT NULL,
  `color` varchar(255) DEFAULT NULL,
  `confirmed` tinyint(4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `calendar_events`
--

INSERT INTO `calendar_events` (`id`, `users_id`, `contacts_id`, `title`, `description`, `date`, `color`, `confirmed`) VALUES
(6, 3, 7, 'Sandrine  BAROUDI', NULL, '2021-09-09 07:00:00', '#038418', 1),
(8, 5, 9, 'malik Djaber', NULL, '2021-09-03 16:00:00', 'blue', 0),
(9, 6, 10, 'Valerie le seigneur', NULL, '2021-09-02 10:16:38', '#038418', 1),
(11, 4, 12, 'Cyprien BRUEL', NULL, '2021-09-06 09:00:00', '#038418', 1),
(12, 3, 13, 'Elodie  LAMBERT', NULL, '2021-09-03 17:00:00', 'blue', 0),
(13, 6, 14, 'Marie-Laure Griffe', NULL, '2021-09-06 08:00:00', '#038418', 1),
(14, 3, 15, 'Yassmin THIESSART', NULL, '2021-09-07 12:30:00', 'blue', 0),
(15, 5, 16, 'pristillia matins', NULL, '2021-09-03 08:18:22', 'blue', 0),
(18, 3, 19, 'cecile brian', NULL, '2021-09-13 11:05:00', 'blue', 0),
(19, 4, 20, 'luca delphine', NULL, '2021-09-06 09:30:00', 'blue', 0),
(21, 6, 22, 'benoit guilley', NULL, '2021-09-06 12:00:00', '#038418', 1),
(22, 6, 24, 'Sadia  Duranton', NULL, '2021-09-15 12:00:00', 'blue', 0),
(23, 1, 25, 'khalil mecha', NULL, '2021-09-22 10:30:00', 'blue', 0),
(24, 6, 26, 'Sadia Duranton', NULL, '2021-09-13 11:00:00', '#038418', 1),
(25, 4, 27, 'estella ngono', NULL, '2021-09-08 15:00:00', '#038418', 1),
(26, 6, 28, 'job  id', NULL, '2021-09-23 04:00:00', 'blue', 0),
(27, 1, 29, 'khalil mecha', NULL, '2021-09-21 04:00:00', '#038418', 1),
(28, 4, 30, 'Cyprien BRUEL', NULL, '2021-09-14 11:00:00', '#038418', 1),
(29, 6, 32, 'Olivier  DAMBRINE', NULL, '2021-09-09 12:00:00', '#038418', 1),
(30, 5, 34, 'Vincent  LAURENT', NULL, '2021-09-17 09:00:00', 'blue', 0),
(31, 3, 35, 'Cathrine descamps', NULL, '2021-09-10 11:00:00', 'blue', 0),
(32, 5, 37, 'corimne koryne', NULL, '2021-09-10 14:00:00', 'blue', 0),
(33, 3, 38, 'Veronique Doumas', NULL, '2021-09-09 09:30:00', 'blue', 0),
(34, 3, 39, 'Jacques Delaporte', NULL, '2021-09-09 15:00:00', '#038418', 1),
(35, 8, 40, 'Bilal Tatfi', NULL, '2021-09-13 14:00:00', 'blue', 0),
(36, 5, 41, 'GERARD HAAS', NULL, '2021-09-13 08:00:00', '#038418', 1),
(37, 3, 42, 'Yagmur  AYDIN', NULL, '2021-09-09 10:00:00', 'blue', 0),
(38, 3, 43, 'AYDIN  Yagmur', NULL, '2021-09-09 10:00:00', 'blue', 0),
(39, 8, 44, 'jaques machner', NULL, '2021-09-09 17:00:00', '#038418', 1),
(40, 6, 45, 'Benoit Guilley', NULL, '2021-09-13 14:00:00', 'blue', 0),
(41, 3, 47, '11 11', NULL, '2021-09-24 06:45:00', 'blue', 0),
(42, 3, 48, 'gjhg gjgjg', NULL, '2021-09-25 05:30:00', 'blue', 0),
(43, 4, 49, 'Gerald  GONTIER', NULL, '2021-09-07 10:00:00', 'blue', 0),
(44, 5, 50, 'Michel  Legrand', NULL, '2021-09-16 10:00:00', '#038418', 1),
(45, 4, 51, 'Anselin thibaut', NULL, '2021-09-13 10:00:00', '#038418', 1),
(46, 5, 52, 'flomant julien', NULL, '2021-09-21 15:00:00', 'blue', 0),
(47, 5, 53, 'julien Flomant', NULL, '2021-09-13 15:00:00', 'blue', 0),
(48, 5, 54, 'julien Flomant', NULL, '2021-09-13 15:00:00', 'blue', 0),
(49, 5, 55, 'pascale besranard', NULL, '2021-09-20 14:00:00', 'blue', 0),
(50, 5, 56, 'pascal besnard', NULL, '2021-09-20 14:00:00', '#038418', 1),
(51, 8, 57, 'jeramie malais', NULL, '2021-09-20 17:30:00', 'blue', 0),
(52, 8, 58, 'jaques machner', NULL, '2021-09-13 17:00:00', '#038418', 1),
(53, 8, 59, 'Bilal Tatfi', NULL, '2021-09-14 15:00:00', 'blue', 0),
(54, 5, 60, 'GERARD  HAAS', NULL, '2021-09-13 18:00:00', 'blue', 0),
(55, 5, 61, 'grafteaux vincent', NULL, '2021-09-16 11:00:00', 'blue', 0),
(56, 5, 62, 'vivien Legrand', NULL, '2021-09-14 19:00:00', 'blue', 0),
(57, 6, 63, 'SEBASTIEN VERDRU', NULL, '2021-09-15 10:00:00', 'blue', 0),
(58, 3, 64, 'bilal  Tatfi', NULL, '2021-09-14 15:30:00', 'blue', 0),
(59, 3, 65, 'Bilal Tatfi', NULL, '2021-09-14 15:30:00', '#038418', 1),
(60, 1, 66, 'khalil mecha', NULL, '2021-09-29 09:00:00', 'blue', 0),
(61, 3, 67, 'Bilal tatfi', NULL, '2021-09-14 15:45:00', 'blue', 0),
(62, 3, 68, 'bilal Tatfi', NULL, '2021-09-14 16:00:00', '#038418', 1),
(63, 5, 70, 'Gerrard Hass', NULL, '2021-09-14 10:45:00', 'blue', 0),
(64, 2, 71, 'taieb jenathan', NULL, '2021-09-17 10:30:00', '#038418', 1),
(65, 5, 72, 'Vincent LAURENT', NULL, '2021-09-17 09:00:00', '#038418', 1),
(66, 3, 73, 'Egho Routier', NULL, '2021-09-16 12:00:00', 'blue', 0),
(67, 3, 74, 'knafo isac', NULL, '1970-01-01 00:00:00', 'blue', 0),
(68, 3, 75, 'isac knafo', NULL, '2021-09-20 10:00:00', 'blue', 0),
(69, 3, 76, 'Hugo Routier', NULL, '2021-09-16 12:00:00', 'blue', 0),
(70, 2, 77, 'vincent Grafteaux', NULL, '2021-09-16 11:00:00', '#038418', 1),
(71, 3, 78, 'Jessica  Lambinet', NULL, '2021-09-28 18:00:00', '#038418', 1),
(72, 6, 80, 'sabiene  todorvic', NULL, '2021-09-17 11:00:00', 'blue', 0),
(73, 3, 82, 'Vincent  LAURENT', NULL, '2021-09-17 17:00:00', '#038418', 1),
(74, 5, 83, 'Camille  de Marcellus', NULL, '2021-09-27 13:30:00', 'blue', 0),
(75, 5, 84, 'Camille  de Marcellus', NULL, '2021-09-27 14:30:00', 'blue', 0),
(76, 5, 85, 'Camille de Marcellus', NULL, '2021-09-27 14:30:00', 'blue', 0),
(77, 5, 86, 'VALERIA PAUL (rappel)', NULL, '2021-09-27 10:00:00', 'blue', 0),
(78, 5, 87, 'Camille de Marcellus', NULL, '2021-09-27 14:30:00', 'blue', 0),
(79, 5, 88, 'marion ******', NULL, '2021-09-23 09:00:00', 'blue', 0),
(80, 5, 89, 'christophe TISSERAND', NULL, '2021-09-27 14:00:00', 'blue', 0),
(81, 6, 90, 'Simon Martynoff', NULL, '2021-09-27 16:00:00', '#038418', 1),
(82, 4, 91, 'Salvatore FORTE', NULL, '2021-09-28 10:00:00', '#038418', 1),
(83, 6, 92, 'Eric  DEGREEF', NULL, '2021-09-28 14:00:00', 'blue', 0),
(84, 5, 94, 'stéphanie Duré', NULL, '2021-09-29 11:00:00', 'blue', 0),
(85, 5, 95, 'richet guwennaelle', NULL, '2021-09-30 13:30:00', '#038418', 1),
(86, 6, 96, 'Gerard  BERNET URIETA', NULL, '2021-09-28 13:00:00', 'blue', 0),
(87, 5, 97, 'plin anita', NULL, '2021-09-28 12:00:00', 'blue', 0),
(88, 9, 98, 'test test', NULL, '2021-10-06 07:30:00', '#038418', 1),
(89, 5, 99, 'jean  joyeux', NULL, '2021-09-29 10:30:00', 'blue', 0),
(90, 4, 100, 'joseph  bassama', NULL, '2021-09-30 13:00:00', 'blue', 0),
(91, 4, 101, 'Beaufils romain', NULL, '2021-10-05 15:00:00', 'blue', 0),
(92, 5, 102, 'anita plin', NULL, '2021-09-30 12:00:00', 'blue', 0),
(93, 5, 103, 'méggie garcelon', NULL, '2021-10-08 14:30:00', 'blue', 0),
(94, 6, 104, 'Michel Oliphar', NULL, '2021-09-29 16:00:00', '#038418', 1),
(95, 5, 105, 'arnaud malicet', NULL, '2021-10-08 09:00:00', 'blue', 0),
(96, 5, 106, 'méggie garcelon', NULL, '2021-10-01 14:30:00', '#038418', 1),
(97, 3, 107, 'christophe  TISSERAND', NULL, '2021-09-29 10:30:00', 'blue', 0),
(98, 3, 108, 'Remy  MARGINET', NULL, '2021-09-30 10:30:00', '#038418', 1),
(99, 3, 109, 'christophe  TISSERAND', NULL, '2021-09-29 17:30:00', 'blue', 0),
(100, 4, 110, 'Josteph Bassama', NULL, '2021-09-30 13:00:00', '#038418', 1),
(101, 3, 111, 'sofyane smida', NULL, '2021-09-30 10:45:00', 'blue', 0),
(102, 2, 112, 'M Mickael   TIRONI', NULL, '2021-10-01 18:00:00', 'blue', 0),
(103, 3, 113, 'lancry esaac', NULL, '2021-10-04 17:00:00', 'blue', 0),
(104, 5, 114, 'janina kubicki', NULL, '2021-10-05 08:00:00', 'blue', 0),
(105, 3, 115, 'lancry esaac', NULL, '2021-10-04 17:00:00', 'blue', 0),
(106, 2, 116, 'Khalil Ouhammou', NULL, '2021-10-04 17:00:00', 'blue', 0),
(107, 3, 117, 'méggie  garcelon', NULL, '2021-10-06 14:30:00', 'blue', 0),
(108, 3, 118, 'marie noelle', NULL, '2021-10-07 10:00:00', 'blue', 0),
(109, 3, 119, 'Marie-Noelle  DIEHL RODRIGUES', NULL, '2021-10-07 10:00:00', 'blue', 0),
(110, 3, 120, 'SOUBIGOU GUY', NULL, '2021-10-04 15:00:00', '#038418', 1);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `contacts_id` int(11) NOT NULL,
  `activities_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `salaries` varchar(255) DEFAULT NULL,
  `siret` varchar(255) DEFAULT NULL,
  `siren` varchar(255) DEFAULT NULL,
  `naf` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `turnover` varchar(255) DEFAULT NULL,
  `turnovers_id` int(11) DEFAULT NULL,
  `state_help` longtext DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `contacts_id`, `activities_id`, `name`, `status`, `salaries`, `siret`, `siren`, `naf`, `phone`, `turnover`, `turnovers_id`, `state_help`, `created_at`, `updated_at`) VALUES
(7, 7, 51, 'O SPA O SPA DETENTE', 'EURL', 'de 0 à 5 Personnes', '78951894100016', '78951894', '9604Z', '344866792', '0', 6, 'Chaumage partiel', '2021-09-02 07:33:27', '2021-09-05 23:44:30'),
(9, 9, 52, 'MDEN CONSTRUCTIONS', 'SARL', 'de 5 à 10 Personnes', '83013668500039', '830136685', 'XXXXX', NULL, '0', 7, 'Chaumage partiel', '2021-09-02 07:57:58', '2021-09-05 23:46:14'),
(10, 10, 4, 'Zou Créations', 'Individuelle', 'de 0 à 5 Personnes', '75200807800012', '752008078', '9499Z', NULL, '0', 28, 'Aucune aide', '2021-09-02 08:13:09', '2021-09-05 23:46:31'),
(12, 12, 13, 'Bélice', 'SARL', 'de 20 à 30 Personnes', '83326784200023', '833267842', '2042Z', '643831753', '0', 12, 'Aucune aide', '2021-09-02 09:10:41', '2021-09-05 23:46:45'),
(13, 13, 51, 'Spa Des Iles', 'SARL', 'de 10 à 20 Personnes', '53798014600014', '537980146', '9602B', '321527051', '-30', 7, 'Chaumage partiel', '2021-09-02 12:05:57', '2021-09-05 23:47:15'),
(14, 14, 4, 'Griffe Marie-Laure', 'Individuelle', 'de 0 à 5 Personnes', '44932955600012', '449329556', '524R', '688554476', '-70', 1, 'Fond de solidarité', '2021-09-02 12:37:07', '2021-09-05 23:47:38'),
(15, 15, 2, 'Boucherie Charcuterie Thiessart', 'SARL', 'de 10 à 20 Personnes', '89016855200010', '890168552', '6820B', '322750419', '0', 8, 'Aucune aide', '2021-09-03 05:50:06', '2021-09-05 23:48:03'),
(16, 16, 44, 'Les Paniers De Nicolas', 'SAS', 'de 5 à 10 Personnes', '82315292100024', '823152921', 'XXXXX', '681974410', '-60', 3, 'Fond de solidarité', '2021-09-03 06:17:53', '2021-09-05 23:48:40'),
(19, 19, 51, 'Les Soins O Naturels', 'EURL', 'de 5 à 10 Personnes', '81886505700017', '818865057', 'XXXXX', '321443868', '0', 3, 'Chaumage partiel', '2021-09-03 08:23:08', '2021-09-05 23:48:58'),
(20, 20, 13, 'C\'L C\'est local', 'SAS', 'de 0 à 5 Personnes', '88429151900012', '884291519', 'XXXXX', '629129856', '0', 1, 'Aucune aide', '2021-09-03 09:36:09', '2021-09-05 23:49:19'),
(22, 22, 24, 'Cercle Escrime de Charleville-Mézières', 'Individuelle', 'de 0 à 5 Personnes', '40218880900022', '402188809', 'XXXXX', '662666885', '-50', 6, 'Chaumage partiel', '2021-09-03 12:29:49', '2021-09-05 23:49:55'),
(23, 23, 43, 'Ngono Estelle', 'Individuelle', 'de 0 à 5 Personnes', '89091713100017', '890917131', 'XXXXX', '658821108', '-50', 1, 'Aucune aide', '2021-09-06 08:31:44', '2021-09-06 08:31:44'),
(24, 24, 3, 'Duranton Sadia', 'Individuelle', 'de 0 à 5 Personnes', '49422514700060', '494225147', 'XXXXX', '667864039', '0', 1, 'Chéque numérique et aide numérique de votre région', '2021-09-06 08:51:05', '2021-09-06 08:51:05'),
(25, 25, 4, 'xrone', 'EURL', 'de 20 à 30 Personnes', '83174545000021', '831745450', '1813Z', '20058738', '30', 10, 'Chéque numérique et aide numérique de votre région', '2021-09-06 09:06:47', '2021-09-06 09:06:47'),
(26, 26, 20, 'duranton sadia', 'Individuelle', 'de 0 à 5 Personnes', '49422514700060', '494225147', '8560Z', '06 67 86 40 39', '0', 1, 'Chéque numérique et aide numérique de votre région', '2021-09-06 09:52:28', '2021-09-06 09:52:28'),
(27, 27, 13, 'Ngono Estelle', 'Individuelle', 'de 0 à 5 Personnes', '89091713100017', '890917131', 'XXXXX', '658821108', '-50', 1, 'Aucune aide', '2021-09-06 10:08:11', '2021-09-06 10:08:11'),
(28, 28, 28, 'jobid', 'SARL', 'de 10 à 20 Personnes', '83778516100014', '837785161', 'XXXXX', '50133899', '-90', 2, 'Aucune aide', '2021-09-06 14:46:14', '2021-09-06 14:46:14'),
(29, 29, 6, 'aaa', 'SAS', 'de 5 à 10 Personnes', '83174545000021', '831745450', '1813Z', '20058738', '40', 20, 'Chéque numérique et aide numérique de votre région', '2021-09-06 14:50:21', '2021-09-06 14:50:21'),
(30, 30, 13, 'Bélice', 'SARL', 'de 20 à 30 Personnes', '83326784200023', '833267842', '2042Z', '643831753', '0', 12, 'Aucune aide', '2021-09-06 15:01:40', '2021-09-06 15:01:40'),
(31, 31, 2, 'Madelaine', 'MICRO-ENT', 'de 0 à 5 Personnes', '84173894100019', '841738941', 'XXXXX', NULL, '-70', 1, 'Fond de solidarité', '2021-09-07 08:22:00', '2021-09-07 08:22:00'),
(32, 32, 6, 'Integra Live', 'EURL', 'de 0 à 5 Personnes', '53324264000019', '533242640', '9002Z', '674946180', '-30', 5, 'Crédit d\'impôt', '2021-09-07 08:22:29', '2021-09-07 08:22:29'),
(33, 33, 46, 'Rigaux Peinture', 'SARL', 'de 0 à 5 Personnes', '44045138300010', '440451383', 'XXXXX', '607904429', '0', 4, 'Fond de solidarité', '2021-09-07 09:02:26', '2021-09-07 09:02:26'),
(34, 34, 8, 'VL Distribution', 'SARL', 'de 0 à 5 Personnes', '83400199200025', '834001992', '4759A', '614306258', '-20', 5, 'Chaumage partiel', '2021-09-07 11:23:01', '2021-09-07 11:23:01'),
(35, 35, 51, 'Espace Beaute', 'Individuelle', 'de 0 à 5 Personnes', '45386269000014', '453862690', '523E', '3 27 45 90 56', '-20', 1, 'Aucune aide', '2021-09-07 14:27:41', '2021-09-07 14:27:41'),
(36, 36, 8, 'F 2 D', 'SASU', 'de 0 à 5 Personnes', '87921613300014', '879216133', 'XXXXX', '626193226', '-30', 2, 'Chaumage partiel', '2021-09-07 15:41:20', '2021-09-07 15:41:20'),
(37, 37, 8, 'F 2 D - Tous Travaux Intérieur & Extérieur', 'SASU', 'de 0 à 5 Personnes', '87921613300014', '879216133', 'XXXXX', '06 26 19 32 26', '-30', 2, 'Chaumage partiel', '2021-09-07 15:49:29', '2021-09-07 15:49:29'),
(38, 38, 3, 'Eiffel Immo', 'SARL', 'de 0 à 5 Personnes', '50523084700020', '505230847', 'XXXXX', '140629374', '-30', 2, 'Chaumage partiel', '2021-09-08 09:48:51', '2021-09-08 09:48:51'),
(39, 39, 1, 'Delaporte Jacques', 'Individuelle', 'de 0 à 5 Personnes', '51380652100011', '513806521', 'XXXXX', '360727662', '-30', 1, 'Aucune aide', '2021-09-08 13:06:17', '2021-09-08 13:06:17'),
(40, 40, 44, 'mazaya', 'SARL', 'de 0 à 5 Personnes', '87780285000017', '877802850', '5610C', '646283644', '-20', 1, 'Chaumage partiel', '2021-09-08 13:48:56', '2021-09-08 13:48:56'),
(41, 41, 8, 'GH Plomberie', 'Individuelle', 'de 0 à 5 Personnes', '52406090200011', '524060902', '4322A', '627230730', '-30', 2, 'Crédit d\'impôt', '2021-09-08 14:36:59', '2021-09-08 14:36:59'),
(42, 42, 1, 'DECOVERT', 'SARL', 'de 20 à 30 Personnes', '83206982700018', '832069827', 'XXXXX', NULL, '0', 14, 'Chaumage partiel', '2021-09-08 15:31:13', '2021-09-08 15:31:13'),
(43, 43, 1, 'DECOVERT', 'SARL', 'de 20 à 30 Personnes', '83206982700018', '832069827', 'XXXXX', '603361394', '-30', 15, 'Chaumage partiel', '2021-09-08 15:41:02', '2021-09-08 15:41:02'),
(44, 44, 3, 'HETRE INEDIT', 'EURL', 'de 0 à 5 Personnes', '83514969100015', '835149691', 'XXXXX', '638230257', '-30', 4, 'Aucune aide', '2021-09-09 08:39:02', '2021-09-09 08:39:02'),
(45, 45, 53, 'Cercle Escrime de Charleville-Mézières', 'EURL', 'de 0 à 5 Personnes', '40218880900022', '402188809', 'XXXXX', '662666885', '-50', 6, 'Chaumage partiel', '2021-09-09 08:46:05', '2021-09-09 08:46:05'),
(46, 46, 46, 'integra live', 'SARL', 'de 0 à 5 Personnes', '53324264000019', '533242640', '9002Z', NULL, '-100', 4, 'Aucune aide', '2021-09-09 12:26:56', '2021-09-09 12:26:56'),
(47, 47, 2, '12345', 'SARL', 'de 0 à 5 Personnes', '12345678912345', '123456789', '45', '11', '-60', 2, 'Fond de solidarité', '2021-09-09 12:51:33', '2021-09-09 12:51:33'),
(48, 48, 1, 'jijjklj', 'SASU', 'de 0 à 5 Personnes', '454564564564444', '454564564', '55555', '2222', '30', 2, 'Crédit d\'impôt', '2021-09-09 12:53:17', '2021-09-09 12:53:17'),
(49, 49, 29, 'NG Musique (Eurl)', 'EURL', 'de 0 à 5 Personnes', '47978337500021', '479783375', 'XXXXX', '0326821992', '-10', 5, 'Chéque numérique et aide numérique de votre région', '2021-09-10 10:56:00', '2021-09-10 10:56:00'),
(50, 50, 8, 'Cofip Imo Sas', 'SAS', 'de 5 à 10 Personnes', '33188735600045', '331887356', '6820B', NULL, '-30', 7, 'Aucune aide', '2021-09-10 11:23:23', '2021-09-10 11:23:23'),
(51, 51, 3, 'Quincaillerie Anselin', 'SARL', 'de 0 à 5 Personnes', '43325264000019', '433252640', '4674A', NULL, '-10', 5, 'Chaumage partiel', '2021-09-10 13:08:33', '2021-09-10 13:08:33'),
(52, 52, 13, 'Intermarché SUPER Saint-Quentin', 'SAS', 'de 20 à 30 Personnes', '33135646900014', '331356469', 'XXXXX', '323061470', '0', 36, 'Crédit d\'impôt', '2021-09-10 15:03:28', '2021-09-10 15:03:28'),
(53, 53, 13, 'Intermarché station-service Saint-Quentin', 'SARL', 'de 20 à 30 Personnes', '38464915800019', '384649158', '4711D', '323061470', '0', 29, 'Crédit d\'impôt', '2021-09-10 15:21:07', '2021-09-10 15:21:07'),
(54, 54, 13, 'Intermarché station-service Saint-Quentin', 'SAS', 'de 30 à 40 Personnes', '38464915800019', '384649158', '4711D', '323061470', '0', 36, 'Crédit d\'impôt', '2021-09-10 15:27:26', '2021-09-10 15:27:26'),
(55, 55, 25, 'CARREFOUR DU NET EDITIONS', 'EURL', 'de 0 à 5 Personnes', '50136217200014', '501362172', '5811Z', '323691128', '-90', 1, 'Chaumage partiel', '2021-09-13 08:38:48', '2021-09-13 08:38:48'),
(56, 56, 25, 'CARREFOUR DU NET EDITIONS', 'EURL', 'de 0 à 5 Personnes', '50136217200014', '501362172', '5811Z', '323691128', '-90', 1, 'Chaumage partiel', '2021-09-13 08:55:45', '2021-09-13 08:55:45'),
(57, 57, 2, 'Sopenh', 'SASU', 'de 0 à 5 Personnes', '83081451300016', '830814513', 'XXXXX', NULL, '0', 2, 'Fond de solidarité', '2021-09-13 13:39:37', '2021-09-13 13:39:37'),
(58, 58, 3, 'HETRE INEDIT', 'EURL', 'de 0 à 5 Personnes', '83514969100015', '835149691', 'XXXXX', '638230257', '-30', 4, 'Aucune aide', '2021-09-13 14:13:12', '2021-09-13 14:13:12'),
(59, 59, 44, 'Mazaya', 'SARL', 'de 0 à 5 Personnes', '87780285000017', '877802850', '5610C', '0781508706', '-20', 1, 'Chaumage partiel', '2021-09-13 14:53:36', '2021-09-13 14:53:36'),
(60, 60, 8, 'GH Plomberie', 'Individuelle', 'de 0 à 5 Personnes', '52406090200011', '524060902', '4322A', '627230730', '-30', 2, 'Crédit d\'impôt', '2021-09-13 14:59:11', '2021-09-13 14:59:11'),
(61, 61, 25, 'Via Signalétique', 'EURL', 'de 0 à 5 Personnes', '42159120700020', '421591207', 'XXXXX', '324322626', '-60', 2, 'Aucune aide', '2021-09-13 15:42:25', '2021-09-13 15:42:25'),
(62, 62, 8, 'csq immpo', 'SAS', 'de 0 à 5 Personnes', '81452738800015', '814527388', '0000Z', '0629434034', '-90', 1, 'Aucune aide', '2021-09-14 09:08:13', '2021-09-14 09:08:13'),
(63, 63, 4, 'SOL\'ETICO', 'Individuelle', 'de 0 à 5 Personnes', '79908015500010', '799080155', 'XXXXX', '770264919', '-100', 3, 'Aucune aide', '2021-09-14 11:08:10', '2021-09-14 11:08:10'),
(64, 64, 44, 'Mazaya', 'SARL', 'de 0 à 5 Personnes', '87780285000017', '877802850', '5610C', '781508706', '0', 3, 'Chaumage partiel', '2021-09-14 13:23:10', '2021-09-14 13:23:10'),
(65, 65, 44, 'Mazaya', 'SARL', 'de 0 à 5 Personnes', '87780285000017', '877802850', '5610C', '781508706', '0', 3, 'Chaumage partiel', '2021-09-14 13:30:14', '2021-09-14 13:30:14'),
(66, 66, 5, 'test', 'SASU', 'de 0 à 5 Personnes', '83174545000021', '831745450', '1813Z', '20058738', '10', 4, 'Chéque numérique et aide numérique de votre région', '2021-09-14 13:33:59', '2021-09-14 13:33:59'),
(67, 67, 44, 'Mazaya', 'SARL', 'de 0 à 5 Personnes', '87780285000017', '877802850', '5610C', '781508706', '0', 4, 'Chaumage partiel', '2021-09-14 13:42:46', '2021-09-14 13:42:46'),
(68, 68, 44, 'mazaya', 'SARL', 'de 0 à 5 Personnes', '87780285000017', '877802850', '5610C', NULL, '0', 4, 'Chaumage partiel', '2021-09-14 13:50:32', '2021-09-14 13:50:32'),
(69, 69, 44, 'Mazaya', 'SARL', 'de 0 à 5 Personnes', '87780285000017', '877802850', '5610C', NULL, '50', 2, 'Chéque numérique et aide numérique de votre région', '2021-09-14 14:25:21', '2021-09-14 14:25:21'),
(70, 70, 8, 'GH Plomberie', 'SARL', 'de 0 à 5 Personnes', '52406090200011', '524060902', '4322A', '627230730', '-30', 2, 'Crédit d\'impôt', '2021-09-15 08:41:13', '2021-09-15 08:41:13'),
(71, 71, 13, 'secret garden', 'SASU', 'de 0 à 5 Personnes', '81925866600018', '819258666', '4778C', NULL, '-60', 2, 'Fond de solidarité', '2021-09-15 10:10:09', '2021-09-15 10:10:09'),
(72, 72, 8, 'VL DISTRIBUTION', 'SARL', 'de 0 à 5 Personnes', '83400199200025', '834001992', '4759A', '614306258', '-20', 5, 'Chaumage partiel', '2021-09-15 10:18:26', '2021-09-15 10:18:26'),
(73, 73, 13, 'Cbd\'eau', 'SAS', 'de 0 à 5 Personnes', '84125671200016', '841256712', '4645Z', '786665449', '0', 2, 'Aucune aide', '2021-09-15 14:44:25', '2021-09-15 14:44:25'),
(74, 74, 5, 'Björka design', 'SAS', 'de 0 à 5 Personnes', '79127821100010', '791278211', 'XXXXX', '153304180', '0', 5, 'Aucune aide', '2021-09-15 14:56:26', '2021-09-15 14:56:26'),
(75, 75, 13, 'Björka design', 'SAS', 'de 0 à 5 Personnes', '79127821100010', '791278211', 'XXXXX', '153304180', '0', 6, 'Fond de solidarité', '2021-09-15 15:08:04', '2021-09-15 15:08:04'),
(76, 76, 13, 'Cbd\'eau', 'SAS', 'de 0 à 5 Personnes', '84125671200016', '841256712', '4645Z', '786665449', '0', 4, 'Aucune aide', '2021-09-15 15:45:03', '2021-09-15 15:45:03'),
(77, 77, 14, 'via signalétique', 'EURL', 'de 0 à 5 Personnes', '42159120700020', '421591207', 'XXXXX', NULL, '-40', 2, 'Fond de solidarité', '2021-09-15 15:51:56', '2021-09-15 15:51:56'),
(78, 78, 3, 'Atelier JLA Vitrail, EIRL Jessica Lambinet', 'Individuelle', 'de 0 à 5 Personnes', '84969220700012', '849692207', 'XXXXX', '626255412', '-50', 2, 'Chéque numérique et aide numérique de votre région', '2021-09-16 08:57:39', '2021-09-16 08:57:39'),
(79, 79, 4, 'Au Matériel De Collections', 'EURL', 'de 0 à 5 Personnes', '44195147200014', '441951472', '4779Z', NULL, '-50', 2, 'Chéque numérique et aide numérique de votre région', '2021-09-16 10:15:26', '2021-09-16 10:15:26'),
(80, 80, 4, 'Au Matériel De Collections', 'EURL', 'de 0 à 5 Personnes', '44195147200014', '441951472', '4779Z', NULL, '-50', 2, 'Aucune aide', '2021-09-16 10:36:38', '2021-09-16 10:36:38'),
(81, 81, 4, 'Atelier Arnaud Huet A.H.A', 'SARL', 'de 0 à 5 Personnes', '44125876158400', '441258761', 'XXXX', NULL, '0', 2, 'Aucune aide', '2021-09-16 15:20:40', '2021-09-16 15:20:40'),
(82, 82, 3, 'VL DISTRIBUTION', 'SARL', 'de 0 à 5 Personnes', '83400199200025', '834001992', '4759A', '614306258', '0', 5, 'Chaumage partiel', '2021-09-17 08:45:29', '2021-09-17 08:45:29'),
(83, 83, 27, 'Camille de Marcellus', 'Individuelle', 'de 0 à 5 Personnes', '84118036700036', '841180367', 'XXXXX', '651830135', '-10', 1, 'Aucune aide', '2021-09-21 08:29:25', '2021-09-21 08:29:25'),
(84, 84, 27, 'Camille de Marcellus', 'Individuelle', 'de 0 à 5 Personnes', '84118036700036', '841180367', 'XXXXX', '651830135', '-10', 1, 'Aucune aide', '2021-09-21 08:52:21', '2021-09-21 08:52:21'),
(85, 85, 27, 'Camille de Marcellus', 'Individuelle', 'de 0 à 5 Personnes', '84118036700036', '841180367', 'XXXXX', '651830135', '-10', 1, 'Aucune aide', '2021-09-21 09:02:06', '2021-09-21 09:02:06'),
(86, 86, 51, 'STUDIO VALERIA PAUL', 'SARL', 'de 10 à 20 Personnes', '85222477300014', '852224773', 'XXXXX', '144071982', '-30', 6, 'Chaumage partiel', '2021-09-21 13:36:11', '2021-09-21 13:36:11'),
(87, 87, 27, 'Camille de Marcellus', 'Individuelle', 'de 0 à 5 Personnes', '84118036700010', '841180367', 'XXXXX', '651830135', '-20', 1, 'Aucune aide', '2021-09-21 14:46:10', '2021-09-21 14:46:10'),
(88, 88, 4, 'Atelier Dreieck', 'SARL', 'de 0 à 5 Personnes', '80394727400012', '803947274', '1814Z', '981865862', '-70', 1, 'Fond de solidarité', '2021-09-22 10:44:29', '2021-09-22 10:44:29'),
(89, 89, 8, 'Creas', 'SAS', 'de 0 à 5 Personnes', '49238713900032', '492387139', 'XXXXX', '148790647', '0', 5, 'Chaumage partiel', '2021-09-22 12:26:32', '2021-09-22 12:26:32'),
(90, 90, 4, 'Martynoff Simon Antiquités', 'SAS', 'de 0 à 5 Personnes', '82086084900018', '82086084', '4779Z', NULL, '0', 3, 'Aucune aide', '2021-09-23 08:18:47', '2021-09-23 08:18:47'),
(91, 91, 1, 'Epicerie Circulaire', 'SAS', 'de 0 à 5 Personnes', '89362080700014', '893620807', 'XXXXX', '650017997', '0', 2, 'Aucune aide', '2021-09-23 12:58:33', '2021-09-23 12:58:33'),
(92, 92, 13, 'cave 18', 'EURL', 'de 0 à 5 Personnes', '50228112400011', '502281124', '4725Z', NULL, '20', 4, 'Aucune aide', '2021-09-24 09:35:16', '2021-09-24 09:35:16'),
(93, 93, 13, 'Twins SAS', 'SAS', 'de 0 à 5 Personnes', '79808615300029', '798086153', 'XXXXX', '695715509', '-50', 1, 'Chéque numérique et aide numérique de votre région', '2021-09-24 10:26:33', '2021-09-24 10:26:33'),
(94, 94, 13, 'Twins sas', 'SAS', 'de 0 à 5 Personnes', '79808615300029', '798086153', 'XXXXX', '695715509', '-50', 1, 'Chéque numérique et aide numérique de votre région', '2021-09-24 10:39:57', '2021-09-24 10:39:57'),
(95, 95, 51, 'BELLAMINE ESTHETIQUE', 'Individuelle', 'de 0 à 5 Personnes', '79808615300029', '798086153', 'XXXXX', '323634458', '-30', 2, 'Chaumage partiel', '2021-09-24 14:20:35', '2021-09-24 14:20:35'),
(96, 96, 3, 'C Mon Appart', 'SAS', 'de 0 à 5 Personnes', '79531589400019', '795315894', '6831Z', NULL, '-50', 3, 'Aucune aide', '2021-09-27 09:52:37', '2021-09-27 09:52:37'),
(97, 97, 4, 'Murmures d\'une Charmeuse', 'Individuelle', 'de 0 à 5 Personnes', '52189801500016', '521898015', 'XXXXX', '683111214', '-30', 1, 'Chéque numérique et aide numérique de votre région', '2021-09-27 10:59:52', '2021-09-27 10:59:52'),
(98, 98, 2, 'test2', 'SARL', 'de 0 à 5 Personnes', '84918681200012', '849186812', '6312Z', '123456', '0', 3, 'Crédit d\'impôt', '2021-09-27 13:13:20', '2021-09-27 13:13:20'),
(99, 99, 4, 'Tattoo Art', 'Artisan', 'de 0 à 5 Personnes', '39912561600041', '399125616', '9609Z', '628618311', '-50', 1, 'Chéque numérique et aide numérique de votre région', '2021-09-27 13:19:48', '2021-09-27 13:19:48'),
(100, 100, 44, 'La Kribienne', 'SAS', 'de 0 à 5 Personnes', '84106203700012', '841062037', 'XXXXX', '644000845', '-60', 1, 'Chéque numérique et aide numérique de votre région', '2021-09-27 14:54:37', '2021-09-27 14:54:37'),
(101, 101, 1, 'Beaufils Traiteur', 'EURL', 'de 0 à 5 Personnes', '78938791700014', '789387917', 'XXXXX', '627631342', '-60', 3, 'Fond de solidarité', '2021-09-28 09:29:12', '2021-09-28 09:29:12'),
(102, 102, 4, 'Murmures d\'une Charmeuse', 'Individuelle', 'de 0 à 5 Personnes', '52189801500016', '521898015', 'XXXXX', '06 83 11 12 14', '-30', 1, 'Chéque numérique et aide numérique de votre région', '2021-09-28 10:06:10', '2021-09-28 10:06:10'),
(103, 103, 4, 'L\'Atelier de Dorure', 'MICRO-ENT', 'de 0 à 5 Personnes', '75024934400011', '750249344', '1629Z', '659784595', '-20', 2, 'Chéque numérique et aide numérique de votre région', '2021-09-28 12:40:52', '2021-09-28 12:40:52'),
(104, 104, 4, 'kalbass', 'Individuelle', 'de 0 à 5 Personnes', '52322231300018', '523222313', 'XXXXX', NULL, '-50', 1, 'Chéque numérique et aide numérique de votre région', '2021-09-28 14:52:23', '2021-09-28 14:52:23'),
(105, 105, 13, 'Emulsion', 'SAS', 'de 5 à 10 Personnes', '80738414400026', '807384144', '4644Z', '621296045', '-60', 14, 'Chaumage partiel', '2021-09-28 15:24:39', '2021-09-28 15:24:39'),
(106, 106, 4, 'L\'Atelier de Dorure', 'MICRO-ENT', 'de 0 à 5 Personnes', '75024934400011', '750249344', '1629Z', '659784595', '-20', 2, 'Chéque numérique et aide numérique de votre région', '2021-09-29 09:07:00', '2021-09-29 09:07:00'),
(107, 107, 8, 'C.R.E.C.A.S Centre Recherches Créations Activité Sociale', 'SAS', 'de 0 à 5 Personnes', '49238713900032', '492387139', 'XXXXX', '148790647', '0', 5, 'Chaumage partiel', '2021-09-29 09:14:26', '2021-09-29 09:14:26'),
(108, 108, 51, 'marginet coiffure', 'SASU', 'de 0 à 5 Personnes', '87840499500010', '878404995', 'XXXXX', '619899967', '0', 5, 'Crédit d\'impôt', '2021-09-29 10:51:49', '2021-09-29 10:51:49'),
(109, 109, 8, 'CREAS', 'SARL', 'de 0 à 5 Personnes', '49238713900032', '492387139', 'XXXXX', '148790647', '0', 2, 'Fond de solidarité', '2021-09-29 12:14:54', '2021-09-29 12:14:54'),
(110, 110, 44, 'la kribienne', 'SAS', 'de 0 à 5 Personnes', '84106203700012', '841062037', 'XXXXX', '644000845', '-60', 1, 'Chéque numérique et aide numérique de votre région', '2021-09-29 13:47:24', '2021-09-29 13:47:24'),
(111, 111, 1, 'jobid', 'SARL', 'de 0 à 5 Personnes', '84106203700012', '841062037', 'XXXXX', '1233333', '0', 5, 'Fond de solidarité', '2021-09-30 08:31:09', '2021-09-30 08:31:09'),
(112, 112, 8, 'amicka', 'EURL', 'de 0 à 5 Personnes', '51349501000011', '513495010', '4322A', NULL, '0', 5, 'Chaumage partiel', '2021-09-30 14:49:49', '2021-09-30 14:49:49'),
(113, 113, 8, 'Thermoline', 'SASU', 'de 0 à 5 Personnes', '89452662300018', '894526623', 'XXXXX', '0782576299', '0', 5, 'Aucune aide', '2021-10-01 09:53:09', '2021-10-01 09:53:09'),
(114, 114, 4, 'Atelier De Drouot Cavalier SARL', 'SARL', 'de 0 à 5 Personnes', '53460075400012', '534600754', '9003A', '952239566', '-50', 2, 'Fond de solidarité', '2021-10-01 09:56:48', '2021-10-01 09:56:48'),
(115, 115, 8, 'thermoline', 'SASU', 'de 0 à 5 Personnes', '89452662300018', '894526623', 'XXXXX', '0782576299', '0', 3, 'Aucune aide', '2021-10-01 10:05:43', '2021-10-01 10:05:43'),
(116, 116, 28, 'amimobile', 'SASU', 'de 0 à 5 Personnes', '83068831300014', '830688313', 'XXXXX', '0147391689', '-80', 3, 'Aucune aide', '2021-10-01 10:17:53', '2021-10-01 10:17:53'),
(117, 117, 4, 'L\'Atelier de Dorure', 'SARL', 'de 0 à 5 Personnes', '75024934400011', '750249344', '1629Z', '659784595', '-20', 2, 'Chéque numérique et aide numérique de votre région', '2021-10-01 13:09:35', '2021-10-01 13:09:35'),
(118, 118, 4, 'Enter 10 Productions SARL', 'SARL', 'de 0 à 5 Personnes', '50002946700031', '500029467', 'XXXXX', '0686263185', '-50', 6, 'Crédit d\'impôt', '2021-10-04 09:55:19', '2021-10-04 09:55:19'),
(119, 119, 4, 'Enter 10 Productions SARL', 'SARL', 'de 0 à 5 Personnes', '50002946700031', '500029467', 'XXXXX', '0686263185', '-50', 6, 'Chaumage partiel', '2021-10-04 10:06:12', '2021-10-04 10:06:12'),
(120, 120, 4, 'Soubigou Guy', 'Individuelle', 'de 0 à 5 Personnes', '32392679000037', '323926790', '5911B', '0610640294', '0', 3, 'Aucune aide', '2021-10-04 10:32:15', '2021-10-04 10:32:15');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `position` varchar(255) DEFAULT NULL,
  `comment` longtext DEFAULT NULL,
  `situation` varchar(255) DEFAULT NULL,
  `lead` tinyint(4) NOT NULL DEFAULT 0,
  `confirmed` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `users_id`, `first_name`, `last_name`, `email`, `phone`, `position`, `comment`, `situation`, `lead`, `confirmed`, `created_at`, `updated_at`) VALUES
(7, 3, 'Sandrine', 'BAROUDI', 'ospacompiegne@gmail.com', '344866792', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-02 07:33:27', '2021-09-02 07:34:12'),
(9, 5, 'malik', 'Djaber', 'bureaumpe@gmail.com', '658188319', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-02 07:57:58', '2021-09-02 07:58:37'),
(10, 6, 'Valerie', 'le seigneur', 'zoucrea@gmail.com', '681916131', 'Gérant', 'bon', 'compréhensif', 1, 1, '2021-09-02 08:13:09', '2021-09-02 08:13:56'),
(12, 4, 'Cyprien', 'BRUEL', 'c.bruel@belice-boutique.com', '643831753', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-02 09:10:41', '2021-09-02 09:11:32'),
(13, 3, 'Elodie', 'LAMBERT', 'sarldesiles@orange.fr', '321527051', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-02 12:05:57', '2021-09-02 12:06:47'),
(14, 6, 'Marie-Laure', 'Griffe', 'magriffe@orange.fr', '688554476', 'Gérant', 'Bon', 'compréhensif', 1, 1, '2021-09-02 12:37:07', '2021-09-02 12:38:21'),
(15, 3, 'Yassmin', 'THIESSART', 'benoit.thiessart@wanadoo.fr', '322750419', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-03 05:50:06', '2021-09-03 05:51:57'),
(16, 5, 'pristillia', 'matins', 'martinspristillia@gmail.com', '681974410', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-03 06:17:53', '2021-09-03 06:18:28'),
(19, 3, 'cecile', 'brian', 'cecile.brian1978@gmail.com', '321443868', 'Gérant', NULL, 'économe', 1, 1, '2021-09-03 08:23:08', '2021-09-03 08:23:50'),
(20, 4, 'luca', 'delphine', 'cl.cestlocal@gmail.com', '629129856', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-03 09:36:09', '2021-09-03 09:39:14'),
(22, 6, 'benoit', 'guilley', 'benoitguilley@yahoo.fr', '662666885', 'Gérant', 'très compréhensif', 'compréhensif', 1, 1, '2021-09-03 12:29:49', '2021-09-03 12:31:00'),
(23, NULL, 'estelle', 'ngono', 'estelle.ngono@orange.fr', '0666412013', 'Gérant', NULL, NULL, 1, 0, '2021-09-06 08:31:44', '2021-09-06 08:31:44'),
(24, 6, 'Sadia', 'Duranton', 'sadia@duranton.org', '667864039', 'Gérant', NULL, NULL, 1, 0, '2021-09-06 08:51:05', '2021-09-06 08:51:05'),
(25, 1, 'khalil', 'mecha', 'klilmecha@gmail.com', '20058738', 'Gérant', NULL, NULL, 1, 0, '2021-09-06 09:06:47', '2021-09-06 09:06:47'),
(26, 6, 'Sadia', 'Duranton', 'sadia@duranton.org', '06 67 86 40 39', 'Gérant', 'bon', 'compréhensif', 1, 1, '2021-09-06 09:52:28', '2021-09-06 09:52:58'),
(27, 4, 'estella', 'ngono', 'estelle.ngono@orange.fr', '658821108', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-06 10:08:11', '2021-09-06 10:08:40'),
(28, 6, 'job', 'id', 'klilmcha@gmail.com', '20058738', 'Directeur', NULL, 'économe', 1, 1, '2021-09-06 14:46:14', '2021-09-06 14:46:33'),
(29, 1, 'khalil', 'mecha', 'klilmecha@gmail.com', '20058738', 'Gérant', NULL, NULL, 1, 0, '2021-09-06 14:50:21', '2021-09-06 14:50:21'),
(30, 4, 'Cyprien', 'BRUEL', 'c.bruel@belice-boutique.com', '643831753', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-06 15:01:40', '2021-09-06 15:03:21'),
(31, NULL, 'Anaîs', 'Guinodie-moute', 'contact@madelaineaime.fr', '0622268962', 'Gérant', NULL, NULL, 1, 0, '2021-09-07 08:22:00', '2021-09-07 08:22:00'),
(32, 6, 'Olivier', 'DAMBRINE', 'contact@integra-live.com', '674946180', 'Gérant', NULL, 'roi', 1, 1, '2021-09-07 08:22:29', '2021-09-07 08:30:41'),
(33, 4, 'flavien', 'rigaux', 'contact@rigauxpeinture.fr', '607904429', 'Gérant', NULL, NULL, 1, 0, '2021-09-07 09:02:26', '2021-09-07 09:02:26'),
(34, 5, 'Vincent', 'LAURENT', 'aurent.cuisines@orange.fr', '614306258', 'Gérant', NULL, 'économe', 1, 1, '2021-09-07 11:23:01', '2021-09-07 11:25:29'),
(35, 3, 'Cathrine', 'descamps', 'espacebeaute.va@gmail.com', '633227067', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-07 14:27:41', '2021-09-07 14:28:57'),
(36, NULL, 'corimne', 'Djeouy', 'corimnekoryne@gmail.com', '626193226', 'Gérant', NULL, NULL, 1, 0, '2021-09-07 15:41:20', '2021-09-07 15:41:20'),
(37, 5, 'corimne', 'koryne', 'corimnekoryne@gmail.com', '06 26 19 32 26', 'Gérant', NULL, 'économe', 1, 1, '2021-09-07 15:49:29', '2021-09-07 15:49:57'),
(38, 3, 'Veronique', 'Doumas', 'vdaumas2@gmail.com', '633168577', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-08 09:48:51', '2021-09-08 09:50:12'),
(39, 3, 'Jacques', 'Delaporte', 'lenfantlollita@laposte.net', '673211513', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-08 13:06:17', '2021-09-08 13:06:47'),
(40, 8, 'Bilal', 'Tatfi', 'Bilal.tatfi@gmail.com', '646283644', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-08 13:48:56', '2021-09-08 13:49:54'),
(41, 5, 'GERARD', 'HAAS', 'ghplomberie@gmail.com', '627230730', 'Gérant', 'à rappeler lundi il n\'a pas accés son agenda, création d\'un site vitrine il n\'a pas un site', 'économe', 1, 1, '2021-09-08 14:36:59', '2021-09-08 14:38:36'),
(42, 3, 'Yagmur', 'AYDIN', 'decovert@yahoo.com', '603361394', 'Gérant', NULL, NULL, 1, 0, '2021-09-08 15:31:13', '2021-09-08 15:31:13'),
(43, 3, 'AYDIN', 'Yagmur', 'decovert@yahoo.com', '603361394', 'Gérant', NULL, 'roi', 1, 1, '2021-09-08 15:41:02', '2021-09-08 15:41:36'),
(44, 8, 'jaques', 'machner', 'hetreinedit@gmail.com', '638230257', 'Gérant', NULL, 'économe', 1, 1, '2021-09-09 08:39:02', '2021-09-09 08:40:36'),
(45, 6, 'Benoit', 'Guilley', 'benoitguilley@yahoo.fr', '662666885', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-09 08:46:05', '2021-09-09 08:47:42'),
(46, 2, 'olivier', 'Dambrine', 'contact@integra-live.com', '06744946180', 'Gérant', NULL, NULL, 1, 0, '2021-09-09 12:26:56', '2021-09-09 12:26:56'),
(47, 3, '11', '11', '11', '11', 'Associé', NULL, NULL, 1, 0, '2021-09-09 12:51:33', '2021-09-09 12:51:33'),
(48, 3, 'gjhg', 'gjgjg', 'amine.chouchaine@gmail.com', '22222', 'Directeur', NULL, 'compréhensif', 1, 1, '2021-09-09 12:53:17', '2021-09-09 12:53:34'),
(49, 4, 'Gerald', 'GONTIER', 'contact@ng-musique.com', '0326821992', 'Gérant', 'rappel pour la confirmation du date de rendez vous', 'économe', 1, 1, '2021-09-10 10:56:00', '2021-09-10 10:59:08'),
(50, 5, 'Michel', 'Legrand', 'm-legrand@cofip-imo.fr', '0323652980', 'Gérant', NULL, 'anxieux', 1, 1, '2021-09-10 11:23:23', '2021-09-10 11:27:00'),
(51, 4, 'Anselin', 'thibaut', 'equipeanselin@hotmail.fr', '323624176', 'Gérant', 'il a un site vitrine développer en 2000 .il aime bien avoir un site  vitrine ou bien e-commerce ,il a peut de produit en vente .\nil est très aimable et ouvert .', 'compréhensif', 1, 1, '2021-09-10 13:08:33', '2021-09-10 13:12:44'),
(52, 5, 'flomant', 'julien', 'pdv01119@mousquetaire.com', '323061470', 'Gérant', NULL, NULL, 1, 0, '2021-09-10 15:03:28', '2021-09-10 15:03:28'),
(53, 5, 'julien', 'Flomant', 'pdv01119@mousquetaire.com', '323061470', 'Gérant', NULL, NULL, 1, 0, '2021-09-10 15:21:07', '2021-09-10 15:21:07'),
(54, 5, 'julien', 'Flomant', 'pdv01119@mousquetaire.com', '323061470', 'Gérant', NULL, 'économe', 1, 1, '2021-09-10 15:27:26', '2021-09-10 15:28:42'),
(55, 5, 'pascale', 'besranard', 'pascale.besranard@gmail.com', '323691128', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-13 08:38:48', '2021-09-13 08:39:58'),
(56, 5, 'pascal', 'besnard', 'pascal.besnard@gmail.com', '323691128', 'Gérant', 'il maîtrise le wordress mais il a besoin d\'un expert il a 2 projets à réaliser pour 2 entreprises , il a douté de passer par une agence donc c\'est un sujet à élaborer avec lui', 'compréhensif', 1, 1, '2021-09-13 08:55:45', '2021-09-13 08:58:18'),
(57, 8, 'jeramie', 'malais', 'contact@sopenh.fr', '769721314', 'Gérant', NULL, 'indécis', 1, 1, '2021-09-13 13:39:37', '2021-09-13 13:56:14'),
(58, 8, 'jaques', 'machner', 'hetreinedit@gmail.com', '638230257', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-13 14:13:12', '2021-09-13 14:13:48'),
(59, 8, 'Bilal', 'Tatfi', 'Bilal.tatfi@gmail.com', '646283644', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-13 14:53:36', '2021-09-13 14:54:07'),
(60, 5, 'GERARD', 'HAAS', 'ghplomberie@gmail.com', '627230730', 'Gérant', NULL, 'économe', 1, 1, '2021-09-13 14:59:11', '2021-09-13 14:59:35'),
(61, 5, 'grafteaux', 'vincent', 'sia.signaletique@orange.fr', '324322626', 'Gérant', NULL, 'économe', 1, 1, '2021-09-13 15:42:25', '2021-09-13 15:42:47'),
(62, 5, 'vivien', 'Legrand', 'vivien.legrand@csqimmo.com', '0629434034', 'Gérant', NULL, 'économe', 1, 1, '2021-09-14 09:08:13', '2021-09-14 09:10:31'),
(63, 6, 'SEBASTIEN', 'VERDRU', 'bastoune59@gmail.com', '770264919', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-14 11:08:10', '2021-09-14 11:09:20'),
(64, 3, 'bilal', 'Tatfi', 'mazayaliban@gmail.com', '646283644', 'Gérant', NULL, NULL, 1, 0, '2021-09-14 13:23:10', '2021-09-14 13:23:10'),
(65, 3, 'Bilal', 'Tatfi', 'mazayaliban@gmail.com', '646283644', 'Gérant', NULL, NULL, 1, 0, '2021-09-14 13:30:14', '2021-09-14 13:30:14'),
(66, 1, 'khalil', 'mecha', 'klilmecha@gmail.com', '20058738', 'Gérant', NULL, 'économe', 1, 1, '2021-09-14 13:33:59', '2021-09-14 13:34:39'),
(67, 3, 'Bilal', 'tatfi', 'mazayaliban@gmail.com', '646283644', 'Gérant', NULL, NULL, 1, 0, '2021-09-14 13:42:46', '2021-09-14 13:42:46'),
(68, 3, 'bilal', 'Tatfi', 'mazayaliban@gmail.com', '781508706', 'Gérant', NULL, 'économe', 1, 1, '2021-09-14 13:50:32', '2021-09-14 13:51:15'),
(69, 2, 'Bilal', 'Tatfi', 'mazayaliban@gmail.com', '0781508706', 'Associé', NULL, NULL, 1, 0, '2021-09-14 14:25:21', '2021-09-14 14:25:21'),
(70, 5, 'Gerrard', 'Hass', 'ghplomberie@gmail.com', '627230730', 'Gérant', NULL, 'économe', 1, 1, '2021-09-15 08:41:13', '2021-09-15 08:47:06'),
(71, 2, 'taieb', 'jenathan', 'taiebjon@gmail.com', '0623335574', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-15 10:10:09', '2021-09-15 10:11:55'),
(72, 5, 'Vincent', 'LAURENT', 'laurent.cuisines@orange.fr', '614306258', 'Gérant', NULL, 'économe', 1, 1, '2021-09-15 10:18:26', '2021-09-15 10:19:12'),
(73, 3, 'Egho', 'Routier', 'egoroutier4@gmail.com', '786665449', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-15 14:44:25', '2021-09-15 14:46:13'),
(74, 3, 'knafo', 'isac', 'isac@bjorka-disgn.com', '153304180', 'Gérant', NULL, NULL, 1, 0, '2021-09-15 14:56:26', '2021-09-15 14:56:26'),
(75, 3, 'isac', 'knafo', 'isac@bjorka-design.com', '153304180', 'Gérant', NULL, 'économe', 1, 1, '2021-09-15 15:08:04', '2021-09-15 15:08:36'),
(76, 3, 'Hugo', 'Routier', 'ugoroutier4@gmail.com', '786665449', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-15 15:45:03', '2021-09-15 15:45:27'),
(77, 2, 'vincent', 'Grafteaux', 'via-signaletique@orange.fr', '0324322626', 'Gérant', NULL, 'économe', 1, 1, '2021-09-15 15:51:56', '2021-09-15 15:52:25'),
(78, 3, 'Jessica', 'Lambinet', 'jessica@jlavitrail.com', '626255412', 'Gérant', NULL, 'économe', 1, 1, '2021-09-16 08:57:39', '2021-09-16 08:58:02'),
(79, 6, 'sabiene', 'todorvic', 'fabtod@yahoo.fr', '142360318', 'Gérant', NULL, NULL, 1, 0, '2021-09-16 10:15:26', '2021-09-16 10:15:26'),
(80, 6, 'sabiene', 'todorvic', 'fabtod@yahoo.fr', '142360318', 'Gérant', NULL, 'économe', 1, 1, '2021-09-16 10:36:38', '2021-09-16 10:37:48'),
(81, 6, 'Arnaud', 'Huet', 'huet.atelier@gmail.com', '06 60 49 24 47', 'Gérant', NULL, NULL, 1, 0, '2021-09-16 15:20:40', '2021-09-16 15:20:40'),
(82, 3, 'Vincent', 'LAURENT', 'laurent.cuisines@orange.fr', '614306258', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-17 08:45:29', '2021-09-17 08:45:56'),
(83, 5, 'Camille', 'de Marcellus', 'Camillede Marcellus@gmail.com', '651830135', 'Gérant', NULL, NULL, 1, 0, '2021-09-21 08:29:25', '2021-09-21 08:29:25'),
(84, 5, 'Camille', 'de Marcellus', 'camilledemarcellus@gmail.com', '651830135', 'Gérant', NULL, NULL, 1, 0, '2021-09-21 08:52:21', '2021-09-21 08:52:21'),
(85, 5, 'Camille', 'de Marcellus', 'camilledemarcellus@gmail.com', '651830135', 'Gérant', NULL, NULL, 1, 0, '2021-09-21 09:02:06', '2021-09-21 09:02:06'),
(86, 5, 'VALERIA', 'PAUL', 'vp@studiovaleriapaul.com', '0628645212', 'Gérant', NULL, 'économe', 1, 1, '2021-09-21 13:36:11', '2021-09-21 13:37:10'),
(87, 5, 'Camille', 'de Marcellus', 'camilledemarcellus@gmail.com', '651830135', 'Gérant', 'elle est physiothérapeute, elle veut faire le renouvellement du site plus community management .', 'économe', 1, 1, '2021-09-21 14:46:10', '2021-09-21 14:47:51'),
(88, 5, 'marion', '******', 'atelier.dreieck@gmail.com', '981865862', 'Gérant', 'site du cpn ?', 'économe', 1, 1, '2021-09-22 10:44:29', '2021-09-22 10:45:30'),
(89, 5, 'christophe', 'TISSERAND', 'c.r.e.a.s@hotmail.fr', '148790647', 'Gérant', NULL, 'économe', 1, 1, '2021-09-22 12:26:32', '2021-09-22 12:27:11'),
(90, 6, 'Simon', 'Martynoff', 'contact@galerie-Martynoff.com', '662053253', 'Gérant', NULL, 'roi', 1, 1, '2021-09-23 08:18:47', '2021-09-23 08:19:44'),
(91, 4, 'Salvatore', 'FORTE', 'contact@epiceriecirculaire.fr', '650017997', 'Gérant', 'secteur d\'activité : épiceries fines\nun site e commerce', 'économe', 1, 1, '2021-09-23 12:58:33', '2021-09-23 12:59:46'),
(92, 6, 'Eric', 'DEGREEF', 'contact@cave18.fr', '0142527777', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-24 09:35:16', '2021-09-24 09:36:03'),
(93, 5, 'Stéphanie', 'Duré', 'twins.laon@free.fr', '695715509', 'Gérant', NULL, NULL, 1, 0, '2021-09-24 10:26:33', '2021-09-24 10:26:33'),
(94, 5, 'stéphanie', 'Duré', 'twins.laon@free.fr', '695715509', 'Gérant', NULL, 'anxieux', 1, 1, '2021-09-24 10:39:57', '2021-09-24 10:40:37'),
(95, 5, 'richet', 'guwennaelle', 'institutbellamine@gmail.com', '323634458', 'Gérant', NULL, 'économe', 1, 1, '2021-09-24 14:20:35', '2021-09-24 14:21:25'),
(96, 6, 'Gerard', 'BERNET URIETA', 'gbernet@c-mon-appart.com', '685301306', 'Gérant', NULL, 'économe', 1, 1, '2021-09-27 09:52:37', '2021-09-27 09:54:52'),
(97, 5, 'plin', 'anita', 'anita.plin@wanadoo.fr', '683111214', 'Gérant', NULL, 'économe', 1, 1, '2021-09-27 10:59:52', '2021-09-27 11:01:40'),
(98, 9, 'test', 'test', 'slimen.waad@gmail.com', '123456', 'Gérant', NULL, 'économe', 1, 1, '2021-09-27 13:13:20', '2021-09-27 13:13:46'),
(99, 5, 'jean', 'joyeux', 'decoradermijpettooart@yahoo.fr', '628618311', 'Gérant', NULL, 'économe', 1, 1, '2021-09-27 13:19:48', '2021-09-27 13:22:29'),
(100, 4, 'joseph', 'bassama', 'josephbassama@yahoo.fr', '669425858', 'Gérant', 'verification du mail', 'économe', 1, 1, '2021-09-27 14:54:37', '2021-09-27 14:55:49'),
(101, 4, 'Beaufils', 'romain', 'contact@beaufils-traiteur.fr', '627631342', 'Gérant', 'secteur d\'activité : traiteur', 'compréhensif', 1, 1, '2021-09-28 09:29:12', '2021-09-28 09:30:32'),
(102, 5, 'anita', 'plin', 'anita.plin@wanadoo.fr', '06 83 11 12 14', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-28 10:06:10', '2021-09-28 10:06:39'),
(103, 5, 'méggie', 'garcelon', 'atelierdedorure@gmail.com', '659784595', 'Gérant', NULL, 'économe', 1, 1, '2021-09-28 12:40:52', '2021-09-28 12:41:58'),
(104, 6, 'Michel', 'Oliphar', 'moliphar@kalbass.fr', '0669178588', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-28 14:52:23', '2021-09-28 14:53:07'),
(105, 5, 'arnaud', 'malicet', 'arnaud.malicet@emulsion.fr', '621296045', 'Gérant', 'il a besoin d\'un site E commerce et renouvellement du CRM ERP', 'économe', 1, 1, '2021-09-28 15:24:39', '2021-09-28 15:25:42'),
(106, 5, 'méggie', 'garcelon', 'latelierdedorure@gmail.com', '659784595', 'Gérant', NULL, 'économe', 1, 1, '2021-09-29 09:07:00', '2021-09-29 09:08:13'),
(107, 3, 'christophe', 'TISSERAND', 'c.r.e.a.s@hotmail.fr', '148790647', 'Gérant', NULL, 'économe', 1, 1, '2021-09-29 09:14:26', '2021-09-29 09:14:49'),
(108, 3, 'Remy', 'MARGINET', 'remymarginet7@hotmail.fr', '619899967', 'Gérant', NULL, 'économe', 1, 1, '2021-09-29 10:51:49', '2021-09-29 10:52:39'),
(109, 3, 'christophe', 'TISSERAND', 'c.r.e.a.s@hotmail.fr', '148790647', 'Gérant', NULL, 'économe', 1, 1, '2021-09-29 12:14:54', '2021-09-29 12:16:46'),
(110, 4, 'Josteph', 'Bassama', 'jostephbassama@yahoo.fr', '669425858', 'Gérant', NULL, 'économe', 1, 1, '2021-09-29 13:47:24', '2021-09-29 13:48:34'),
(111, 3, 'sofyane', 'smida', 's.smida@jobid.fr', '12333333', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-30 08:31:09', '2021-09-30 08:31:31'),
(112, 2, 'M Mickael', 'TIRONI', 'amicka.plomberie@laposte.net', '699266699', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-09-30 14:49:49', '2021-09-30 14:51:31'),
(113, 3, 'lancry', 'esaac', 'concat@thermoline-cvc.fr', '0782576299', 'Gérant', NULL, 'économe', 1, 1, '2021-10-01 09:53:09', '2021-10-01 09:54:26'),
(114, 5, 'janina', 'kubicki', 'contact@atelierdedrouot.fr', '952239566', 'Gérant', 'elle doute elle n\'a pas voulu donné une date pour le rendez vous', 'anxieux', 1, 1, '2021-10-01 09:56:48', '2021-10-01 10:00:06'),
(115, 3, 'lancry', 'esaac', 'contact@thermoline-cvc.fr', '0782576299', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-10-01 10:05:43', '2021-10-01 10:08:54'),
(116, 2, 'Khalil', 'Ouhammou', 'clichy@amimobile.fr', '0660890742', 'Gérant', NULL, 'économe', 1, 1, '2021-10-01 10:17:53', '2021-10-01 10:19:09'),
(117, 3, 'méggie', 'garcelon', 'latelierdedorure@gmail.com', '659784595', 'Gérant', NULL, 'économe', 1, 1, '2021-10-01 13:09:35', '2021-10-01 13:10:21'),
(118, 3, 'marie', 'noelle', 'mn.diehl@center-10.com', '0686263185', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-10-04 09:55:19', '2021-10-04 09:58:46'),
(119, 3, 'Marie-Noelle', 'DIEHL RODRIGUES', 'mn.diehl@enter-10.com', '0686263185', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-10-04 10:06:12', '2021-10-04 10:06:39'),
(120, 3, 'SOUBIGOU', 'GUY', 'soubigoug@gmail.com', '0610640294', 'Gérant', NULL, 'compréhensif', 1, 1, '2021-10-04 10:32:15', '2021-10-04 10:33:17');

-- --------------------------------------------------------

--
-- Table structure for table `counters`
--

CREATE TABLE `counters` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `elapsed_time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `counters`
--

INSERT INTO `counters` (`id`, `users_id`, `elapsed_time`) VALUES
(1, 6, '104'),
(2, 4, '-260'),
(3, 6, '253'),
(4, 6, '253'),
(5, 6, '253'),
(6, 6, '253'),
(7, 6, '253'),
(8, 6, '253'),
(9, 6, '253'),
(10, 6, '253'),
(11, 6, '253'),
(12, 4, '-312'),
(13, 6, '-1096'),
(14, 5, '-1457'),
(15, 5, '-1457'),
(16, 5, '-1457'),
(17, 5, '-1457'),
(18, 3, '-154'),
(19, 5, '-27'),
(20, 3, '-132'),
(21, 3, '-1028'),
(22, 3, '-1028'),
(23, 3, '-536'),
(24, 3, '-536'),
(25, 8, '-74'),
(26, 5, '-490'),
(27, 5, '-490'),
(28, 5, '-490'),
(29, 5, '-490'),
(30, 3, '-70'),
(31, 8, '-82'),
(32, 8, '-82'),
(33, 8, '-82'),
(34, 8, '-82'),
(35, 6, '-385'),
(36, 3, '294'),
(37, 4, '-1332'),
(38, 5, '-498'),
(39, 4, '-395'),
(40, 5, '79'),
(41, 5, '79'),
(42, 5, '79'),
(43, 5, '79'),
(44, 5, '-230'),
(45, 5, '-741'),
(46, 5, '-151'),
(47, 5, '-741'),
(48, 5, '-151'),
(49, 8, '-1072'),
(50, 8, '-223'),
(51, 8, '-223'),
(52, 8, '85'),
(53, 8, '85'),
(54, 8, '-59'),
(55, 5, '-198'),
(56, 5, '-1811'),
(57, 5, '-570'),
(58, 5, '-1811'),
(59, 5, '-570'),
(60, 5, '-519'),
(61, 6, '-853'),
(62, 6, '-853'),
(63, 6, '-853'),
(64, 6, '-853'),
(65, 6, '-853'),
(66, 6, '-853'),
(67, 6, '-853'),
(68, 6, '-853'),
(69, 6, '-853'),
(70, 6, '-853'),
(71, 6, '-853'),
(72, 6, '-853'),
(73, 6, '-853'),
(74, 6, '-853'),
(75, 6, '-853'),
(76, 6, '-853'),
(77, 6, '-853'),
(78, 6, '-853'),
(79, 6, '-853'),
(80, 6, '-853'),
(81, 6, '-853'),
(82, 6, '-853'),
(83, 6, '-853'),
(84, 6, '-853'),
(85, 6, '-853'),
(86, 1, '234'),
(87, 3, '162'),
(88, 5, '-306'),
(89, 2, '-941'),
(90, 2, '-124'),
(91, 2, '-941'),
(92, 2, '-124'),
(93, 5, '-311'),
(94, 3, '-1012'),
(95, 3, '-20'),
(96, 3, '99'),
(97, 2, '-202'),
(98, 3, '-1956'),
(99, 6, '-1147'),
(100, 6, '-1147'),
(101, 6, '7'),
(102, 6, '7'),
(103, 3, '-636'),
(104, 5, '-5379'),
(105, 5, '-5379'),
(106, 5, '-5379'),
(107, 5, '-5379'),
(108, 5, '-5379'),
(109, 5, '-5379'),
(110, 5, '-56'),
(111, 5, '-56'),
(112, 5, '-56'),
(113, 5, '-56'),
(114, 5, '-56'),
(115, 5, '-56'),
(116, 5, '-56'),
(117, 5, '-56'),
(118, 5, '-56'),
(119, 5, '-56'),
(120, 5, '-56'),
(121, 5, '-56'),
(122, 5, '-56'),
(123, 5, '-56'),
(124, 5, '-56'),
(125, 5, '-56'),
(126, 5, '-56'),
(127, 5, '-56'),
(128, 5, '-56'),
(129, 5, '-56'),
(130, 5, '-56'),
(131, 5, '-56'),
(132, 5, '-56'),
(133, 5, '-56'),
(134, 5, '-56'),
(135, 5, '-56'),
(136, 5, '-56'),
(137, 5, '-56'),
(138, 5, '-56'),
(139, 5, '-56'),
(140, 5, '-732'),
(141, 5, '-732'),
(142, 5, '-47'),
(143, 5, '-47'),
(144, 5, '-385'),
(145, 5, '-385'),
(146, 5, '-385'),
(147, 5, '-385'),
(148, 5, '-1573'),
(149, 5, '-1573'),
(150, 5, '-1573'),
(151, 5, '-1573'),
(152, 5, '-1573'),
(153, 5, '-1573'),
(154, 5, '-1573'),
(155, 5, '-1573'),
(156, 5, '-1573'),
(157, 5, '-1573'),
(158, 5, '-1573'),
(159, 5, '-1573'),
(160, 5, '-1573'),
(161, 5, '-1573'),
(162, 5, '-1573'),
(163, 5, '-1573'),
(164, 5, '-828'),
(165, 5, '-828'),
(166, 5, '-828'),
(167, 5, '-828'),
(168, 5, '-828'),
(169, 5, '-828'),
(170, 5, '-828'),
(171, 5, '-828'),
(172, 5, '-828'),
(173, 5, '-828'),
(174, 5, '-828'),
(175, 5, '-828'),
(176, 5, '-828'),
(177, 5, '-828'),
(178, 5, '-828'),
(179, 5, '-828'),
(180, 5, '-116'),
(181, 5, '-116'),
(182, 5, '-116'),
(183, 5, '-116'),
(184, 5, '-116'),
(185, 5, '-116'),
(186, 5, '-116'),
(187, 5, '-116'),
(188, 5, '134'),
(189, 5, '134'),
(190, 5, '134'),
(191, 5, '134'),
(192, 5, '134'),
(193, 5, '134'),
(194, 5, '134'),
(195, 5, '134'),
(196, 5, '134'),
(197, 5, '134'),
(198, 5, '134'),
(199, 5, '134'),
(200, 5, '134'),
(201, 5, '134'),
(202, 6, '-215'),
(203, 4, '-433'),
(204, 4, '-433'),
(205, 4, '-433'),
(206, 4, '-433'),
(207, 6, '-1761'),
(208, 6, '-458'),
(209, 6, '-458'),
(210, 6, '-1761'),
(211, 5, '-449'),
(212, 5, '-1879'),
(213, 5, '-449'),
(214, 5, '-1879'),
(215, 5, '-889'),
(216, 6, '-317'),
(217, 5, '-136'),
(218, 5, '-136'),
(219, 5, '-136'),
(220, 5, '-136'),
(221, 9, '62'),
(222, 5, '-921'),
(223, 5, '-921'),
(224, 5, '-921'),
(225, 5, '-921'),
(226, 5, '-921'),
(227, 5, '-921'),
(228, 5, '-425'),
(229, 5, '-425'),
(230, 5, '-425'),
(231, 4, '-1041'),
(232, 4, '-252'),
(233, 5, '15'),
(234, 5, '-11151'),
(235, 5, '-11151'),
(236, 5, '-11151'),
(237, 5, '-11151'),
(238, 5, '-11151'),
(239, 5, '-11151'),
(240, 5, '-11151'),
(241, 5, '-11151'),
(242, 5, '-11151'),
(243, 5, '-11151'),
(244, 5, '-1315'),
(245, 5, '-1315'),
(246, 5, '-1315'),
(247, 5, '-1315'),
(248, 5, '-1315'),
(249, 5, '-1315'),
(250, 5, '-1315'),
(251, 5, '-1315'),
(252, 5, '-1315'),
(253, 5, '-1315'),
(254, 5, '-129'),
(255, 5, '-129'),
(256, 5, '-129'),
(257, 5, '-129'),
(258, 5, '-129'),
(259, 6, '-474'),
(260, 5, '-17847'),
(261, 5, '-17847'),
(262, 5, '-17847'),
(263, 5, '-17847'),
(264, 5, '-17847'),
(265, 5, '-17847'),
(266, 5, '-17847'),
(267, 5, '-17847'),
(268, 5, '-17847'),
(269, 5, '-17847'),
(270, 5, '-17847'),
(271, 5, '-17847'),
(272, 5, '-5008'),
(273, 5, '-5008'),
(274, 5, '-5008'),
(275, 5, '-5008'),
(276, 5, '-5008'),
(277, 5, '-5008'),
(278, 5, '-5007'),
(279, 5, '-5007'),
(280, 5, '-5007'),
(281, 5, '-5007'),
(282, 5, '-5007'),
(283, 5, '-5007'),
(284, 5, '-836'),
(285, 5, '-836'),
(286, 5, '-836'),
(287, 5, '-836'),
(288, 5, '-836'),
(289, 5, '-836'),
(290, 5, '-347'),
(291, 5, '-347'),
(292, 5, '-347'),
(293, 5, '-347'),
(294, 5, '-347'),
(295, 5, '-347'),
(296, 5, '-91'),
(297, 3, '-32'),
(298, 3, '9'),
(299, 3, '52'),
(300, 4, '-328'),
(301, 4, '37'),
(302, 4, '-328'),
(303, 4, '37'),
(304, 3, '230'),
(305, 2, '-263'),
(306, 3, '-97'),
(307, 5, '-584'),
(308, 3, '-484'),
(309, 3, '-484'),
(310, 3, '-27'),
(311, 3, '-27'),
(312, 2, '-149'),
(313, 3, '-94'),
(314, 3, '-2202'),
(315, 3, '-305'),
(316, 3, '-2202'),
(317, 3, '-305'),
(318, 3, '-2969'),
(319, 3, '-2969'),
(320, 3, '-2969'),
(321, 3, '-599'),
(322, 3, '-599'),
(323, 3, '-599'),
(324, 3, '66'),
(325, 3, '66'),
(326, 3, '66'),
(327, 3, '-17');

-- --------------------------------------------------------

--
-- Table structure for table `development`
--

CREATE TABLE `development` (
  `id` int(11) NOT NULL,
  `contacts_id` int(11) NOT NULL,
  `have_website` varchar(255) DEFAULT NULL,
  `website_type` varchar(255) DEFAULT NULL,
  `website_value` varchar(255) DEFAULT NULL,
  `website_link` varchar(255) DEFAULT NULL,
  `website_dev_date` varchar(255) DEFAULT NULL,
  `have_crm` varchar(255) DEFAULT NULL,
  `crm_type` varchar(255) DEFAULT NULL,
  `crm_dev` varchar(255) DEFAULT NULL,
  `crm_name` varchar(255) DEFAULT NULL,
  `erp_name` varchar(255) DEFAULT NULL,
  `crm_dev_date` varchar(255) DEFAULT NULL,
  `agency_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `development`
--

INSERT INTO `development` (`id`, `contacts_id`, `have_website`, `website_type`, `website_value`, `website_link`, `website_dev_date`, `have_crm`, `crm_type`, `crm_dev`, `crm_name`, `erp_name`, `crm_dev_date`, `agency_name`) VALUES
(7, 7, 'oui', 'Vitrine', '10', 'http://www.ospadetente.fr', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'multimedia'),
(9, 9, 'oui', 'Vitrine', '10', 'https://www.facebook.com/mden.constructions/', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(10, 10, 'oui', 'Vitrine', '0', 'https://www.zoucreations.fr', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(12, 12, 'oui', 'E-commerce', '10', 'https://www.belice-boutique.com/', '2019', 'oui', 'ERP', 'interne', NULL, NULL, '2019', 'interne'),
(13, 13, 'oui', 'Vitrine', '10', 'http://spadesiles.fr', '2008', NULL, NULL, NULL, NULL, NULL, NULL, 'ideagraphique'),
(14, 14, 'oui', 'Vitrine', '12', 'www.marielauregriffe.com', '2019', NULL, NULL, NULL, NULL, NULL, NULL, 'wix.com'),
(15, 15, 'oui', 'E-commerce', '10', 'http://www.boucherie-thiessart.fr/', '2016', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(16, 16, 'oui', 'Vitrine', '10', 'http://lespaniersdenicolas.fr/', '2017', NULL, NULL, NULL, NULL, NULL, NULL, 'freelance'),
(19, 19, 'oui', 'E-commerce', '10', 'http://www.soinsonaturels.fr', '2014', NULL, NULL, NULL, NULL, NULL, NULL, 'opsites'),
(20, 20, 'oui', 'E-commerce', '10', 'http://www.cl-cestlocal.fr/', '2019', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(22, 22, 'oui', 'Vitrine', '10', 'http://club.quomodo.com', '2010', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(23, 23, 'oui', 'Vitrine', '10', 'solocal.fr', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(24, 24, 'oui', 'Vitrine', '10', 'http://bilandecompetencesaube.fr/', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'freelancur'),
(25, 25, 'non', NULL, NULL, NULL, NULL, 'oui', 'CRM-ERP', 'Sage', NULL, NULL, '2019', NULL),
(26, 26, 'oui', 'Vitrine', '100', 'https://bilandecompetencesaube.fr', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'freelancer'),
(27, 27, 'oui', 'Vitrine', '10', 'www.estella.fr', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(28, 28, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 29, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 30, 'oui', 'E-commerce', '10', 'https://www.belice-boutique.com/', '2018', 'oui', 'ERP', 'Oracle', NULL, NULL, '2018', 'interne'),
(31, 31, 'oui', 'Vitrine', '10', 'https://madeleineaime.fr/', '2017', NULL, NULL, NULL, NULL, NULL, NULL, 'Interne'),
(32, 32, 'oui', 'Vitrine', '20', 'http://integra-live.com', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'stagaire'),
(33, 33, 'oui', 'Vitrine', '10', 'www.rigauxpeinture.fr', '2018', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(34, 34, 'oui', 'Vitrine', '10', 'cuisine.fr', '2019', NULL, NULL, NULL, NULL, NULL, NULL, 'kao'),
(35, 35, 'oui', 'Vitrine', '10', 'www.espacebeaute.fr', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(36, 36, 'oui', 'Vitrine', '10', 'www.f2d.fr', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'page jaune'),
(37, 37, 'oui', 'Vitrine', '20', 'https://www.sasu-f2d.fr/', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'solocal'),
(38, 38, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 39, 'oui', 'Vitrine', '20', 'www.', '2018', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(40, 40, 'oui', 'Vitrine', '10', 'www.mazaya.fr', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(41, 41, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 42, 'non', NULL, NULL, NULL, NULL, 'non', NULL, NULL, NULL, NULL, NULL, NULL),
(43, 43, 'non', NULL, NULL, NULL, NULL, 'non', NULL, NULL, NULL, NULL, NULL, NULL),
(44, 44, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 45, 'oui', 'Vitrine', '10', 'www.club.quomodo.com', '2009', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(46, 46, 'oui', 'Vitrine', '10', 'www.integrat-live.com', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(47, 47, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 48, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 49, 'oui', 'E-commerce', '10', 'www.ng-musique.com', '2018', NULL, NULL, NULL, NULL, NULL, NULL, 'inetrne'),
(50, 50, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 51, 'oui', 'Vitrine', '10', 'www', '1999', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(52, 52, 'oui', 'Vitrine', '10', 'intermarché.com', '2019', 'non', NULL, NULL, NULL, NULL, NULL, 'agence'),
(53, 53, 'oui', 'Vitrine', '20', 'http://www.intermarche.com/enseigne/services/stations-services', '2019', 'non', NULL, NULL, NULL, NULL, NULL, 'interne'),
(54, 54, 'oui', 'Vitrine', '1000', 'https://www.intermarche.com/', '2019', 'non', NULL, NULL, NULL, NULL, NULL, 'mohsen'),
(55, 55, 'oui', 'E-commerce', '10', 'carrefour-du-net.com', '2006', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(56, 56, 'oui', 'Vitrine', '50', 'carrefour-du-net.com', '2006', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(57, 57, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 58, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 59, 'oui', 'Vitrine', '100', 'www.mazaya.fr', '2019', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(60, 60, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 61, 'oui', 'Vitrine', '100', 'www.', '2019', NULL, NULL, NULL, NULL, NULL, NULL, 'axomédia'),
(62, 62, 'oui', 'E-commerce', '100', 'www.csq.com', '2017', NULL, NULL, NULL, NULL, NULL, NULL, 'incom'),
(63, 63, 'oui', 'Vitrine', '20', 'www.soliteco.fr', '2018', NULL, NULL, NULL, NULL, NULL, NULL, 'incom'),
(64, 64, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 65, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 66, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 67, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 68, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 69, 'oui', 'Vitrine', '10', 'www.mazaya.fr', '2019', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(70, 70, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 71, 'oui', 'Vitrine', '10', 'https://www.secretgarden109.fr/', '2019', NULL, NULL, NULL, NULL, NULL, NULL, 'Freelance'),
(72, 72, 'oui', 'Vitrine', '111', 'www.cuisine.fr', '2018', NULL, NULL, NULL, NULL, NULL, NULL, 'kao'),
(73, 73, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 74, 'oui', 'E-commerce', '30', 'www.bjorka-design.com', '2014', NULL, NULL, NULL, NULL, NULL, NULL, 'decowest'),
(75, 75, 'oui', 'E-commerce', '20', 'www.www.bjorka-design.com', '2014', NULL, NULL, NULL, NULL, NULL, NULL, 'decowest'),
(76, 76, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(77, 77, 'oui', 'Vitrine', '10', 'https://www.viasignaletique.fr/', '2019', NULL, NULL, NULL, NULL, NULL, NULL, 'saxomediat'),
(78, 78, 'oui', 'Vitrine', '20', 'https://www.jlavitrail.com', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'freelance'),
(79, 79, 'oui', 'E-commerce', '20', 'www.ambc.fr', '2013', NULL, NULL, NULL, NULL, NULL, NULL, 'io'),
(80, 80, 'oui', 'E-commerce', '10', 'www.ambc.fr', '2013', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(81, 81, 'oui', 'Vitrine', '10', 'www.atelier-ebenisterie-antiquites.com', '2018', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(82, 82, 'oui', 'Vitrine', '10', 'www.cuisine.fr', '2017', NULL, NULL, NULL, NULL, NULL, NULL, 'kao'),
(83, 83, 'oui', 'Vitrine', '100', 'www.therapie_Camille de Marcellus.fr', '2017', NULL, NULL, NULL, NULL, NULL, NULL, 'elle meme'),
(84, 84, 'oui', 'Vitrine', '10', 'www.Camille_Marcellus.fr', '2017', NULL, NULL, NULL, NULL, NULL, NULL, 'elle meme'),
(85, 85, 'oui', 'Vitrine', '100', 'www.Camille_Marcellus.fr', '2017', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(86, 86, 'oui', 'Vitrine', '20', 'www.STUDIOVALERIAPAUL.com', '2015', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(87, 87, 'oui', 'Vitrine', '55', 'www.Camille_Marcellus.fr', '2017', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(88, 88, 'oui', 'Vitrine', '100', 'www.Atelier-Dreieck.com', '2013', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(89, 89, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(90, 90, 'oui', 'E-commerce', '10', 'https://galerie-martynoff.com/fr/', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'io'),
(91, 91, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(92, 92, 'oui', 'Vitrine', '10', 'www.cave18.fr', '2007', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(93, 93, 'oui', 'Vitrine', '100', 'www.santé-nutrition.fr', '2016', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(94, 94, 'oui', 'Vitrine', '1000', 'www.santé-nutrition.fr', '2015', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(95, 95, 'oui', 'Vitrine', '100', 'www.BELLAMINE ESTHETIQUE.com', '2017', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(96, 96, 'oui', 'Vitrine', '10', 'www..c-mon-appart.com', '2018', NULL, NULL, NULL, NULL, NULL, NULL, 'io'),
(97, 97, 'oui', 'E-commerce', '100', 'www.Murmures-dune-charmeuse.fr', '2018', NULL, NULL, NULL, NULL, NULL, NULL, 'valdoise communication'),
(98, 98, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, 99, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, 100, 'oui', 'Vitrine', '10', 'www.', '2019', NULL, NULL, NULL, NULL, NULL, NULL, 'locam'),
(101, 101, 'oui', 'Vitrine', '1', 'www.beaufils-traiteur.fr', '2013', NULL, NULL, NULL, NULL, NULL, NULL, 'phil informatique'),
(102, 102, 'oui', 'E-commerce', '100', 'www.murmures-dune-charmeuse.fr', '2017', NULL, NULL, NULL, NULL, NULL, NULL, 'valdoise communication'),
(103, 103, 'oui', 'Vitrine', '0', 'www.hhhh.fr', '2014', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(104, 104, 'oui', 'Market-place', '10', 'www.kalbass.fr', '2019', NULL, NULL, NULL, NULL, NULL, NULL, '123web'),
(105, 105, 'oui', 'Vitrine', '100', 'www.emulsion-paros.fr', '2017', NULL, NULL, NULL, NULL, NULL, NULL, 'fffff'),
(106, 106, 'oui', 'Vitrine', '100', 'www.', '2013', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(107, 107, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(108, 108, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, 109, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(110, 110, 'oui', 'Vitrine', '10', 'www.', '2018', NULL, NULL, NULL, NULL, NULL, NULL, 'locam'),
(111, 111, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, 112, 'oui', 'Vitrine', '10', 'https://www.plomberie-amicka.fr/', '2006', NULL, NULL, NULL, NULL, NULL, NULL, 'https://www.futurdigital.fr/'),
(113, 113, 'oui', 'Vitrine', '10', 'https://www.thermoline-cvc.fr', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'interne /agence'),
(114, 114, 'oui', 'Vitrine', '100', 'http://www.atelierdedrouot.fr', '2012', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(115, 115, 'oui', 'Vitrine', '10', 'https://www.thermoline-cvc.fr', '2020', NULL, NULL, NULL, NULL, NULL, NULL, 'interne/agence'),
(116, 116, 'oui', 'E-commerce', '0', 'https://amimobile.fr/', '2019', NULL, NULL, NULL, NULL, NULL, NULL, 'Interne'),
(117, 117, 'oui', 'Vitrine', '10', 'www', '2012', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(118, 118, 'oui', 'Vitrine', '20', 'www.enter-10.com', '2007', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(119, 119, 'oui', 'Vitrine', '30', 'www.enter-10.com', '2007', NULL, NULL, NULL, NULL, NULL, NULL, 'interne'),
(120, 120, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `eligibility`
--

CREATE TABLE `eligibility` (
  `id` int(11) NOT NULL,
  `contacts_id` int(11) DEFAULT NULL,
  `cpn_id` int(11) DEFAULT NULL,
  `regional_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `eligibility`
--

INSERT INTO `eligibility` (`id`, `contacts_id`, `cpn_id`, `regional_id`) VALUES
(5, 7, 79, 3),
(7, 9, 10, 3),
(8, 10, 205, 2),
(9, 12, 154, 1),
(10, 13, 205, 3),
(11, 14, 205, 2),
(12, 15, 205, 3),
(13, 16, 205, 2),
(16, 19, 205, 3),
(17, 20, 129, 2),
(19, 22, 7, 3),
(20, 26, 14, 7),
(21, 27, 2, 2),
(22, 28, 2, 2),
(23, 30, 164, 1),
(24, 32, 14, 7),
(25, 34, 6, 3),
(26, 35, 7, 3),
(27, 37, 6, 3),
(28, 38, 6, NULL),
(29, 39, 9, 3),
(30, 40, 77, 2),
(31, 41, 2, 3),
(32, 43, 155, 3),
(33, 44, 121, 2),
(34, 45, 7, 3),
(35, 48, 2, NULL),
(36, 49, 5, 7),
(37, 49, 5, 7),
(38, 49, 5, 7),
(39, 50, 18, 3),
(40, 51, 9, 3),
(41, 54, 154, 3),
(42, 55, 4, 1),
(43, 56, 2, 1),
(44, 57, 133, 2),
(45, 58, 121, 2),
(46, 59, 81, 2),
(47, 60, 14, NULL),
(48, 61, 6, 7),
(49, 62, 205, 3),
(50, 63, 205, 7),
(51, 66, 4, NULL),
(52, 68, 79, 2),
(53, 70, 9, 3),
(54, 71, 118, 2),
(55, 72, 119, 3),
(56, 73, 205, 3),
(57, 75, 122, 2),
(58, 76, 205, 3),
(59, 77, 6, 7),
(60, 78, 119, 3),
(61, 80, 116, 2),
(62, 82, 119, 3),
(63, 86, 205, 2),
(64, 87, 2, 2),
(65, 88, 1, 2),
(66, 89, 12, 2),
(67, 90, 205, 2),
(68, 91, 121, 2),
(69, 92, 205, 2),
(70, 94, 1, NULL),
(71, 95, 205, 3),
(72, 96, 4, 2),
(73, 97, 205, 2),
(74, 98, 18, NULL),
(75, 99, 1, 2),
(76, 100, 1, 2),
(77, 101, 2, 2),
(78, 102, 205, 2),
(79, 103, 118, 2),
(80, 104, 119, 2),
(81, 105, 167, 2),
(82, 106, 118, 2),
(83, 107, 12, NULL),
(84, 108, 8, NULL),
(85, 109, 121, 2),
(86, 110, 2, 2),
(87, 111, 11, NULL),
(88, 112, 5, 2),
(89, 113, 205, 2),
(90, 114, 7, 2),
(91, 115, 256, 2),
(92, 116, 117, 2),
(93, 117, 118, 2),
(94, 118, 9, 2),
(95, 119, 9, 2),
(96, 120, 6, 2);

-- --------------------------------------------------------

--
-- Table structure for table `experiences`
--

CREATE TABLE `experiences` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `experiences`
--

INSERT INTO `experiences` (`id`, `name`, `description`) VALUES
(1, 'débutant', NULL),
(2, 'semi-expérimenté', NULL),
(3, 'expérimenté', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `investment`
--

CREATE TABLE `investment` (
  `id` int(11) NOT NULL,
  `contacts_id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `transitions` longtext DEFAULT NULL,
  `budget` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `investment`
--

INSERT INTO `investment` (`id`, `contacts_id`, `service_id`, `transitions`, `budget`) VALUES
(7, 7, 31, '', '9300'),
(9, 9, 22, '', '4100'),
(10, 10, 43, '', '3000'),
(12, 12, 25, '', '20000'),
(13, 13, 43, '', '4500'),
(14, 14, 43, '', '3000'),
(15, 15, 43, '', '5300'),
(16, 16, 43, '', '3500'),
(19, 19, 43, '', '4600'),
(20, 20, 24, '', '7700'),
(22, 22, 22, '', '3600'),
(23, 23, 22, '', '3000'),
(24, 24, 22, '', '5600'),
(25, 25, 25, '', '24000'),
(26, 26, 22, '', '5000'),
(27, 27, 22, '', '3000'),
(28, 28, 22, '', '3000'),
(29, 29, 31, '', '21100'),
(30, 30, 25, '', '24600'),
(31, 31, 22, '', '1000'),
(32, 32, 22, '', '5000'),
(33, 33, 31, '', '59500'),
(34, 34, 22, '', '3500'),
(35, 35, 22, '', '3600'),
(36, 36, 22, '', '1500'),
(37, 37, 22, '', '3500'),
(38, 38, 22, '', '3500'),
(39, 39, 22, '', '4000'),
(40, 40, 31, 'Community Management (Pack standard)', '6600'),
(41, 41, 22, '', '3100'),
(42, 42, 25, '', '17000'),
(43, 43, 25, 'Site vitrine', '21500'),
(44, 44, 24, '', '5000'),
(45, 45, 22, '', '3600'),
(46, 46, 43, '', '900'),
(47, 47, 43, '', '4600'),
(48, 48, 22, '', '3000'),
(49, 49, 22, '', '3400'),
(50, 50, 22, '', '22800'),
(51, 51, 22, '', '3900'),
(52, 52, 25, '', '15300'),
(53, 53, 25, '', '15000'),
(54, 54, 25, '', '20200'),
(55, 55, 22, '', '3300'),
(56, 56, 22, 'Catalogue', '3000'),
(57, 57, 24, '', '20000'),
(58, 58, 24, '', '5000'),
(59, 59, 31, '', '15000'),
(60, 60, 22, '', '5000'),
(61, 61, 22, '', '3500'),
(62, 62, 43, '', '3800'),
(63, 63, 43, '', '3800'),
(64, 64, 31, '', '5100'),
(65, 65, 31, '', '5000'),
(66, 66, 22, '', '3300'),
(67, 67, 31, '', '5000'),
(68, 68, 31, '', '10000'),
(69, 69, 22, '', '5400'),
(70, 70, 22, '', '4000'),
(71, 71, 24, '', '3200'),
(72, 72, 24, '', '4000'),
(73, 73, 43, '', '4000'),
(74, 74, 31, '', '5200'),
(75, 75, 24, '', '5700'),
(76, 76, 43, '', '5700'),
(77, 77, 22, '', '3500'),
(78, 78, 24, '', '4000'),
(79, 79, 24, '', '15000'),
(80, 80, 24, '', '2000'),
(81, 81, 22, '', '3200'),
(82, 82, 24, '', '4000'),
(83, 83, 22, '', '1000'),
(84, 84, 22, '', '1300'),
(85, 85, 22, '', '4000'),
(86, 86, 43, '', '5300'),
(87, 87, 22, '', '3100'),
(88, 88, 22, '', '2900'),
(89, 89, 22, '', '4600'),
(90, 90, 43, '', '5000'),
(91, 91, 24, '', '5000'),
(92, 92, 43, '', '3400'),
(93, 93, 22, '', '500'),
(94, 94, 22, '', '1700'),
(95, 95, 43, '', '3200'),
(96, 96, 22, '', '3300'),
(97, 97, 43, '', '1500'),
(98, 98, 22, 'SEO, Application réseau sociaux, Application marketplace', '8300'),
(99, 99, 22, '', '1500'),
(100, 100, 22, '', '2900'),
(101, 101, 22, '', '3000'),
(102, 102, 43, '', '1500'),
(103, 103, 24, '', '3300'),
(104, 104, 24, '', '3500'),
(105, 105, 25, '', '30500'),
(106, 106, 24, '', '3300'),
(107, 107, 22, '', '4600'),
(108, 108, 22, '', '3800'),
(109, 109, 24, '', '5000'),
(110, 110, 22, '', '3000'),
(111, 111, 22, '', '4400'),
(112, 112, 22, '', '3400'),
(113, 113, 43, '', '4100'),
(114, 114, 22, '', '3600'),
(115, 115, 44, '', '4600'),
(116, 116, 24, '', '2800'),
(117, 117, 24, '', '3400'),
(118, 118, 22, '', '3900'),
(119, 119, 22, '', '3900'),
(120, 120, 22, '', '3500');

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `descritpion` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `name`, `descritpion`) VALUES
(1, 'utilisateur', NULL),
(2, 'editeur', NULL),
(3, 'conseiller', NULL),
(4, 'confirmateur', NULL),
(5, 'superviseur', NULL),
(6, 'admin', NULL),
(9, 'superamdin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `meetings`
--

CREATE TABLE `meetings` (
  `id` int(11) NOT NULL,
  `contacts_id` int(11) NOT NULL,
  `zoom_id` varchar(255) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `date` datetime NOT NULL,
  `link` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `meetings`
--

INSERT INTO `meetings` (`id`, `contacts_id`, `zoom_id`, `topic`, `type`, `date`, `link`, `password`) VALUES
(6, 7, '93480631303', 'Sandrine  BAROUDI, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-09 07:00:00', 'https://zoom.us/j/93480631303?pwd=TFhUbXJqaHdzTDVJdHBuc3Juck5Cdz09', 'gJ6K8t'),
(8, 9, '91314707357', 'malik Djaber, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-03 16:00:00', 'https://zoom.us/j/91314707357?pwd=RFcyTytLNVdRUVBqVnI2MnlCeHFlUT09', 'q86khS'),
(9, 10, '91790098969', 'Valerie le seigneur, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-02 12:30:00', 'https://zoom.us/j/91790098969?pwd=bUxERHdQQStXbmpYUEFLUXIweEg2Zz09', 'R7sGy7'),
(10, 12, '92086492915', 'Cyprien BRUEL, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-06 09:00:00', 'https://zoom.us/j/92086492915?pwd=aXlWWHRNOFhMaGxYY3FKbUdwMTEvUT09', '9DShtR'),
(11, 13, '97286321066', 'Elodie  LAMBERT, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-03 17:00:00', 'https://zoom.us/j/97286321066?pwd=Ky8wNkpLcU5SMFZsZUZBWkFOQnhOUT09', '8buC2u'),
(12, 14, '95368752802', 'Marie-Laure Griffe, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-06 08:00:00', 'https://zoom.us/j/95368752802?pwd=Z3JrZGozUkE3bDJTVFcyRFdlczhMdz09', 'BRVV36'),
(13, 15, '94368531685', 'Yassmin THIESSART, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-07 12:30:00', 'https://zoom.us/j/94368531685?pwd=M1gvZjBWblVoelg2YVRFZ1R0c0U5QT09', '2rt5Zr'),
(14, 16, '91782812214', 'pristillia matins, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-03 08:18:22', 'https://zoom.us/j/91782812214?pwd=Z2c0cEkwZGlDbFh6V01WRmZrZmFEZz09', 'fN5XRb'),
(17, 19, '91998571217', 'cecile brian, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-13 11:00:00', 'https://zoom.us/j/91998571217?pwd=ZXNkVmRtaFpTQk9NZ29YMUtKRWo5QT09', 'UXF8yN'),
(18, 20, '98343105283', 'luca delphine, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-06 09:30:00', 'https://zoom.us/j/98343105283?pwd=emZrRkJIOTExd1Z1V2JQRGVvdzl3dz09', 'jN80GR'),
(20, 22, '96454984527', 'benoit guilley, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-06 12:00:00', 'https://zoom.us/j/96454984527?pwd=MTJNeWx4dnF5NTBkSVpncDZHRERZQT09', '414ibt'),
(21, 25, '99986956710', 'khalil mecha, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-22 08:30:00', 'https://zoom.us/j/99986956710?pwd=Uis5VGJFZHdJTHh1MjBwMHk1bWFkUT09', '8WK20t'),
(22, 26, '93443236945', 'Sadia Duranton, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-13 09:00:00', 'https://zoom.us/j/93443236945?pwd=MjlqRHRUbkdRSVhqSDNWYjloeVVQQT09', 'UsK2SM'),
(23, 27, '97859625636', 'estella ngono, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-08 13:00:00', 'https://zoom.us/j/97859625636?pwd=TnVZMjN0T2xGZlRJS2dwa2ptTldjQT09', '19sakV'),
(24, 28, '91688726463', 'job  id, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-23 02:00:00', 'https://zoom.us/j/91688726463?pwd=ejJ2UFJWanlKSjFPZFc5S3VRcXNxdz09', '32bfGx'),
(25, 29, '98757600990', 'khalil mecha, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-21 02:00:00', 'https://zoom.us/j/98757600990?pwd=NnJZaTQ1aGVPZnkvNHpjNjFhRG5Jdz09', 'w9bMiT'),
(26, 30, '93062913180', 'Cyprien BRUEL, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-14 09:00:00', 'https://zoom.us/j/93062913180?pwd=Z0svcmZyUEFMWXE4KzVTMWhWRG0yUT09', 'H9sfVz'),
(27, 32, '92182144471', 'Olivier  DAMBRINE, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-09 10:00:00', 'https://zoom.us/j/92182144471?pwd=OEFjZHVFNGF3N1FGa0pnSVVObXVGdz09', '8FDSyE'),
(28, 34, '94748438275', 'Vincent  LAURENT, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-17 07:00:00', 'https://zoom.us/j/94748438275?pwd=QjRxT0k5dEdqTXVwRmUzTWx3V3Nidz09', 'FYJhy6'),
(29, 35, '96602740861', 'Cathrine descamps, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-10 09:00:00', 'https://zoom.us/j/96602740861?pwd=cUlMZ3lFMjU3ZVV1a1dZS25JK0dIUT09', 'eT5qiw'),
(30, 37, '98186634125', 'corimne koryne, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-10 08:00:00', 'https://zoom.us/j/98186634125?pwd=aG5ya1dPMzhrLy9MZ2I2T05IeFIwQT09', 'GxTK2F'),
(31, 38, '99747992639', 'Veronique Doumas, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-09 07:30:00', 'https://zoom.us/j/99747992639?pwd=Ny9CZGovMG0yd211YzNVYkxRUTZZQT09', '1ryHF5'),
(32, 39, '95630881081', 'Jacques Delaporte, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-09 13:00:00', 'https://zoom.us/j/95630881081?pwd=UkZ4M05pRVRwWHJialc3WHJtQnlqUT09', '1emq9w'),
(33, 40, '96383839173', 'Bilal Tatfi, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-13 12:00:00', 'https://zoom.us/j/96383839173?pwd=NXFmeVBDWXhydTFHMWhmS3VHNXVsUT09', 'gX5fcC'),
(34, 41, '98997315442', 'GERARD HAAS, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-13 06:00:00', 'https://zoom.us/j/98997315442?pwd=eVAvbjkwZndmT3BFK1lEMkRuWjI5Zz09', 'JS9Nez'),
(35, 43, '95801170347', 'AYDIN  Yagmur, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-09 08:00:00', 'https://zoom.us/j/95801170347?pwd=UGJ2MSs3UUxqdDA5UVlSU1ExZFhTZz09', '9RwrqJ'),
(36, 44, '92865908246', 'jaques machner, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-09 15:00:00', 'https://zoom.us/j/92865908246?pwd=MUs2QklSVmwzNFh1Q3VrV1NTMnRidz09', 'nHGm76'),
(37, 45, '96531803883', 'Benoit Guilley, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-13 12:00:00', 'https://zoom.us/j/96531803883?pwd=QjYzRDhEVENpQzR6QXQ3UHh2QnhiQT09', 'Yja1YB'),
(38, 48, '91030809356', 'gjhg gjgjg, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-25 03:30:00', 'https://zoom.us/j/91030809356?pwd=cEQ2Y3ViWU13OXZMOXV4RnMwNTJ1UT09', 'M2BxB6'),
(39, 49, '94294718722', 'Gerald  GONTIER, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-10 10:57:29', 'https://zoom.us/j/94294718722?pwd=Ny80WkxOYlFmMDUzOXJWOHZDQ3RzUT09', 'NSR6M1'),
(40, 50, '91528014492', 'Michel  Legrand, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-16 08:00:00', 'https://zoom.us/j/91528014492?pwd=Tm53ZndtMHp5K2wwcVBuV2ZnZGV6UT09', 'GJqve0'),
(41, 51, '92566687550', 'Anselin thibaut, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-13 08:00:00', 'https://zoom.us/j/92566687550?pwd=QWFyTzlibWpoamx4ZmE3WGl0UGlsZz09', 'XK6jNq'),
(42, 54, '99098911649', 'julien Flomant, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-13 13:00:00', 'https://zoom.us/j/99098911649?pwd=VzV1eTFLQlp1WlNJNDVKdmpqQjF6QT09', 'bYDV00'),
(43, 55, '94687119181', 'pascale besranard, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-20 12:00:00', 'https://zoom.us/j/94687119181?pwd=U0FIdnAxbXdJNXRiNG9CejVZOHlsdz09', 'BG0ff2'),
(44, 56, '95762049828', 'pascal besnard, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-20 12:00:00', 'https://zoom.us/j/95762049828?pwd=OTZBMEc4UzAxeDUzMDN0NFJtYzY1QT09', '6B2vku'),
(45, 57, '97199392454', 'jeramie malais, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-20 15:30:00', 'https://zoom.us/j/97199392454?pwd=alRvQkplREJWRFlJLzM1cTVoZU9XUT09', '2pPnwF'),
(46, 58, '99814422631', 'jaques machner, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-13 15:00:00', 'https://zoom.us/j/99814422631?pwd=b2pZaFlxYmg3N2pDWTIvZHJiNDNKUT09', 'KNVeZ5'),
(47, 59, '96581990586', 'Bilal Tatfi, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-14 13:00:00', 'https://zoom.us/j/96581990586?pwd=RGpOZkcyeVhCdmZTMm94SmNsWSt6UT09', 'UQ0A21'),
(48, 60, '96653737751', 'GERARD  HAAS, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-13 16:00:00', 'https://zoom.us/j/96653737751?pwd=SHFtVEkzU211SmRuczdMYXZETENyQT09', 'L05QAi'),
(49, 61, '96942579623', 'grafteaux vincent, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-16 09:00:00', 'https://zoom.us/j/96942579623?pwd=ejJUSEFUMGR5SCtuOThYdTUybEEzZz09', '9HDG2G'),
(50, 62, '96579429311', 'vivien Legrand, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-14 17:00:00', 'https://zoom.us/j/96579429311?pwd=SHVHL2Z6UkVOWXNHbVNOR0xXNXp1UT09', 'EWbc7s'),
(51, 63, '98119613971', 'SEBASTIEN VERDRU, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-15 08:00:00', 'https://zoom.us/j/98119613971?pwd=R1B1RWRKK29HN2tSK3BLR09XKzJsdz09', '5Nd3KW'),
(52, 66, '98821734651', 'khalil mecha, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-29 07:00:00', 'https://zoom.us/j/98821734651?pwd=NlBpK0RHYU9mNFhwVXlJbnQxWTdZUT09', 'ER7V4Y'),
(53, 68, '97677652466', 'bilal Tatfi, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-14 14:00:00', 'https://zoom.us/j/97677652466?pwd=Q1FtczBnSzBEQkJnOFZveW5sOWVMUT09', 'eJ42dy'),
(54, 70, '94454010799', 'Gerrard Hass, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-15 08:46:57', 'https://zoom.us/j/94454010799?pwd=R0cxU3pCTTdac0xPaElWdjNWS2MvZz09', 'q4RM2t'),
(55, 71, '91017559325', 'taieb jenathan, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-17 08:30:00', 'https://zoom.us/j/91017559325?pwd=UjdSc2xodlRqY3RTK3JkbUhDTTVkZz09', 'j81xRD'),
(56, 72, '97114558975', 'Vincent LAURENT, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-17 07:00:00', 'https://zoom.us/j/97114558975?pwd=RUdxeSt2enVUV1ZSTUtsemxWQXc5dz09', 'aLuz5f'),
(57, 73, '96761973012', 'Egho Routier, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-16 10:00:00', 'https://zoom.us/j/96761973012?pwd=THNNNWI2RFdkZGlQaEh3NDdJbUpHUT09', 'GjAVB7'),
(58, 75, '91038737123', 'isac knafo, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-20 08:00:00', 'https://zoom.us/j/91038737123?pwd=dEpYdXdScTNBZXdGeEVnai9mZUZOdz09', 'ya6dt4'),
(59, 76, '92831522502', 'Hugo Routier, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-16 10:00:00', 'https://zoom.us/j/92831522502?pwd=VWUwQkhkVTNBRm5VS0NPcU9PVkVzUT09', 'iqKQ8A'),
(60, 77, '91784947538', 'vincent Grafteaux, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-16 09:00:00', 'https://zoom.us/j/91784947538?pwd=U0ZFanVlZytSRUhiR2lTSWxjWEJMQT09', 'ZHyjf7'),
(61, 78, '99010769565', 'Jessica  Lambinet, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-28 16:00:00', 'https://zoom.us/j/99010769565?pwd=Wm8ydERnSEdhYThRN1BUQmVzc3ZOQT09', '73jyL3'),
(62, 80, '91601498814', 'sabiene  todorvic, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-17 09:00:00', 'https://zoom.us/j/91601498814?pwd=aUp6L1Vaa25KWjBDc0Fpd1d2VVdqQT09', 'Kff5dL'),
(63, 82, '96907930479', 'Vincent  LAURENT, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-17 15:00:00', 'https://zoom.us/j/96907930479?pwd=QVo5alNTVlE4SWcxZXptTkxsUlJwUT09', 'AxXXA3'),
(64, 84, '99678983221', 'Camille  de Marcellus, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-27 12:30:00', 'https://zoom.us/j/99678983221?pwd=N2VselRFVWdLbzNoNVJ5V20zcWJnQT09', 'H6ExUH'),
(65, 85, '98500107807', 'Camille de Marcellus, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-27 12:30:00', 'https://zoom.us/j/98500107807?pwd=dFNaQWNuTEh0eUVkY2t5bHdteEI4Zz09', '45D2Ve'),
(66, 86, '98551256114', 'VALERIA PAUL (rappel), Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-27 08:00:00', 'https://zoom.us/j/98551256114?pwd=NjJtNFBwcGIxaG9kcmlONFFVK1d0QT09', 'cXdb7q'),
(67, 87, '98909885707', 'Camille de Marcellus, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-27 12:30:00', 'https://zoom.us/j/98909885707?pwd=ZjRuemJ4RGxUalZKNGlTNW5TUzhJZz09', 'i54z0y'),
(68, 88, '93951756025', 'marion ******, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-23 07:00:00', 'https://zoom.us/j/93951756025?pwd=OGhCT1B3TmF0bmhBTHVtNmo0RUZtZz09', 'LG5sEr'),
(69, 89, '91559424538', 'christophe TISSERAND, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-27 12:00:00', 'https://zoom.us/j/91559424538?pwd=WnhzQ0F3VGRPbnNlNHQ4MG0rdnAwdz09', 'iJJc4W'),
(70, 90, '93057822034', 'Simon Martynoff, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-27 14:00:00', 'https://zoom.us/j/93057822034?pwd=ZlZZOGF5Z3BnUDlueFJ6VjdqRHZaQT09', 'S0idKA'),
(71, 91, '91205007782', 'Salvatore FORTE, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-28 08:00:00', 'https://zoom.us/j/91205007782?pwd=YkNkVXRjOWFtVXI0OHpWdXArVW15Zz09', 'P6WMhs'),
(72, 92, '92170990146', 'Eric  DEGREEF, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-28 12:00:00', 'https://zoom.us/j/92170990146?pwd=aUZ2ZlFEcXRLOVk2dnhGSnRTWVo0Zz09', '1Mb3Uz'),
(73, 94, '91631262140', 'stéphanie Duré, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-29 09:00:00', 'https://zoom.us/j/91631262140?pwd=U0dTWUV5RVVySWxjK0pLL2V2SXkxZz09', 'HhPX71'),
(74, 95, '97281291989', 'richet guwennaelle, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-30 11:30:00', 'https://zoom.us/j/97281291989?pwd=Znk2YUs0MEFSVDFpSW5DdVd0MXYrQT09', 'kHDdK9'),
(75, 96, '99787022844', 'Gerard  BERNET URIETA, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-28 11:00:00', 'https://zoom.us/j/99787022844?pwd=OUd2ZU9EVFJYYVpoSXozdlRCRUlEQT09', 'ScD2qa'),
(76, 97, '94184413531', 'plin anita, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-29 10:00:00', 'https://zoom.us/j/94184413531?pwd=TVJOTjJFMDRWb3IyT2U0WlFFVDBGUT09', 'E68f6Z'),
(77, 98, '99406878297', 'test test, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-10-06 05:30:00', 'https://zoom.us/j/99406878297?pwd=aVdZZjlYYzBsa1R3MUdVYllZSUFHUT09', 'yvNxC3'),
(78, 99, '97220774827', 'jean  joyeux, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-29 08:30:00', 'https://zoom.us/j/97220774827?pwd=ZVh1RTFBczR1c2dKZTlsSTd2NHlyQT09', 'e3vzXf'),
(79, 100, '91612370789', 'joseph  bassama, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-30 11:00:00', 'https://zoom.us/j/91612370789?pwd=Mk5Sc2d6Vk5aZ0JXcFpWR3Z5RW4zUT09', 'LKAvt5'),
(80, 101, '94220997592', 'Beaufils romain, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-10-05 13:00:00', 'https://zoom.us/j/94220997592?pwd=Ykt0R2RnSEkvbDM5S082aGk2NXMwQT09', 'u718xc'),
(81, 102, '92905054999', 'anita plin, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-30 10:00:00', 'https://zoom.us/j/92905054999?pwd=ZENDSGxFS2xHS2ZrTE1GdG43c2txdz09', 'R2HeQy'),
(82, 103, '91711834173', 'méggie garcelon, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-10-08 12:30:00', 'https://zoom.us/j/91711834173?pwd=WFUrTXlyRHBTWERzQlJUSjNFUVNUZz09', '71azGN'),
(83, 104, '99175955358', 'Michel Oliphar, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-29 14:00:00', 'https://zoom.us/j/99175955358?pwd=NVppbWZDSWhhbG4vTS83cW1UZ25Rdz09', '8mFHqn'),
(84, 105, '93274909929', 'arnaud malicet, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-10-08 07:00:00', 'https://zoom.us/j/93274909929?pwd=RS9Dd0ZuN3lsZDVOZ1JxZUFaQWduUT09', 'eQ2GdP'),
(85, 106, '95615155209', 'méggie garcelon, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-10-01 12:30:00', 'https://zoom.us/j/95615155209?pwd=dDRkcm5paDhtTjdoQnpZYkpVc1VQZz09', '0hTS4a'),
(86, 107, '96245202462', 'christophe  TISSERAND, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-29 08:30:00', 'https://zoom.us/j/96245202462?pwd=SzhKN09jcld4S2M2eDJoRVI1eWt2Zz09', '7BqK3W'),
(87, 108, '95278209873', 'Remy  MARGINET, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-30 08:30:00', 'https://zoom.us/j/95278209873?pwd=NlJTbis1RXdpNnYrQUVVaFVKWjVpZz09', 'd8XLmq'),
(88, 109, '97486896941', 'christophe  TISSERAND, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-29 15:30:00', 'https://zoom.us/j/97486896941?pwd=bHB1aVNIYlFaYUVvY3c0R0FLb21Idz09', '3e83Uu'),
(89, 110, '99731903823', 'Josteph Bassama, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-30 11:00:00', 'https://zoom.us/j/99731903823?pwd=b3Rzckd0M1hZOGZxeW8rc1lMZUVtdz09', 'BDX44C'),
(90, 111, '97888088380', 'sofyane smida, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-09-30 08:45:00', 'https://zoom.us/j/97888088380?pwd=MmpxLzRkOUxsbGZ4YkpWaWx5K2F6QT09', 'QW00mM'),
(91, 112, '99082454717', 'M Mickael   TIRONI, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-10-01 16:00:00', 'https://zoom.us/j/99082454717?pwd=NVZpZjBGR2FaTDdtTGlRbHFIV2RUdz09', 'E4Nj6P'),
(92, 113, '94618619825', 'lancry esaac, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-10-04 15:00:00', 'https://zoom.us/j/94618619825?pwd=UGhIZHM3WXBnWmQrd0dtRnJhcGFIZz09', 'pCAv4H'),
(93, 114, '92475138149', 'janina kubicki, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-10-05 06:00:00', 'https://zoom.us/j/92475138149?pwd=RkY3Q0NkWHFNcEk3Q2Voa2pRanZ6dz09', 'Gt6CKg'),
(94, 115, '98894096912', 'lancry esaac, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-10-04 15:00:00', 'https://zoom.us/j/98894096912?pwd=Skl4S1YxNkdJcTgxWnVaeEZHcVlDUT09', 'ZS4ELi'),
(95, 116, '91875411260', 'Khalil Ouhammou, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-10-04 15:00:00', 'https://zoom.us/j/91875411260?pwd=OWVwMHJYZGlKVHZILzRsOGJPT2VoQT09', 'v65PXH'),
(96, 117, '99473623079', 'méggie  garcelon, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-10-06 12:30:00', 'https://zoom.us/j/99473623079?pwd=TUJvbGRUTTdHMlZ4QWZZTlFUdnM3dz09', 'syC17M'),
(97, 118, '97290763669', 'marie noelle, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-10-07 08:00:00', 'https://zoom.us/j/97290763669?pwd=NWxja1pCbUVxRmlyTFVETXZJdUNJQT09', 'g3ptwa'),
(98, 119, '94619228349', 'Marie-Noelle  DIEHL RODRIGUES, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-10-07 08:00:00', 'https://zoom.us/j/94619228349?pwd=YktiSFZhNjM5cVY5clp4WU90cWw3UT09', 'g3i0kX'),
(99, 120, '99261220452', 'SOUBIGOU GUY, Rendez-vous Avec votre conseiller Numerique Pour votre projet', 'Entretien vidéo conférence', '2021-10-04 13:00:00', 'https://zoom.us/j/99261220452?pwd=THdZd2krZjdBc1JyaTdjZEM0cndEUT09', '0f6zui');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0708e48a82c5b1f18b268794e65a7d3fc7c69e8a4dd7a168f9ef2ca4fa0c99f2f09872b5af6ad370', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-23 06:32:49', '2021-09-23 06:32:49', '2022-09-23 08:32:49'),
('09ff2439778a43661a06b8a64bedb93e76c8f9a9ab266dbb1c78d6a86ceb4d4578fe86c2320ce81a', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-21 06:58:10', '2021-09-21 06:58:10', '2022-09-21 08:58:10'),
('12289a3ce1c82d69f01f1cb055d25555f59b08944b13ef0d8efbd03b1cfb16503815b3466021b266', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-15 06:35:44', '2021-09-15 06:35:44', '2022-09-15 08:35:44'),
('133ffeafe891c9a2c4cccabf0d85651d0706f94f706e3eb1095cb675b6507dcfc9dd7705a150bbe7', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-22 08:23:37', '2021-09-22 08:23:37', '2022-09-22 10:23:37'),
('13a5976aa7562bb8218b9521e68fb85f90f7a0ed65c8fe100e7c5475ccbc4b874deb0739c75cd3e5', 10, 1, 'PersonalAccessToken', '[]', 1, '2021-09-18 07:36:25', '2021-09-18 07:36:25', '2022-09-18 09:36:25'),
('165f39c1888caf78eeb1e47295d69b0ee88115cb2a5c790cb67ad92763bd5d7e99ef83dfd45bac4b', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-14 06:46:53', '2021-09-14 06:46:53', '2022-09-14 08:46:53'),
('19eca02854a630e411a0fc646a7b66ba4f4bca05ab890c6207df62ae8638671d5711ff96b8529465', 4, 1, 'PersonalAccessToken', '[]', 1, '2021-09-14 06:49:38', '2021-09-14 06:49:38', '2022-09-14 08:49:38'),
('1d2f7b5126db2a38f24064297b065913e1da24d27d3981ce35a011bef67fca51bbdd9398bc0964e9', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-27 05:48:51', '2021-09-27 05:48:51', '2022-09-27 07:48:51'),
('37395da1288c75e754fc0a7b322e68226c1ec0352387e9bd7e9d7e8aa7fa78da595f0b627843dc6e', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-21 06:07:59', '2021-09-21 06:07:59', '2022-09-21 08:07:59'),
('510bfebffbd15627bbe27598cc4234c0420a00cd60e57983e0086c5855d07995cca5fa54f076ce06', 4, 1, 'PersonalAccessToken', '[]', 0, '2021-09-17 08:42:09', '2021-09-17 08:42:09', '2022-09-17 10:42:09'),
('512d8224e3e6cfa4c8406fe52ec54d54d29e33f1c8d1f618c93f0975d2d91bf59d5bd998bff9f58c', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-29 06:51:16', '2021-09-29 06:51:16', '2022-09-29 08:51:16'),
('53c1a0cdb81957d07c57e21ef5cfe396a037bc59ac269e65d1ba0ea717ba0f9e6d4b9c3872472d53', 5, 1, 'PersonalAccessToken', '[]', 0, '2021-10-04 05:54:41', '2021-10-04 05:54:41', '2022-10-04 07:54:41'),
('5ba6f30974ffb1013c3e745edb236adbb77a4aaf8b92eed67d4be804c0e47a981cf306eeadc25404', 2, 1, 'PersonalAccessToken', '[]', 0, '2021-09-14 12:12:19', '2021-09-14 12:12:19', '2022-09-14 14:12:19'),
('5f4b75349a57c0e648509b1099886aa7c65e653e87301f63182d9b5fa7001664bc99d8c7f0f13934', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-15 10:37:01', '2021-09-15 10:37:01', '2022-09-15 12:37:01'),
('62d920b4ba208e9a7b03b471b5d24de5228967ed820fa7a49c8cc4547579d7a9b2b74f93f9bde4b4', 10, 1, 'PersonalAccessToken', '[]', 0, '2021-09-18 07:39:53', '2021-09-18 07:39:53', '2022-09-18 09:39:53'),
('6a65eea5f493711bd0f769a4cf919cad101e50b259591bb43cee9bd0f83497a6db0f381662345fe9', 6, 1, 'PersonalAccessToken', '[]', 0, '2021-09-14 06:42:59', '2021-09-14 06:42:59', '2022-09-14 08:42:59'),
('74bd17629dab6036cf2a530b170c12798ae850974a9726816e8ed4c98cfaf25c2b118d088a74a819', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-23 11:37:23', '2021-09-23 11:37:23', '2022-09-23 13:37:23'),
('82247c023c19e1c97bba8afa7a0674098f294e9d30f6f392505cc78703ec39f204824a4df989a886', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-21 12:40:55', '2021-09-21 12:40:55', '2022-09-21 14:40:55'),
('892898cae54811a1e3032abc144d75c52abf5b8b1feaa1a2fec52ce0944ef58df127837009cbb522', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-24 06:19:35', '2021-09-24 06:19:35', '2022-09-24 08:19:35'),
('8d6b7d5a8e69e210b306af6afe6b1bcc4e13e3073652b44b73baf7b992c1b23ac76411a030c26f17', 4, 1, 'PersonalAccessToken', '[]', 1, '2021-09-16 10:23:47', '2021-09-16 10:23:47', '2022-09-16 12:23:47'),
('8e6d0bfa10b928d8908e479e65cadc5b1f1f1331a9d2583a56aacb005ca577e2cd50719287958a64', 3, 1, 'PersonalAccessToken', '[]', 1, '2021-09-14 06:43:43', '2021-09-14 06:43:43', '2022-09-14 08:43:43'),
('92530003cfb20ab52061ae0d88ca01072d80ff91ce8d5416fa2bf843858f3c4ca5653b6dab6ce60b', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-20 07:56:26', '2021-09-20 07:56:26', '2022-09-20 09:56:26'),
('97cd3fbc16b864c0a05880ac9ad351f65f6fbc4e5365fc457349aa930f0d648a8a1278b8690e87cf', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-15 06:54:12', '2021-09-15 06:54:12', '2022-09-15 08:54:12'),
('9fb19df0dcff46ac47bcd44d1faa3d9ba259e0e1e0de97ac6c1d42a2eec21a248ebc0cba108e0bad', 3, 1, 'PersonalAccessToken', '[]', 1, '2021-09-16 13:51:19', '2021-09-16 13:51:19', '2022-09-16 15:51:19'),
('a0da632a3a2a5188bd8d6fd6775a2c1359abdae45be24ab5cf3c4fcfff19cda1670f0396682ca6b4', 4, 1, 'PersonalAccessToken', '[]', 1, '2021-09-20 08:44:36', '2021-09-20 08:44:36', '2022-09-20 10:44:36'),
('a68e659e9d0b15d7e71dd6514c1d3ab763a6e6622fa9d63d2088f463fffa626b2611fedaae15dff0', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-16 10:23:18', '2021-09-16 10:23:18', '2022-09-16 12:23:18'),
('adee54dfeaa41a1e9f67a0acf0f9b39a79583fc594af5f73e838aa8acb05b5041a76362ca3d162a5', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-10-01 06:14:28', '2021-10-01 06:14:28', '2022-10-01 08:14:28'),
('aeebe1b473c016f5288a01ecfb92071c9e03484d5bbab7a20c94c7ede1abb1b2a01799d4e29acacc', 11, 1, 'PersonalAccessToken', '[]', 0, '2021-09-29 11:06:50', '2021-09-29 11:06:50', '2022-09-29 13:06:50'),
('af25a82c90bcc7f35c62d7f8c8f004cb9a82d0cab3e9cf63d544bc35d386a25d94818c5fbccd9ba4', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-29 11:41:30', '2021-09-29 11:41:30', '2022-09-29 13:41:30'),
('b25c2240b33a5676e5ace7a0af16d9ae4ea2ab1d07f8d7dfabc74cb1d768711160fe642040fce3a5', 1, 1, 'PersonalAccessToken', '[]', 1, '2021-09-14 04:50:33', '2021-09-14 04:50:33', '2022-09-14 06:50:33'),
('ba710d3a81ecba02930a157c079e334ea292a917ee560687119eabd67c882dbd6d821e8ef4b5123c', 9, 1, 'PersonalAccessToken', '[]', 0, '2021-09-18 07:36:22', '2021-09-18 07:36:22', '2022-09-18 09:36:22'),
('c3b6e5c3b51f994a27159a7f2e9e6fc5d70294550b14c3d2ee505b803fa2df09d6eeee7acca724dd', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-16 06:06:01', '2021-09-16 06:06:01', '2022-09-16 08:06:01'),
('c79819abcf033fe82f34da38fb7154e17213c23ab1e440efc405c8ef3ecc4f533e87e7ce709a9e3b', 5, 1, 'PersonalAccessToken', '[]', 1, '2021-09-30 07:30:21', '2021-09-30 07:30:21', '2022-09-30 09:30:21'),
('d2f2de0a01641ced0c22ce7a6241bb05666c397d7fc80adcf9b46d71fb708f6602c9b71d4e1582d9', 1, 1, 'PersonalAccessToken', '[]', 1, '2021-09-14 04:52:40', '2021-09-14 04:52:40', '2022-09-14 06:52:40'),
('d77b7323e0522702c2f0432aea4f2827ca1529a83f4dbbab3448abc4263a4294533af28291061e20', 4, 1, 'PersonalAccessToken', '[]', 0, '2021-09-22 06:24:27', '2021-09-22 06:24:27', '2022-09-22 08:24:27'),
('e3ea55e1fddf29053763d50e13edbc093745913df78428fd894f59b07592f2cdc1b01b3f1cfe399a', 2, 1, 'PersonalAccessToken', '[]', 0, '2021-09-17 12:12:17', '2021-09-17 12:12:17', '2022-09-17 14:12:17'),
('eca56740120acae777923e5049852226d4d041f2adb2f818adffc4fa4bdaec69a5606b0160a7de3f', 3, 1, 'PersonalAccessToken', '[]', 0, '2021-10-01 11:10:43', '2021-10-01 11:10:43', '2022-10-01 13:10:43');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'azgjSc6klnnpidJrhOzLdKHWmAvVI5uqMHIhWvan', NULL, 'http://localhost', 1, 0, 0, '2021-07-18 19:50:01', '2021-07-18 19:50:01'),
(2, NULL, 'Laravel Password Grant Client', 'UsQI7Gso46KuMvadBKBPaxyCqV71kJfVfASHEDeg', 'users', 'http://localhost', 0, 1, 0, '2021-07-18 19:50:01', '2021-07-18 19:50:01');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-07-18 19:50:01', '2021-07-18 19:50:01');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `link`) VALUES
(1, 'home', 'https://www.linkedin.com/pages-extensions/FollowCompany?id=76078573&counter=bottom');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('novasis@gmx.com', '$2y$10$zZkT6v8q1A7JyeRRC5fKkO3UytcY7zbRClrHtsEsjEzdoRAmpWeAu', '2021-10-04 21:44:59');

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE `pictures` (
  `id` int(11) NOT NULL,
  `articles_id` int(11) DEFAULT NULL,
  `users_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `link` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pictures`
--

INSERT INTO `pictures` (`id`, `articles_id`, `users_id`, `name`, `link`) VALUES
(1, 1, 15, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(2, 1, 16, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(3, 1, 17, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(4, NULL, 18, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(5, NULL, 19, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(6, NULL, 20, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(7, NULL, 21, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(8, NULL, 22, NULL, 'tshirt1.jpg'),
(9, NULL, 23, NULL, '54018118_254184482189383_4597457088128483328_o.jpg'),
(10, NULL, 24, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(11, NULL, 25, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(12, NULL, 26, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(13, NULL, 27, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(14, NULL, 28, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(15, NULL, 29, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(16, NULL, 30, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(17, NULL, 31, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(18, NULL, 32, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(19, NULL, 33, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(20, NULL, 34, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(21, NULL, 35, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(22, NULL, 36, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(23, NULL, 37, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(24, NULL, 38, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(25, NULL, 39, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(26, NULL, 40, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(27, NULL, 41, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(28, NULL, 42, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(29, NULL, 43, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(30, NULL, 44, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(31, NULL, 45, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(32, NULL, 46, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(33, NULL, 47, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(34, NULL, 48, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(35, NULL, 49, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(36, NULL, 50, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(37, NULL, 51, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(38, NULL, 52, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(39, NULL, 53, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(40, NULL, 54, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(41, NULL, 55, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(42, NULL, 56, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(43, NULL, 57, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(44, NULL, 58, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(45, NULL, 59, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(46, NULL, 60, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `privileges`
--

CREATE TABLE `privileges` (
  `id` int(11) NOT NULL,
  `levels_id` int(11) NOT NULL,
  `pages_id` int(11) NOT NULL,
  `edit` tinyint(4) NOT NULL DEFAULT 1,
  `update` tinyint(4) NOT NULL DEFAULT 1,
  `delete` tinyint(4) NOT NULL DEFAULT 1,
  `add` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE `regions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `name`) VALUES
(1, 'Nouvelle-Aquitaine'),
(2, 'Ile-de-France'),
(3, 'Hauts-de-France'),
(4, 'Auvergne-Rhone-Alpes'),
(5, 'Normandie'),
(6, 'Grand Est'),
(7, 'Pays de la Loire'),
(8, 'La Réunion'),
(9, 'Martinique'),
(10, 'Guadeloupe');

-- --------------------------------------------------------

--
-- Table structure for table `regions_nafs`
--

CREATE TABLE `regions_nafs` (
  `id` int(11) NOT NULL,
  `regions_id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `excluded` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `regions_nafs`
--

INSERT INTO `regions_nafs` (`id`, `regions_id`, `code`, `excluded`) VALUES
(1, 1, '0111Z', 1),
(2, 1, '0112Z', 1),
(3, 1, '0113Z', 1),
(4, 1, '0114Z', 1),
(5, 1, '0115Z', 1),
(6, 1, '0116Z', 1),
(7, 1, '0119Z', 1),
(8, 1, '0121Z', 1),
(9, 1, '0122Z', 1),
(10, 1, '0123Z', 1),
(11, 1, '0124Z', 1),
(12, 1, '0125Z', 1),
(13, 1, '0126Z', 1),
(14, 1, '0127Z', 1),
(15, 1, '0128Z', 1),
(16, 1, '0129Z', 1),
(17, 1, '0130Z', 1),
(18, 1, '0141Z', 1),
(19, 1, '0142Z', 1),
(20, 1, '0143Z', 1),
(21, 1, '0144Z', 1),
(22, 1, '0145Z', 1),
(23, 1, '0146Z', 1),
(24, 1, '0147Z', 1),
(25, 1, '0149Z', 1),
(26, 1, '0150Z', 1),
(27, 1, '0161Z', 1),
(28, 1, '0162Z', 1),
(29, 1, '0163Z', 1),
(30, 1, '0164Z', 1),
(31, 1, '0170Z', 1),
(32, 1, '0311Z', 1),
(33, 1, '0312Z', 1),
(34, 1, '0321Z', 1),
(35, 1, '0322Z', 1),
(36, 1, '9604Z', 1),
(37, 1, '9609Z', 1),
(38, 1, '4110A', 1),
(39, 1, '4110B', 1),
(40, 1, '4110C', 1),
(41, 1, '4110D', 1),
(42, 1, '6810Z', 1),
(43, 1, '6820A', 1),
(44, 1, '6820B', 1),
(45, 1, '6831Z', 1),
(46, 1, '6832A', 1),
(47, 1, '6832B', 1),
(48, 1, '6411Z', 1),
(49, 1, '6419Z', 1),
(50, 1, '6420Z', 1),
(51, 1, '6430Z', 1),
(52, 1, '6491Z', 1),
(53, 1, '6492Z', 1),
(54, 1, '6499Z', 1),
(55, 1, '8610Z', 1),
(56, 1, '8621Z', 1),
(57, 1, '8622A', 1),
(58, 1, '8622B', 1),
(59, 1, '8622C', 1),
(60, 1, '8623Z', 1),
(61, 1, '8690A', 1),
(62, 1, '8690B', 1),
(63, 1, '8690C', 1),
(64, 1, '8690D', 1),
(65, 1, '8690E', 1),
(66, 1, '8690F', 1),
(67, 1, '8510Z', 1),
(68, 1, '8520Z', 1),
(69, 1, '8531Z', 1),
(70, 1, '8510Z', 1),
(71, 1, '8520Z', 1),
(72, 1, '8531Z', 1),
(73, 1, '8532Z', 1),
(74, 1, '8541Z', 1),
(75, 1, '8542Z', 1),
(76, 1, '8551Z', 1),
(77, 1, '8552Z', 1),
(78, 1, '8553Z', 1),
(79, 1, '8559A', 1),
(80, 1, '8559B', 1),
(81, 1, '8560Z', 1),
(82, 1, '6910Z', 1),
(83, 1, '6920Z', 1),
(84, 1, '7010Z', 1),
(85, 1, '7111Z', 1),
(86, 1, '7112A', 1),
(87, 1, '7311Z', 1),
(88, 1, '7312Z', 1),
(89, 1, '7320Z', 1),
(90, 1, '7311Z', 1),
(91, 1, '7312Z', 1),
(92, 1, '7320Z', 1),
(93, 1, '7420Z', 1),
(94, 1, '7500Z', 1),
(95, 2, '4773Z', 1),
(96, 2, '4774Z', 1),
(97, 2, '4791B', 1),
(98, 2, '4932Z', 1),
(99, 2, '6312Z', 1),
(100, 2, '6419Z', 1),
(101, 2, '6430Z', 1),
(102, 2, '6499Z', 1),
(103, 2, '6511Z', 1),
(104, 2, '6512Z', 1),
(105, 2, '6520Z', 1),
(106, 2, '6530Z', 1),
(107, 2, '6611Z ', 1),
(108, 2, '6612Z', 1),
(109, 2, '6619A', 1),
(110, 2, '6619B', 1),
(111, 2, '6621Z', 1),
(112, 2, '6622Z', 1),
(113, 2, '6629Z ', 1),
(114, 2, ' 6630Z', 1),
(115, 2, ' 6831Z', 1),
(116, 2, '6910Z ', 1),
(117, 2, '6920Z', 1),
(118, 2, '7120A', 1),
(119, 2, '7500Z ', 1),
(120, 2, '8610Z', 1),
(121, 2, '8621Z', 1),
(122, 2, '8622A', 1),
(123, 2, '8622B ', 1),
(124, 2, '8622C', 1),
(125, 2, '8623Z', 1),
(126, 2, '8690A', 1),
(127, 2, '8690B', 1),
(128, 2, '8690C ', 1),
(129, 2, '8690D ', 1),
(130, 2, '8690E ', 1),
(131, 2, '8690F', 1),
(132, 2, '8710C', 1),
(133, 2, '8411Z', 1),
(134, 2, '8412Z', 1),
(135, 2, '8421Z', 1),
(136, 2, '8422Z', 1),
(137, 2, '8423Z', 1),
(138, 2, '8424Z', 1),
(139, 2, '8425Z', 1),
(140, 2, '8430A', 1),
(141, 2, '8430B', 1),
(142, 2, '8430C ', 1),
(143, 2, '8510Z', 1),
(144, 2, '8520Z', 1),
(145, 2, '8531Z ', 1),
(146, 2, '8532Z', 1),
(147, 2, '8541Z', 1),
(148, 2, '8542Z', 1),
(149, 2, '8551Z', 1),
(150, 2, '8552Z ', 1),
(151, 2, '8553Z', 1),
(152, 2, '8559A', 1),
(153, 2, '8559B', 1),
(154, 2, '8560Z', 1),
(155, 7, '8411Z', 1),
(156, 7, '8412Z', 1),
(157, 7, '8413Z', 1),
(158, 7, '8421Z', 1),
(159, 7, '8422Z', 1),
(160, 7, '8423Z', 1),
(161, 7, '8424Z', 1),
(162, 7, '8425Z', 1),
(163, 7, '8430A', 1),
(164, 7, '8430B', 1),
(165, 7, '8430C', 1),
(166, 7, '4791B', 1),
(167, 7, '4773Z', 1),
(168, 7, '4774Z', 1),
(169, 7, '4778A', 1),
(170, 7, '4791A', 1),
(171, 7, '4791B', 1),
(172, 7, '4932Z', 1),
(173, 7, '6312Z', 1),
(174, 7, '6411Z', 1),
(175, 7, '6492Z', 1),
(176, 7, '6499Z', 1),
(177, 7, '6419Z', 1),
(178, 7, '6430Z', 1),
(179, 7, '6491Z', 1),
(180, 7, '6630Z', 1),
(181, 7, '6831Z', 1),
(182, 7, '6832A', 1),
(183, 7, '6832B', 1),
(184, 7, '6910Z', 1),
(185, 7, '6920Z', 1),
(186, 7, '7120A', 1),
(187, 7, '7500Z', 1),
(188, 7, '8411Z', 1),
(189, 7, '8412Z', 1),
(190, 7, '8413Z', 1),
(191, 7, '8421Z', 1),
(192, 7, '8422Z', 1),
(193, 7, '8423Z', 1),
(194, 7, '8424Z', 1),
(195, 7, '8425Z', 1),
(196, 7, '8430A', 1),
(197, 7, '8430B', 1),
(198, 7, '8430C', 1),
(199, 7, '8510Z', 1),
(200, 7, '8520Z', 1),
(201, 7, '8531Z', 1),
(202, 7, '8532Z', 1),
(203, 7, '8541Z', 1),
(204, 7, '8542Z', 1),
(205, 7, '8551Z', 1),
(206, 7, '8552Z', 1),
(207, 7, '8553Z', 1),
(208, 7, '8559A', 1),
(209, 7, '8559B', 1),
(210, 7, '8560Z', 1),
(211, 7, '8610Z', 1),
(212, 7, '8621Z', 1),
(213, 7, '8622A', 1),
(214, 7, '8622B', 1),
(215, 7, '8622C', 1),
(216, 7, '8623Z', 1),
(217, 7, '8690A', 1),
(218, 7, '8690B', 1),
(219, 7, '8690C', 1),
(220, 7, '8690D', 1),
(221, 7, '8690E', 1),
(222, 7, '8690F', 1),
(223, 7, '8710A', 1),
(224, 7, '8710B', 1),
(225, 7, '8710C', 1),
(226, 7, '4791B', 1),
(227, 7, '6312Z', 1),
(228, 7, '4773Z', 1),
(229, 7, '4774Z', 1),
(230, 7, '4778A', 1),
(231, 7, '4932Z', 1),
(232, 9, '5819Z', 1),
(233, 9, '6312Z', 1);

-- --------------------------------------------------------

--
-- Table structure for table `regions_vouchers`
--

CREATE TABLE `regions_vouchers` (
  `id` int(11) NOT NULL,
  `regions_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `min` int(11) NOT NULL,
  `max` int(11) NOT NULL,
  `refund` int(11) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `regions_vouchers`
--

INSERT INTO `regions_vouchers` (`id`, `regions_id`, `name`, `min`, `max`, `refund`, `amount`) VALUES
(1, 1, 'Chéque E-commerce', 500, 10000, 50, 5000),
(2, 2, 'Chéque commerce connecter', 300, 3000, 50, 1500),
(3, 3, 'Chéque booster TPE numérique', 3000, 30000, 40, 12000),
(4, 4, 'Aide régional mon commerce en ligne', 600, 3000, 50, 1500),
(5, 4, 'Aide régional mon commerce en ligne', 100, 500, 100, 500),
(6, 5, 'Chéque impulsion transition numérique', 2000, 10000, 50, 5000),
(7, 6, 'Chéque soutien à la digitalisation', 1000, 6000, 100, 6000),
(8, 7, 'Chéque investissement numérique - PDLIN', 5000, 30000, 50, 15000),
(9, 8, 'Chèque Numérique : Renforcement du dispositif dans le cadre de la crise sanitaire du Covid-19', 300, 4000, 80, 3200),
(10, 9, 'Pass numarique : aide à la transformation numérique', 300, 10000, 50, 500),
(11, 10, 'Chéque tic', 300, 12500, 80, 10000);

-- --------------------------------------------------------

--
-- Table structure for table `services_grants`
--

CREATE TABLE `services_grants` (
  `id` int(11) NOT NULL,
  `transitions_id` int(11) NOT NULL,
  `orientation` varchar(255) DEFAULT NULL,
  `turnovers_id` int(11) NOT NULL,
  `grants` int(11) DEFAULT NULL,
  `original_price` int(11) DEFAULT NULL,
  `sell_price` int(11) DEFAULT NULL,
  `budget` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `services_grants`
--

INSERT INTO `services_grants` (`id`, `transitions_id`, `orientation`, `turnovers_id`, `grants`, `original_price`, `sell_price`, `budget`) VALUES
(1, 22, 'site vitrine starter', 1, 5100, 5999, 899, 500),
(2, 22, 'site vitrine sur mesur', 2, 3000, 5999, 2999, 3000),
(3, 22, 'site vitrine sur mesur', 3, 2850, 5999, 3149, 3200),
(4, 22, 'site vitrine sur mesur', 4, 2750, 5999, 3249, 3300),
(5, 22, 'site vitrine sur mesur', 5, 2650, 5999, 3349, 3400),
(6, 22, 'site vitrine sur mesur', 6, 2550, 5999, 3449, 3500),
(7, 22, 'site vitrine sur mesur', 7, 2450, 5999, 3549, 3600),
(8, 22, 'site vitrine sur mesur', 8, 2350, 5999, 3649, 3700),
(9, 22, 'site vitrine sur mesur', 9, 2150, 5999, 3849, 3900),
(10, 22, 'site vitrine sur mesur', 10, 1950, 5999, 4049, 4100),
(11, 22, 'site vitrine sur mesur', 11, 1750, 5999, 4249, 4300),
(12, 22, 'site vitrine sur mesur', 12, 1550, 5999, 4449, 4500),
(13, 22, 'site vitrine sur mesur', 13, 1350, 5999, 4649, 4700),
(14, 22, 'site vitrine sur mesur', 14, 1150, 5999, 4849, 4900),
(15, 22, 'site vitrine sur mesur', 15, 950, 5999, 5049, 5100),
(16, 22, 'site vitrine sur mesur', 16, 750, 5999, 5249, 5300),
(17, 22, 'site vitrine sur mesur', 17, 550, 5999, 5449, 5500),
(18, 22, 'site vitrine sur mesur', 18, 350, 5999, 5649, 5700),
(19, 22, 'site vitrine sur mesur', 19, 350, 5999, 5649, 5700),
(20, 22, 'site vitrine sur mesur', 20, 350, 5999, 5649, 5700),
(21, 22, 'site vitrine sur mesur', 21, 350, 5999, 5649, 5700),
(22, 22, 'site vitrine sur mesur', 22, 350, 5999, 5649, 5700),
(23, 22, 'site vitrine sur mesur', 23, 350, 5999, 5649, 5700),
(24, 22, 'site vitrine sur mesur', 24, 350, 5999, 5649, 5700),
(25, 22, 'site vitrine sur mesur', 25, 350, 5999, 5649, 5700),
(26, 22, 'site vitrine sur mesur', 26, 350, 5999, 5649, 5700),
(27, 22, 'site vitrine sur mesur', 27, 350, 5999, 5649, 5700),
(28, 22, 'site vitrine sur mesur', 28, 350, 5999, 5649, 5700),
(29, 22, 'site vitrine sur mesur', 29, 350, 5999, 5649, 5700),
(30, 22, 'site vitrine sur mesur', 30, 350, 5999, 5649, 5700),
(31, 22, 'site vitrine sur mesur', 31, 350, 5999, 5649, 5700),
(32, 22, 'site vitrine sur mesur', 32, 350, 5999, 5649, 5700),
(33, 22, 'site vitrine sur mesur', 33, 350, 5999, 5649, 5700),
(34, 22, 'site vitrine sur mesur', 34, 350, 5999, 5649, 5700),
(35, 22, 'site vitrine sur mesur', 35, 350, 5999, 5649, 5700),
(36, 22, 'site vitrine sur mesur', 36, 350, 5999, 5649, 5700),
(37, 22, 'site vitrine sur mesur', 37, 350, 5999, 5649, 5700),
(38, 22, 'site vitrine sur mesur', 38, 350, 5999, 5649, 5700),
(39, 20, 'Market place', 1, 10250, 20489, 10239, 10300),
(40, 20, 'Market place', 2, 9750, 20489, 10739, 10800),
(41, 20, 'Market place', 3, 9250, 20489, 11239, 11300),
(42, 20, 'Market place', 4, 8750, 20489, 11739, 11800),
(43, 20, 'Market place', 5, 7800, 20489, 12689, 12700),
(44, 20, 'Market place', 6, 7300, 20489, 13189, 13200),
(45, 20, 'Market place', 7, 6800, 20489, 13689, 13700),
(46, 20, 'Market place', 8, 6300, 20489, 14189, 14200),
(47, 20, 'Market place', 9, 5800, 20489, 14689, 14700),
(48, 20, 'Market place', 10, 5500, 20489, 14989, 15000),
(49, 20, 'Market place', 11, 5000, 20489, 15489, 15500),
(50, 20, 'Market place', 12, 4500, 20489, 15989, 16000),
(51, 20, 'Market place', 13, 4000, 20489, 16489, 16500),
(52, 20, 'Market place', 14, 3500, 20489, 16989, 17000),
(53, 20, 'Market place', 15, 3500, 20489, 16989, 17000),
(54, 20, 'Market place', 16, 3500, 20489, 16989, 17000),
(55, 20, 'Market place', 17, 3500, 20489, 16989, 17000),
(56, 20, 'Market place', 18, 3500, 20489, 16989, 17000),
(57, 20, 'Market place', 19, 3500, 20489, 16989, 17000),
(58, 20, 'Market place', 20, 3500, 20489, 16989, 17000),
(59, 20, 'Market place', 21, 3500, 20489, 16989, 17000),
(60, 20, 'Market place', 22, 3500, 20489, 16989, 17000),
(61, 20, 'Market place', 23, 3500, 20489, 16989, 17000),
(62, 20, 'Market place', 24, 3500, 20489, 16989, 17000),
(63, 20, 'Market place', 25, 3500, 20489, 16989, 17000),
(64, 20, 'Market place', 26, 3500, 20489, 16989, 17000),
(65, 20, 'Market place', 27, 3500, 20489, 16989, 17000),
(66, 20, 'Market place', 28, 3500, 20489, 16989, 17000),
(67, 20, 'Market place', 29, 3500, 20489, 16989, 17000),
(68, 20, 'Market place', 30, 3500, 20489, 16989, 17000),
(69, 20, 'Market place', 31, 3500, 20489, 16989, 17000),
(70, 20, 'Market place', 32, 3500, 20489, 16989, 17000),
(71, 20, 'Market place', 33, 3500, 20489, 16989, 17000),
(72, 20, 'Market place', 34, 3500, 20489, 16989, 17000),
(73, 20, 'Market place', 35, 3500, 20489, 16989, 17000),
(74, 20, 'Market place', 36, 3500, 20489, 16989, 17000),
(75, 20, 'Market place', 37, 3500, 20489, 16989, 17000),
(76, 20, 'Market place', 38, 3500, 20489, 16989, 17000),
(77, 31, 'App starter', 1, 6000, 12000, 6000, 6500),
(78, 31, 'App starter', 2, 6000, 12000, 6000, 7000),
(79, 31, 'App starter', 3, 6000, 12000, 6000, 7500),
(80, 31, 'App sur mesur ', 4, 10000, 22999, 12000, 13000),
(81, 31, 'App sur mesur ', 5, 8851, 22999, 14148, 14200),
(82, 31, 'App sur mesur ', 6, 7702, 22999, 15297, 15300),
(83, 31, 'App sur mesur ', 7, 6553, 22999, 16446, 16500),
(84, 31, 'App sur mesur ', 8, 5404, 22999, 17595, 17600),
(85, 31, 'App sur mesur ', 9, 4255, 22999, 18744, 18700),
(86, 31, 'App sur mesur ', 10, 3106, 22999, 19893, 19900),
(87, 31, 'App sur mesur ', 11, 1957, 22999, 21042, 21100),
(88, 31, 'App sur mesur ', 12, 1957, 22999, 21042, 21100),
(89, 31, 'App sur mesur ', 13, 1957, 22999, 21042, 21100),
(90, 31, 'App sur mesur ', 14, 1957, 22999, 21042, 21100),
(91, 31, 'App sur mesur ', 15, 1957, 22999, 21042, 21100),
(92, 31, 'App sur mesur ', 16, 1957, 22999, 21042, 21100),
(93, 31, 'App sur mesur ', 17, 1957, 22999, 21042, 21100),
(94, 31, 'App sur mesur ', 18, 1957, 22999, 21042, 21100),
(95, 31, 'App sur mesur ', 19, 1957, 22999, 21042, 21100),
(96, 31, 'App sur mesur ', 20, 1957, 22999, 21042, 21100),
(97, 31, 'App sur mesur ', 21, 1957, 22999, 21042, 21100),
(98, 31, 'App sur mesur ', 22, 1957, 22999, 21042, 21100),
(99, 31, 'App sur mesur ', 23, 1957, 22999, 21042, 21100),
(100, 31, 'App sur mesur ', 24, 1957, 22999, 21042, 21100),
(101, 31, 'App sur mesur ', 25, 1957, 22999, 21042, 21100),
(102, 31, 'App sur mesur ', 26, 1957, 22999, 21042, 21100),
(103, 31, 'App sur mesur ', 27, 1957, 22999, 21042, 21100),
(104, 31, 'App sur mesur ', 28, 1957, 22999, 21042, 21100),
(105, 31, 'App sur mesur ', 29, 1957, 22999, 21042, 21100),
(106, 31, 'App sur mesur ', 30, 1957, 22999, 21042, 21100),
(107, 31, 'App sur mesur ', 31, 1957, 22999, 21042, 21100),
(108, 31, 'App sur mesur ', 32, 1957, 22999, 21042, 21100),
(109, 31, 'App sur mesur ', 33, 1957, 22999, 21042, 21100),
(110, 31, 'App sur mesur ', 34, 1957, 22999, 21042, 21100),
(111, 31, 'App sur mesur ', 35, 1957, 22999, 21042, 21100),
(112, 31, 'App sur mesur ', 36, 1957, 22999, 21042, 21100),
(113, 31, 'App sur mesur ', 37, 1957, 22999, 21042, 21100),
(114, 31, 'App sur mesur ', 38, 1957, 22999, 21042, 21100),
(115, 24, 'Site ecommerce starter', 1, 8400, 9599, 1199, 1500),
(116, 24, 'Site ecommerce starter', 2, 8400, 9599, 1199, 2000),
(117, 24, 'Site ecommerce 50-100 produits', 3, 7000, 9599, 2599, 2600),
(118, 24, 'Site ecommerce 50-100 produits', 4, 7000, 9599, 2599, 3000),
(119, 24, 'Site ecommerce 50-100 produits', 5, 7000, 9599, 2599, 3500),
(120, 24, 'Site ecommerce 150-500 produits', 6, 5000, 9599, 4599, 4600),
(121, 24, 'Site ecommerce 150-500 produits', 7, 5000, 9599, 4599, 5000),
(122, 24, 'Site ecommerce 150-500 produits', 8, 5000, 9599, 4599, 5500),
(123, 24, 'Site ecommerce Produit illimité', 9, 3600, 9599, 5999, 7000),
(124, 24, 'Site ecommerce Produit illimité', 10, 3600, 9599, 5999, 7100),
(125, 24, 'Site ecommerce Produit illimité', 11, 3600, 9599, 5999, 7200),
(126, 24, 'Site ecommerce Produit illimité', 12, 3600, 9599, 5999, 7300),
(127, 24, 'Site ecommerce Produit illimité', 13, 3600, 9599, 5999, 7400),
(128, 24, 'Site ecommerce Produit illimité', 14, 3600, 9599, 5999, 7500),
(129, 24, 'Site ecommerce sur mesur illimité', 15, 2880, 9599, 6719, 7600),
(130, 24, 'Site ecommerce sur mesur illimité', 16, 2400, 9599, 7199, 8000),
(131, 24, 'Site ecommerce sur mesur illimité', 17, 1920, 9599, 7679, 8500),
(132, 24, 'Site ecommerce sur mesur illimité', 18, 1440, 9599, 8159, 9000),
(133, 24, 'Site ecommerce sur mesur illimité', 19, 950, 9599, 8650, 10000),
(134, 24, 'Site ecommerce sur mesur illimité', 20, 950, 9599, 8650, 10000),
(135, 24, 'Site ecommerce sur mesur illimité', 21, 950, 9599, 8650, 10000),
(136, 24, 'Site ecommerce sur mesur illimité', 22, 950, 9599, 8650, 10000),
(137, 24, 'Site ecommerce sur mesur illimité', 23, 950, 9599, 8650, 10000),
(138, 24, 'Site ecommerce sur mesur illimité', 24, 950, 9599, 8650, 10000),
(139, 24, 'Site ecommerce sur mesur illimité', 25, 950, 9599, 8650, 10000),
(140, 24, 'Site ecommerce sur mesur illimité', 26, 950, 9599, 8650, 10000),
(141, 24, 'Site ecommerce sur mesur illimité', 27, 950, 9599, 8650, 10000),
(142, 24, 'Site ecommerce sur mesur illimité', 28, 950, 9599, 8650, 10000),
(143, 24, 'Site ecommerce sur mesur illimité', 29, 950, 9599, 8650, 10000),
(144, 24, 'Site ecommerce sur mesur illimité', 30, 950, 9599, 8650, 10000),
(145, 24, 'Site ecommerce sur mesur illimité', 31, 950, 9599, 8650, 10000),
(146, 24, 'Site ecommerce sur mesur illimité', 32, 950, 9599, 8650, 10000),
(147, 24, 'Site ecommerce sur mesur illimité', 33, 950, 9599, 8650, 10000),
(148, 24, 'Site ecommerce sur mesur illimité', 34, 950, 9599, 8650, 10000),
(149, 24, 'Site ecommerce sur mesur illimité', 35, 950, 9599, 8650, 10000),
(150, 24, 'Site ecommerce sur mesur illimité', 36, 950, 9599, 8650, 10000),
(151, 24, 'Site ecommerce sur mesur illimité', 37, 950, 9599, 8650, 10000),
(152, 24, 'Site ecommerce sur mesur illimité', 38, 950, 9599, 8650, 10000),
(153, 25, 'CRM /ERP', 1, 11999, 29999, 18000, 19500),
(154, 25, 'CRM /ERP', 2, 10500, 29999, 19499, 20000),
(155, 25, 'CRM /ERP', 3, 9000, 29999, 20999, 21000),
(156, 25, 'CRM /ERP', 4, 8100, 29999, 21899, 22000),
(157, 25, 'CRM /ERP', 5, 7800, 29999, 22199, 22200),
(158, 25, 'CRM /ERP', 6, 7500, 29999, 22499, 22500),
(159, 25, 'CRM /ERP', 7, 7200, 29999, 22799, 22800),
(160, 25, 'CRM /ERP', 8, 6600, 29999, 23399, 23400),
(161, 25, 'CRM /ERP', 9, 6300, 29999, 23699, 23700),
(162, 25, 'CRM /ERP', 10, 6000, 29999, 23999, 24000),
(163, 25, 'CRM /ERP', 11, 5700, 29999, 24299, 24300),
(164, 25, 'CRM /ERP', 12, 5400, 29999, 24599, 24600),
(165, 25, 'CRM /ERP', 13, 5100, 29999, 24899, 24900),
(166, 25, 'CRM /ERP', 14, 4800, 29999, 25199, 25200),
(167, 25, 'CRM /ERP', 15, 4500, 29999, 25499, 25500),
(168, 25, 'CRM /ERP', 16, 4500, 29999, 25499, 25500),
(169, 25, 'CRM /ERP', 17, 4500, 29999, 25499, 25500),
(170, 25, 'CRM /ERP', 18, 4500, 29999, 25499, 25500),
(171, 25, 'CRM /ERP', 19, 4500, 29999, 25499, 25500),
(172, 25, 'CRM /ERP', 20, 4500, 29999, 25499, 25500),
(173, 25, 'CRM /ERP', 21, 4500, 29999, 25499, 25500),
(174, 25, 'CRM /ERP', 22, 4500, 29999, 25499, 25500),
(175, 25, 'CRM /ERP', 23, 4500, 29999, 25499, 25500),
(176, 25, 'CRM /ERP', 24, 4500, 29999, 25499, 25500),
(177, 25, 'CRM /ERP', 25, 4500, 29999, 25499, 25500),
(178, 25, 'CRM /ERP', 26, 4500, 29999, 25499, 25500),
(179, 25, 'CRM /ERP', 27, 4500, 29999, 25499, 25500),
(180, 25, 'CRM /ERP', 28, 4500, 29999, 25499, 25500),
(181, 25, 'CRM /ERP', 29, 4500, 29999, 25499, 25500),
(182, 25, 'CRM /ERP', 30, 4500, 29999, 25499, 25500),
(183, 25, 'CRM /ERP', 31, 4500, 29999, 25499, 25500),
(184, 25, 'CRM /ERP', 32, 4500, 29999, 25499, 25500),
(185, 25, 'CRM /ERP', 33, 4500, 29999, 25499, 25500),
(186, 25, 'CRM /ERP', 34, 4500, 29999, 25499, 25500),
(187, 25, 'CRM /ERP', 35, 4500, 29999, 25499, 25500),
(188, 25, 'CRM /ERP', 36, 4500, 29999, 25499, 25500),
(189, 25, 'CRM /ERP', 37, 4500, 29999, 25499, 25500),
(190, 25, 'CRM /ERP', 38, 4500, 29999, 25499, 25500),
(191, 43, 'Community manager pack1', 1, 675, 1499, 824, 830),
(192, 43, 'Community manager pack1', 2, 660, 1499, 839, 840),
(193, 43, 'Community manager pack1', 3, 645, 1499, 854, 860),
(194, 43, 'Community manager pack1', 4, 630, 1499, 869, 870),
(195, 43, 'Community manager pack1', 5, 615, 1499, 884, 890),
(196, 43, 'Community manager pack1', 6, 600, 1499, 899, 900),
(197, 43, 'Community manager pack1', 7, 580, 1499, 919, 920),
(198, 43, 'Community manager pack1', 8, 570, 1499, 929, 930),
(199, 43, 'Community manager pack1', 9, 555, 1499, 944, 950),
(200, 43, 'Community manager pack1', 10, 540, 1499, 959, 1000),
(201, 43, 'Community manager pack1', 11, 375, 1499, 1124, 1200),
(202, 43, 'Community manager pack1', 12, 360, 1499, 1139, 1240),
(203, 43, 'Community manager pack1', 13, 345, 1499, 1154, 1260),
(204, 43, 'Community manager pack1', 14, 330, 1499, 1169, 1270),
(205, 43, 'Community manager pack1', 15, 300, 1499, 1199, 1400),
(206, 43, 'Community manager pack1', 16, 300, 1499, 1199, 1400),
(207, 43, 'Community manager pack1', 17, 300, 1499, 1199, 1400),
(208, 43, 'Community manager pack1', 18, 300, 1499, 1199, 1400),
(209, 43, 'Community manager pack1', 19, 300, 1499, 1199, 1400),
(210, 43, 'Community manager pack1', 20, 300, 1499, 1199, 1400),
(211, 43, 'Community manager pack1', 21, 300, 1499, 1199, 1400),
(212, 43, 'Community manager pack1', 22, 300, 1499, 1199, 1400),
(213, 43, 'Community manager pack1', 23, 300, 1499, 1199, 1400),
(214, 43, 'Community manager pack1', 24, 300, 1499, 1199, 1400),
(215, 43, 'Community manager pack1', 25, 300, 1499, 1199, 1400),
(216, 43, 'Community manager pack1', 26, 300, 1499, 1199, 1400),
(217, 43, 'Community manager pack1', 27, 300, 1499, 1199, 1400),
(218, 43, 'Community manager pack1', 28, 300, 1499, 1199, 1400),
(219, 43, 'Community manager pack1', 29, 300, 1499, 1199, 1400),
(220, 43, 'Community manager pack1', 30, 300, 1499, 1199, 1400),
(221, 43, 'Community manager pack1', 31, 300, 1499, 1199, 1400),
(222, 43, 'Community manager pack1', 32, 300, 1499, 1199, 1400),
(223, 43, 'Community manager pack1', 33, 300, 1499, 1199, 1400),
(224, 43, 'Community manager pack1', 34, 300, 1499, 1199, 1400),
(225, 43, 'Community manager pack1', 35, 300, 1499, 1199, 1400),
(226, 43, 'Community manager pack1', 36, 300, 1499, 1199, 1400),
(227, 43, 'Community manager pack1', 37, 300, 1499, 1199, 1400),
(228, 43, 'Community manager pack1', 38, 300, 1499, 1199, 1400),
(229, 44, 'Community manager pack2', 1, 1265, 2529, 1264, 1270),
(230, 44, 'Community manager pack2', 2, 1240, 2529, 1289, 1290),
(231, 44, 'Community manager pack2', 3, 1140, 2529, 1389, 1390),
(232, 44, 'Community manager pack2', 4, 1087, 2529, 1442, 1450),
(233, 44, 'Community manager pack2', 5, 1011, 2529, 1518, 1520),
(234, 44, 'Community manager pack2', 6, 960, 2529, 1569, 1570),
(235, 44, 'Community manager pack2', 7, 935, 2529, 1594, 1600),
(236, 44, 'Community manager pack2', 8, 910, 2529, 1619, 1620),
(237, 44, 'Community manager pack2', 9, 860, 2529, 1669, 1670),
(238, 44, 'Community manager pack2', 10, 835, 2529, 1694, 1700),
(239, 44, 'Community manager pack2', 11, 760, 2529, 1769, 1770),
(240, 44, 'Community manager pack2', 12, 733, 2529, 1796, 1800),
(241, 44, 'Community manager pack2', 13, 709, 2529, 1820, 1900),
(242, 44, 'Community manager pack2', 14, 633, 2529, 1896, 2000),
(243, 44, 'Community manager pack2', 15, 633, 2529, 1896, 2000),
(244, 44, 'Community manager pack2', 16, 633, 2529, 1896, 2000),
(245, 44, 'Community manager pack2', 17, 633, 2529, 1896, 2000),
(246, 44, 'Community manager pack2', 18, 633, 2529, 1896, 2000),
(247, 44, 'Community manager pack2', 19, 633, 2529, 1896, 2000),
(248, 44, 'Community manager pack2', 20, 633, 2529, 1896, 2000),
(249, 44, 'Community manager pack2', 21, 633, 2529, 1896, 2000),
(250, 44, 'Community manager pack2', 22, 633, 2529, 1896, 2000),
(251, 44, 'Community manager pack2', 23, 633, 2529, 1896, 2000),
(252, 44, 'Community manager pack2', 24, 633, 2529, 1896, 2000),
(253, 44, 'Community manager pack2', 25, 633, 2529, 1896, 2000),
(254, 44, 'Community manager pack2', 26, 633, 2529, 1896, 2000),
(255, 44, 'Community manager pack2', 27, 633, 2529, 1896, 2000),
(256, 44, 'Community manager pack2', 28, 633, 2529, 1896, 2000),
(257, 44, 'Community manager pack2', 29, 633, 2529, 1896, 2000),
(258, 44, 'Community manager pack2', 30, 633, 2529, 1896, 2000),
(259, 44, 'Community manager pack2', 31, 633, 2529, 1896, 2000),
(260, 44, 'Community manager pack2', 32, 633, 2529, 1896, 2000),
(261, 44, 'Community manager pack2', 33, 633, 2529, 1896, 2000),
(262, 44, 'Community manager pack2', 34, 633, 2529, 1896, 2000),
(263, 44, 'Community manager pack2', 35, 633, 2529, 1896, 2000),
(264, 44, 'Community manager pack2', 36, 633, 2529, 1896, 2000),
(265, 44, 'Community manager pack2', 37, 633, 2529, 1896, 2000),
(266, 44, 'Community manager pack2', 38, 633, 2529, 1896, 2000);

-- --------------------------------------------------------

--
-- Table structure for table `transitions`
--

CREATE TABLE `transitions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `logo` longtext NOT NULL,
  `desc` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transitions`
--

INSERT INTO `transitions` (`id`, `name`, `category`, `logo`, `desc`) VALUES
(1, 'Création logo', 'Graphique', 'Creation logo', 'Booster votre image par la création d\'une identité visuelle qui vous démarquera de la concurrence.'),
(2, 'Refonte logo', 'Graphique', 'Refonte logo', 'Remettre votre logo au goût du jour pour concéder à votre entreprise une nouvelle ére.'),
(3, 'Carte de visite', 'Graphique', 'Carte de visite', 'La carte visite est la route premiére vitrine de votre entreprise. Elle doit refléter l\'image que vous portez à votre entreprise.'),
(4, 'Flayer commerciale', 'Graphique', 'Flayer commerciale', 'Conçu pour susciter l\'intérêt des prospects, un flyer contenant un bon design et un message percutant est un gage de réussite.'),
(5, 'Carte de fidélité', 'Graphique', 'Carte de fidélité', 'Attribué des avantages sous formes de services, cadeaux et remises à vos clients à travers des cartes de fidélité personnalisées... quoi de mieux pour les gâter et donc les fidéliser.'),
(6, 'Poster / Affiche', 'Graphique', 'Poster-Affiche', 'Le poster ou l\'affiche vous permet de toucher une large audience. Considérer comme une affiche à portée de main, le poster est idéal pour insérer plus de détails que dans une affiche.'),
(7, 'Dépliant', 'Graphique', 'Dépliant', 'Les dépliants vous permettront de présenter tout votre panel de prestations. Parfait pour faire passer un message captivant.'),
(8, 'Plaquette', 'Graphique', 'Plaquette', 'La plaquette permet de mettre en avant un message, une offre ou un service. Utile pour les commerciaux de l’entreprise qui peuvent prendre appui sur la plaquette commerciale pour prospecter.'),
(9, 'Retouche photo', 'Graphique', 'Retouche photo', 'Améliorer la qualité de la photo, la sublimer ou la retoucher implique un plus grand intérêt au produit ! Indispensable pour cibler un plus large public.'),
(10, 'Charte graphique', 'Graphique', 'Charte graphique', 'La charte graphique vous permet de maintenir une cohérence autour de votre image. C’est la Bible de votre identité visuelle et est indispensable pour toute conception graphique.'),
(11, 'Signature mail', 'Graphique', 'Signature mail', 'Considérée comme la carte visite commerciale, la signature mail est indispensable pour mettre en avant l’atout de votre entreprise ou son contenu.'),
(12, 'Menu restaurant', 'Graphique', 'Menu restaurant', 'Votre menu reflète toute l’image que vous portez à votre établissement. A l’ère du numérique, plus les prospects ont des informations détaillées sur ce que vous proposez, plus ils sont tentés d’essayer.'),
(13, 'Cover single', 'Graphique', 'Cover single', 'Le cover Single est le support visuel de l\'univers de l\'artiste !Il nous est donc indispensable de mettre la lumière sur l’originalité du single à travers sa pochette d’album.'),
(14, 'Pochette à rabat', 'Graphique', 'Pochette à rabat', 'La pochette à rabat est parfaite pour introduire des fichiers produits ou techniques, un devis ou un contrat. Elle est considérée jusqu’à maintenant un document commerciale incontournable.'),
(15, 'Catalogue', 'Graphique', 'Catalogue', 'Idéal pour promouvoir une gamme de produits auprès des consommateurs. En format digital ou en souvenir papier ; le catalogue reste l’outil de communication le plus explicite.'),
(16, 'Couverture de livre', 'Graphique', 'Couverture de livre', 'Une couverture de livre pertinente se doit de constituer une vraie étape dans le marketing de l’éditeur.(trice) ou de l’auteur(e). C’est la première vitrine de ce que le livre contient.'),
(17, 'Kit stand salon', 'Graphique', 'Kit stand salon', 'Pour vos expositions ou forum, le KIT Stand Salon est idéal pour captiver l’intérêt des visiteurs et professionnels.'),
(18, 'Bon de commande', 'Graphique', 'Bon de commande', 'Le bon de commande est un document qui vous permet de définir et valider les modalités de prestation.'),
(19, 'Packaging', 'Graphique', 'Packaging', 'Outre ses fonctions physiques, le packaging est incontestablement la vitrine de toute entreprise. Il permet de communiquer toutes les informations sur le produit au consommateur. C\'est un vecteur de sens, de signification.'),
(20, 'Site marketplace', 'Services', 'Site marketplace', 'Un site web marketplace permet de mettre en relation des vendeurs et des acheteurs, particuliers ou professionnels.'),
(21, 'SEO', 'Développement', 'SEO', 'Le référencement naturel (SEO) consiste souvent à apporter de petites modifications à certaines parties de votre site.'),
(22, 'Site vitrine', 'Services', 'Site vitrine', 'Site vitrine ergonomique est un moyen efficace de parler de vos services.'),
(24, 'Site e-commerce', 'Services', 'Site e-commerce', 'Site web clé en main e-commerce est éligible à la remise de 30% pris en charge par l\'état IDF fond de solidarité pour les créateurs d\'entreprises.'),
(25, 'ERP / CRM', 'Services', 'ERP-CRM', 'Idéal pour l’organisation et la gestion des ressources de votre entreprise et les relations commerciales. L’objectif étant de relier les différentes activités synchroniser leur fonctionnement.'),
(26, 'Application réseau sociaux', 'Développement', 'Application réseau sociaux', 'Application mobile pour réseau social généraliste ou spécialisé.'),
(27, 'Application marketplace', 'Développement', 'Application marketplace', 'Une application marketplace permet de mettre en relation des vendeurs et des acheteurs, particuliers ou professionnels.'),
(28, 'Application jeux mobile', 'Développement', 'Application jeux mobile', 'Une interface intuitive,un langage de script graphique effets particules, et animations.'),
(29, 'Application Marketing', 'Développement', 'Application Marketing', 'Une application mobile marketing ergonomique est un moyen efficace de parlerde vos services.'),
(30, 'Application ERP / CRM', 'Développement', 'Application ERP-CRM', 'Une application ERP / CRM Vous permettra d’automatiser les processus de vente, et de recueillir des informations sur les clients.'),
(31, 'Application E-commerce', 'Services', 'E-commerce', 'Application mobile e-commerce est un moyen efficace pour parler de vos produits et optimiser vos ventes.'),
(32, 'Design applicatif XD', 'Graphique', 'Design applicatif XD', 'Un design applicatif personnalisé et sur mesure, à l’image de votre entreprise.'),
(33, 'Banniére vidéo facebook', 'Montage', 'Banniére vidéo facebook', 'Vitrine de votre facebook c\'est la premiére impression qu\'un utilisateur aura de votre activité. Un facteur décisif.'),
(34, 'Espace et compagne mailing', 'Marketing', 'Espace et compagne mailing', 'Un moyen performant de communiquer sur un événement.'),
(35, 'Banniére réseaux sociaux', 'Graphique', 'Banniére réseaux sociaux', 'La publicité en ligne par excellence pour augmenter vos ventes et développer votre image et notoriété.'),
(36, 'Rédaction web marketing', 'Marketing', 'Rédaction web marketing', 'Rédaction stratégique pour votre audience blog articles avec contenue qualitatif recherche mots-clefs dans votre secteur d\'activité.'),
(37, 'Stratégie marketing', 'Marketing', 'Stratégie marketing', 'Une stratégie marketing bien étudiée et spécifique à votre secteur d\'activité.'),
(38, 'Texte marketing', 'Marketing', 'Texte marketing', 'Un texte marketing captivant, pour votre entreprise.'),
(39, 'Animation logo', 'Montage', 'Animation logo', 'L\'animation de votre logo amplifie l\'impact visuel de votre message et le rendra plus facilement mémorisable.'),
(40, 'Tournage studio', 'Montage', 'Tournage studio', 'Un studio de tournage ( photo / vidéo ) à votre disposition, pour la réalisation d\'un spot publicitaire ou autres.'),
(41, 'Vidéo motion désign', 'Montage', 'Vidéo motion désign', 'Diffuser des informations de façon simple, presenter l’utilisation d’un produit ou le fonctionnement d’un service dans une vidéo motion.'),
(42, 'Vidéo marketing', 'Montage', 'Vidéo marketing', 'La vidéo permet d’expliquerclairement les avantages d’un produit, d’un service, d’un concept ou encore d’une entreprise.'),
(43, 'Community Management (Pack standard)', 'Services', 'Community Management', ''),
(44, 'Community Management (Pack master)', 'Services', 'Community Management', '');

-- --------------------------------------------------------

--
-- Table structure for table `turnovers`
--

CREATE TABLE `turnovers` (
  `id` int(11) NOT NULL,
  `transitions_id` int(11) NOT NULL,
  `min` int(11) NOT NULL,
  `max` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `turnovers`
--

INSERT INTO `turnovers` (`id`, `transitions_id`, `min`, `max`) VALUES
(1, 22, 5000, 50000),
(2, 22, 50000, 100000),
(3, 22, 100000, 200000),
(4, 22, 200000, 300000),
(5, 22, 300000, 400000),
(6, 22, 400000, 500000),
(7, 25, 500000, 600000),
(8, 25, 600000, 700000),
(9, 25, 700000, 800000),
(10, 25, 800000, 900000),
(11, 25, 900000, 1000000),
(12, 25, 1000000, 1500000),
(13, 20, 1500000, 2000000),
(14, 20, 2000000, 2500000),
(15, 20, 2500000, 3000000),
(16, 20, 3000000, 3500000),
(17, 20, 3500000, 4000000),
(18, 20, 4000000, 4500000),
(19, 31, 4500000, 5000000),
(20, 31, 5000000, 5500000),
(21, 31, 5500000, 6000000),
(22, 31, 6000000, 6500000),
(23, 31, 6500000, 7000000),
(24, 24, 7000000, 7500000),
(25, 24, 7500000, 8000000),
(26, 24, 8000000, 8500000),
(27, 24, 8500000, 9000000),
(28, 24, 9000000, 9500000),
(29, 43, 9500000, 10000000),
(30, 43, 10000000, 10500000),
(31, 43, 10500000, 11000000),
(32, 43, 11000000, 12000000),
(33, 43, 12000000, 13000000),
(34, 44, 13000000, 14000000),
(35, 44, 14000000, 15000000),
(36, 44, 15000000, 20000000),
(37, 44, 20000000, 25000000),
(38, 44, 25000000, 30000000);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `levels_id` int(11) DEFAULT 1,
  `experiences_id` int(11) DEFAULT 1,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` enum('tpe','age','col') DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `authorized` tinyint(4) DEFAULT 0,
  `blocked` tinyint(4) DEFAULT 0,
  `stored` tinyint(4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `levels_id`, `experiences_id`, `first_name`, `last_name`, `email`, `role`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `authorized`, `blocked`, `stored`) VALUES
(1, 3, 1, 'khalil', 'mecha', 'klilmecha@gmail.com', NULL, NULL, '$2y$10$HOdVM6dLiGKVBs.Q.FXqjOkq2rFLitAD2kqguLSvmjftj3S24YV46', NULL, '2021-08-30 13:35:11', '2021-08-30 13:35:11', 0, 0, 0),
(2, 3, 1, 'sufyane', 'Smida', 's.smida@jobid.fr', NULL, NULL, '$2y$10$kuYCZYD9KUWPbFG4g3JyQejYl8bJ5HAAKzXSykn53KOPp3ZdgYD8O', NULL, '2021-08-30 14:03:29', '2021-09-03 15:01:52', 0, 0, 0),
(3, 3, 1, 'Amine', 'Chouchaine', 'amine.chouchaine@gmail.com', NULL, NULL, '$2y$10$y97MRAEZ5YGFX/8n5oly2.at0MqPP6XedgYl4ZuZPTKCyuvCp1iza', NULL, '2021-08-31 11:28:53', '2021-08-31 11:28:53', 0, 0, 0),
(4, 3, 1, 'imen', 'mabrouki', 'mabroukiimen99@gmail.com', NULL, NULL, '$2y$10$vNNBl70q3ninUhukY9dkEOKJT1jemvGTZcw7gzNyoLV08c5q9VuGy', NULL, '2021-09-01 05:54:49', '2021-09-01 11:22:59', 0, 0, 0),
(5, 3, 1, 'zeineb', 'hamrouni', 'zeinebhmr@hotmail.com', NULL, NULL, '$2y$10$P.3xn01VVwSSSmdEVMm4Vuvh5cRJQ8JgRTWXjTwfe6DlKEBsN8bDq', NULL, '2021-09-01 06:09:51', '2021-09-01 06:09:51', 0, 0, 0),
(6, 3, 1, 'Racil', 'Ben Hadj Amor', 'racilbenhadjamor@gmail.com', NULL, NULL, '$2y$10$ig2tdhQ09t8UQJwB0X80G.p86849RjyMkQSE.KTB3S0OZNiCcshVe', NULL, '2021-09-01 06:11:11', '2021-09-01 06:11:11', 0, 0, 0),
(7, 3, 1, 'Zaineb', 'ben hammouda', 'zainebbenhammouda@gmail.com', NULL, NULL, '$2y$10$3.fvJuF9b.fCewZvL7tEze2mBIAL.vTbrHDQ6s9Ozhl6HrUnFKIce', NULL, '2021-09-02 11:09:33', '2021-09-03 13:20:31', 0, 0, 0),
(8, 3, 1, 'Ghazi', 'Arfa', 'ghaziarfaa@gmail.com', NULL, NULL, '$2y$10$ndgwz/nD.gQPwhuDgc394e194Tx.oAI30iAbd9U3nm9Frz7zZfspy', NULL, '2021-09-07 13:48:16', '2021-09-07 13:48:16', 0, 0, 0),
(9, 3, 1, 'waad', 'slimen', 'slimen.waa@gmail.com', NULL, NULL, '$2y$10$ESIKEBPEdAQLAwR6Pbl1IO.UQMwjYVN1mPNLE/ECJMs5uj3TcSfuu', NULL, '2021-09-18 09:36:17', '2021-09-18 09:36:17', 0, 0, 0),
(10, 3, 1, 'rahma', 'ajra', 'ajrarahma@gmail.com', NULL, NULL, '$2y$10$5AU.8xL8pfDdO0K/5EsnLOnmXw.8ILbZdr6kepVUfHBgz1SICCOFK', NULL, '2021-09-18 09:36:22', '2021-09-18 09:36:22', 0, 0, 0),
(11, 3, 1, 'ferchichi', 'mohamed', 'ferchichi9999@gmail.com', NULL, NULL, '$2y$10$Rw28hPglbxODgMss2jueHuQqxMoSeKTF.Hx6vVElcR06ihCS1PPnK', NULL, '2021-09-29 13:06:46', '2021-10-06 10:14:49', 0, 0, 0),
(12, 1, 1, 'ghjghj', 'dfg', 'novasis@gmx.com', 'age', NULL, '$2y$10$WDa4SdM4MiYuS1CUx66Bj.NeK6.2lQSDKOl7oVSZ/V9DCLEXxVUi6', NULL, '2021-10-04 22:10:58', '2021-10-04 22:10:58', 0, 0, 0),
(13, 1, 1, 'ghjghj', 'dfg', 'novasis@gmx.com', 'age', NULL, '$2y$10$mACiM54vlzaX6bBytjHip.hDYodh9AvFbRp4.QgPh3oFs5c3jOEJC', NULL, '2021-10-04 22:11:43', '2021-10-04 22:11:43', 0, 0, 0),
(14, 1, 1, 'ghjghj', 'dfg', 'novasis@gmx.com', 'age', NULL, '$2y$10$Ke7NqEXfNZAZ1.Rp3UKPt.Wd80u0pE/tLh2jIiQxU7F94unPF3sCa', NULL, '2021-10-04 22:24:03', '2021-10-04 22:24:03', 0, 0, 0),
(15, 1, 1, 'ghjghj', 'dfg', 'novasis@gmx.com', 'age', NULL, '$2y$10$Xi9oe4TGm0Rjxa/sHJe12.EBWVHrlvB1BaIBannt9bKIU3TQOb.Be', NULL, '2021-10-04 22:25:28', '2021-10-04 22:25:28', 0, 0, 0),
(16, 1, 1, 'ghjghj', 'dfg', 'novasis@gmx.com', 'age', NULL, '$2y$10$e2ERP0jt3624kmPwb2JYqu3lWPgYLrznE4GntIAWXbWh12CcaIPce', NULL, '2021-10-04 22:26:29', '2021-10-04 22:26:29', 0, 0, 0),
(17, 1, 1, 'yala', 'dfg', 'novasis@gmx.com', 'tpe', NULL, '$2y$10$Wi73xGA8kSiHu1lNxwgfPuBmmEyQdPvqAFYXH8H6vffiDZe1I4w96', NULL, '2021-10-04 22:30:11', '2021-10-04 22:30:11', 0, 0, 0),
(18, 1, 1, 'dfg dfgfdg', 'dfg', 'novasis@gmx.com', 'tpe', NULL, '$2y$10$VbXnx98aw1JcQZAx3r42KumtYhvZUUXoHkfPCmpDYWdIz4yl93bj2', NULL, '2021-10-05 09:08:06', '2021-10-05 09:08:06', 0, 0, 0),
(19, 1, 1, 'dfg dfgfdg', 'dfg', 'novasis@gmx.com', 'tpe', NULL, '$2y$10$eVXMLitd4KckRdPCzpg8teFiaSQvAt6dyKyx9s656cxyaF9bFJNU2', NULL, '2021-10-05 09:08:07', '2021-10-05 09:08:07', 0, 0, 0),
(20, 1, 1, 'kaiss ouni', 'dfg', 'novasis@gmx.com', NULL, NULL, '$2y$10$R/Iajf2IJE8hCmcVCBTGRegLLmfPiRhXrt0vojq4PD4BRBAFPtnpq', NULL, '2021-10-05 09:10:45', '2021-10-05 09:10:45', 0, 0, 0),
(21, 1, 1, 'kaiss ouni', 'dfg', 'novasis@gmx.com', 'tpe', NULL, '$2y$10$ysZ7RlPESdJUL/7p53OQPOPmz4MC0OV09rSxd.fuZp.4Vq.EQNYa2', NULL, '2021-10-05 09:11:02', '2021-10-05 09:11:02', 0, 0, 0),
(22, 1, 1, 'aaaaaaaaa', 'dfg', 'novasisdfgfdgdfg@gmx.com', 'tpe', NULL, '$2y$10$8M.1uu493i7GZubWJjcRJOPV889EXJAAzOLmWHkg5.Ys9k9mHQNIe', NULL, '2021-10-05 09:12:12', '2021-10-05 09:12:12', 0, 0, 0),
(23, 1, 1, 'mohamed', 'dfg', 'ferchichi@gmail.com', NULL, NULL, '$2y$10$rJYi2WjIN1gqRudmfnk6POcqQH93HEIlCC00fuEEtq6LtwR3UUeG.', NULL, '2021-10-05 09:43:49', '2021-10-05 09:43:49', 0, 0, 0),
(24, 1, 1, 'mohamed', 'dfg', 'novasis@gmx.com', NULL, NULL, '$2y$10$8TBHRbrgxaUNapaDpi9mU.BJU/IilER83h6yYfw6GFp3.eHfHGgP.', NULL, '2021-10-05 09:44:03', '2021-10-05 09:44:03', 0, 0, 0),
(25, 1, 1, 'mohamed', 'dfg', 'novasis@gmx.com', NULL, NULL, '$2y$10$J/IpQCbUoF53bGmxT5iS0uxwxDSbKwS5HOSWmCnv92wvPvSXq01qy', NULL, '2021-10-05 09:44:15', '2021-10-05 09:44:15', 0, 0, 0),
(26, 1, 1, 'mohamed', 'dfg', 'ferchichi@gmail.com', 'tpe', NULL, '$2y$10$hU4Em0C3X.0ip3k7.BEDbuJKuDPjh5gm5MrePwaZZaXZ2OunbXIL6', NULL, '2021-10-05 09:44:40', '2021-10-05 09:44:40', 0, 0, 0),
(28, 1, 1, 'ahmed', 'dfg', 'ahmed5888@gmx.com', 'tpe', NULL, '$2y$10$fcZWg0iwwroZGJUyQ7s/rO.5g7rweQL91yuxylFWc39TmGod1X9NK', NULL, '2021-10-05 10:16:49', '2021-10-05 10:16:49', 0, 0, 0),
(29, 1, 1, 'lala laal', 'dfg', 'lalla@att.net', 'age', NULL, '$2y$10$RR3g6KHq7v5bIGsdRWKk9OnQrrIvZkuhKNQMr.3XKxT9/TUJqzAo.', NULL, '2021-10-05 10:22:33', '2021-10-05 10:22:33', 0, 0, 0),
(30, 1, 1, 'cpn-crm', 'dfg', 'cpn-crm@att.net', 'tpe', NULL, '$2y$10$YINn6hHuOzDQyw2.ynLgiO8Ic5NFVpcLgSibMGC/2dNcAiKmsKqge', NULL, '2021-10-05 10:24:49', '2021-10-05 10:24:49', 0, 0, 0),
(31, 1, 1, 'fervchi', 'dfg', 'ferchichi@att.net', 'tpe', NULL, '$2y$10$iiXRKzTvaO71icL3tjRAlehg8uSOlIHyoC5/NwyctLEqqtVLGm.hu', NULL, '2021-10-05 10:28:52', '2021-10-05 10:28:52', 0, 0, 0),
(32, 1, 1, 'fdhfgh', 'dfg', 'novasisfghgf@gmx.com', NULL, NULL, '$2y$10$EJqjxOpHtItYZIxoGJEDfOwl12tW9F3RWiIhF2xYXzsuHVF/x1Ape', NULL, '2021-10-05 10:45:07', '2021-10-05 10:45:07', 0, 0, 0),
(33, 1, 1, 'kaiss ouni', 'dfg', 'novasis@gmx.com', 'tpe', NULL, '$2y$10$zLc8Zt1Msb3BvXVT1JtUR.db/w2fWPjP1c2UT7k62OJSn7Mt3dQpe', NULL, '2021-10-05 10:45:19', '2021-10-05 10:45:19', 0, 0, 0),
(34, 1, 1, 'sdfdsf', 'dfg', 'novasis000000@gmx.com', NULL, NULL, '$2y$10$8ZsFCwJbL2/AcPvDz90MOujaLbAqhdwEq63G0zLy05g5Oexgkqfhu', NULL, '2021-10-05 10:45:58', '2021-10-05 10:45:58', 0, 0, 0),
(35, 1, 1, 'sdfdsf', 'dfg', 'novasis@gmx.com', 'age', NULL, '$2y$10$/hwi8xVWNeg2fEP8TokAau6hYkxecKrG54QZVJDq6ULd.fs3awpUe', NULL, '2021-10-05 10:46:12', '2021-10-05 10:46:12', 0, 0, 0),
(36, 1, 1, 'kaiss ouni', 'dfg', 'back@gmx.com', NULL, NULL, '$2y$10$qB8ql3qbnXgH/GvG7PBlPeASqg3vIMHs/DRPGuCSfKSFWnZpWCl/e', NULL, '2021-10-05 10:50:13', '2021-10-05 10:50:13', 0, 0, 0),
(37, 1, 1, 'kaiss ouni', 'dfg', 'novasis@gmx.com', 'tpe', NULL, '$2y$10$8A542d6oJ38QVdZYQSMpJO/0WdmyXLpleoONrJVbhgqnVbfEdgVOC', NULL, '2021-10-05 10:50:24', '2021-10-05 10:50:24', 0, 0, 0),
(38, 1, 1, 'kaiss ouni', 'dfg', 'novasis587@gmail.com', 'age', NULL, '$2y$10$oECPH3rTcTNs5JXANcCh8.74959fSRIGwe3wbWDbDlQ2GAW92Rqx.', NULL, '2021-10-07 14:06:01', '2021-10-07 14:06:01', 0, 0, 0),
(39, 1, 1, 'kaiss ouni', 'dfg', 'email@gmx.com', 'age', NULL, '$2y$10$QO890BLDW5btV0eiDSvPkO8UxiayNv/eqYj/Sye6I5jGuUMXLTCyO', NULL, '2021-10-07 14:31:36', '2021-10-07 14:31:36', 0, 0, 0),
(40, 1, 1, 'cpn-aide-aux', 'dfg', 'oxllade@gmx.com', 'age', NULL, '$2y$10$ubwt99dtjDZrXH5JXAkHoOoQxNvMBUofpDhOc52uxazyepxkpq1Nu', NULL, '2021-10-07 14:32:33', '2021-10-07 14:32:33', 0, 0, 0),
(41, 1, 1, 'kais ouni', 'dfg', 'novasis742345@gmx.com', NULL, NULL, '$2y$10$5RO.lm.HKd09Qy/y5uWagOze9ULu9b6A.esfiIAvJjc4LZWMyeCx.', NULL, '2021-10-07 14:34:47', '2021-10-07 14:34:47', 0, 0, 0),
(42, 1, 1, 'kaiss ouni', 'dfg', 'novasis534534543@gmx.com', NULL, NULL, '$2y$10$DiH9/2AOsewcNi8SMIEr8uDKWjEzJZ0QRfcUHU3F0Zepw0zEU.sHC', NULL, '2021-10-07 14:35:00', '2021-10-07 14:35:00', 0, 0, 0),
(43, 1, 1, 'kais ouni', 'dfg', 'novasis7453543@gmx.com', NULL, NULL, '$2y$10$TWc5UZmBgSMxQoyykJmgIOn7/fWoV0WZbXRljN6.8ar0XkehWGePi', NULL, '2021-10-07 14:35:16', '2021-10-07 14:35:16', 0, 0, 0),
(44, 1, 1, 'kaiss ouni', 'dfg', 'novasis@gmx.com', 'age', NULL, '$2y$10$.wEZ2itoZbJLCEvwcmylM.Ej5p2aX35.AmMxB6Fsnyn.vh9AxOJ3C', NULL, '2021-10-07 14:35:27', '2021-10-07 14:35:27', 0, 0, 0),
(45, 1, 1, 'dfgfdg', 'dfg', 'dfgfdg213123@gmx.com', 'tpe', NULL, '$2y$10$i7lCIt/6BnukTp9ii2J43O8WxVt0XvB/wdMvanwFBB/r3Hh8Nc.MO', NULL, '2021-10-07 14:50:37', '2021-10-07 14:50:37', 0, 0, 0),
(46, 1, 1, 'dfgfdg', 'dfg', 'dfgfdg213123111@gmx.com', 'tpe', NULL, '$2y$10$3YcIZ4e78D8j3uVpHAPJEO3Qo8j9a.MRbq5pC2XIfGyfxHCiFvUWa', NULL, '2021-10-07 15:02:49', '2021-10-07 15:02:49', 0, 0, 0),
(47, 1, 1, 'crm-aide', 'dfg', 'crm-aide-@att.net', NULL, NULL, '$2y$10$ZPIagf7jMhC.v9kGlkOfK.kGH5wUMM2asdtQdG1xCB1i4qus0NVHe', NULL, '2021-10-07 15:11:16', '2021-10-07 15:11:16', 0, 0, 0),
(48, 1, 1, 'crm aide', 'dfg', 'crm-aide-@att.net', NULL, NULL, '$2y$10$xKURZEOjy5JuOnzQG4A72u0yi7uFce9IQK1IV/Y/DN0hSyGVE1pLm', NULL, '2021-10-07 15:11:37', '2021-10-07 15:11:37', 0, 0, 0),
(49, 1, 1, 'crm', 'dfg', 'crm-aide-aux@gmx.com', NULL, NULL, '$2y$10$NO3yld0ujxqV1QMZXsI5Ues1bo3La0J5D/EYqOnfJT668VvBdsQai', NULL, '2021-10-07 15:11:56', '2021-10-07 15:11:56', 0, 0, 0),
(50, 1, 1, 'crm-aide', 'dfg', 'crm-aide-aux@gmx.com', 'tpe', NULL, '$2y$10$ezsuX6iIcelnux55W0KriueSTbW8u40BNGnKwVJwdUfQ7yzOt3PFy', NULL, '2021-10-07 15:12:19', '2021-10-07 15:12:19', 0, 0, 0),
(51, 1, 1, 'fghgf', 'dfg', 'fghfghgf12321@gmx.com', 'tpe', NULL, '$2y$10$0CJRYuK6owVdp4FZqXjTj.RxEIWNY7g32Cz4/Dqd9LpOz8DIQzUI6', NULL, '2021-10-07 15:24:27', '2021-10-07 15:24:27', 0, 0, 0),
(52, 1, 1, 'fghgf', 'dfg', 'fghfghgf12321@gmx.com', 'tpe', NULL, '$2y$10$5zgd9XQCe1XUMtXIScsLLOuPrnjJmivML0P1bHiIwpERCMNzhICNW', NULL, '2021-10-07 15:25:43', '2021-10-07 15:25:43', 0, 0, 0),
(53, 1, 1, 'fghgf', 'dfg', 'fghfghgf12321@gmx.com', 'tpe', NULL, '$2y$10$Ey5K6vZaDpcLoCjtOKhVL.8ZiERcnyjV6sS/lgo2BjOJbqKtpZmvi', NULL, '2021-10-07 15:25:57', '2021-10-07 15:25:57', 0, 0, 0),
(54, 1, 1, 'fghgf', 'dfg', 'fghfghgf12321@gmx.com', 'tpe', NULL, '$2y$10$6HoNdlLkQ9mfo7IS.F61Ue1msZtWu/S9nW1GDLSbbt/RSTmYgzFOy', NULL, '2021-10-07 15:26:25', '2021-10-07 15:26:25', 0, 0, 0),
(55, 1, 1, 'fghgf', 'dfg', 'fghfghgf12321@gmx.com', 'tpe', NULL, '$2y$10$fU1VI9ujSanBEMA7nm9pzOekQ1MsAFDckO8GN8nd2LSHRV1fk05xa', NULL, '2021-10-07 15:26:46', '2021-10-07 15:26:46', 0, 0, 0),
(56, 1, 1, 'fghgf', 'dfg', 'fghfghgf12321@gmx.com', 'tpe', NULL, '$2y$10$OhU7y0gE1SkPdpYQEa4HeOOodJKNkqOGDovrYzCF.XXz.HSZiYAdC', NULL, '2021-10-07 15:27:25', '2021-10-07 15:27:25', 0, 0, 0),
(57, 1, 1, 'dfgfdgfd', 'dfg', '1231233@gmx.com', 'tpe', NULL, '$2y$10$X5UhFU5rtjI4nFqfcmUuBu9ivjmu03XJPYxiIHdf454MEzeSD8RnO', NULL, '2021-10-07 15:27:53', '2021-10-07 15:27:53', 0, 0, 0),
(58, 1, 1, 'dfg dfgfdg', 'dfg', 'novasis12313@gmx.com', 'age', NULL, '$2y$10$wIYChQynkNfGs2TulS8/..70dfJHDy2J.Xkk0Tc5ExNqNOLqUMOQ.', NULL, '2021-10-07 19:25:06', '2021-10-07 19:25:06', 0, 0, 0),
(59, 1, 1, 'kilwa', 'dfg', 'novasis45646@gmx.com', 'tpe', NULL, '$2y$10$AgcPoO7wsS9ViR6POjYm1OC7Rx4q.LKVYCc7sJNMQh9HSXbZRXuGi', NULL, '2021-10-08 08:44:07', '2021-10-08 08:44:07', 0, 0, 0),
(60, 1, 1, 'sdfdsf sdfsd', 'dfg', 'finaltest@gmx.com', 'age', NULL, '$2y$10$RZrix07KmNrzRLDFOUbNleKm/gjY16lfuaD.B11DXVOi9gcB/ZCJi', NULL, '2021-10-08 09:32:57', '2021-10-08 09:32:57', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `verifications`
--

CREATE TABLE `verifications` (
  `id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `type` varchar(45) NOT NULL,
  `code` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `used` tinyint(4) NOT NULL DEFAULT 0,
  `deleted` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `verifications`
--

INSERT INTO `verifications` (`id`, `users_id`, `type`, `code`, `created_at`, `updated_at`, `used`, `deleted`) VALUES
(1, NULL, 'eligibility', 'IPOt6KJG8wtGxjf9AHSOAqzNqn3Rr9wD', '2021-09-06 08:51:38', '2021-09-06 08:51:38', 0, 0),
(2, NULL, 'eligibility', '6MXFYrRxiUYWGeg4x8BUa2ngRSem75nU', '2021-09-06 08:51:47', '2021-09-06 08:51:47', 0, 0),
(3, NULL, 'eligibility', 'zJvIVTeAb5bfAvKWv838WeUlvV9uOpHJ', '2021-09-06 08:51:52', '2021-09-06 08:51:52', 0, 0),
(4, NULL, 'eligibility', '8xO6lNDGMuLT0CsYQiEH5OjYtHi2Efjv', '2021-09-06 08:51:56', '2021-09-06 08:51:56', 0, 0),
(5, NULL, 'eligibility', 'HSGfgyKXcKAe27xDR9asYC9KUUTjqrrs', '2021-09-06 08:52:13', '2021-09-06 08:52:13', 0, 0),
(6, NULL, 'eligibility', 'boNOBOkroLPwI0QStRNt6QGYO3q1Gcnm', '2021-09-06 09:01:52', '2021-09-06 09:01:52', 0, 0),
(7, NULL, 'eligibility', 'Olc7mPXHneykqTLYzCUodnBYsu4N2T2J', '2021-09-06 09:01:59', '2021-09-06 09:01:59', 0, 0),
(8, NULL, 'eligibility', 'dmHn59T2q1kLT8qzzpsLVeKHOwtTsh3M', '2021-09-06 09:06:56', '2021-09-06 09:06:56', 0, 0),
(9, NULL, 'eligibility', '1D3r21EtdzbsNCJBMhyxxSOZkjsB3qjW', '2021-09-06 09:07:59', '2021-09-06 09:07:59', 0, 0),
(10, NULL, 'eligibility', '3wjPolPRrBIEqryIAN3vDLg62Wm7oxi8', '2021-09-06 09:13:46', '2021-09-06 09:13:46', 0, 0),
(11, NULL, 'eligibility', 'yTy6homTWTemVOkazSKwV7MKRqrUkqZs', '2021-09-06 09:20:19', '2021-09-06 09:20:19', 0, 0),
(12, NULL, 'eligibility', '5StynYaOt78YLFzkQF5Y9JNqFJA8CWWe', '2021-09-06 09:22:04', '2021-09-06 09:22:04', 0, 0),
(13, NULL, 'eligibility', '17CcbGy6aUZXOsgql2C43yfDxqs295px', '2021-09-06 09:23:47', '2021-09-06 09:23:47', 0, 0),
(14, NULL, 'eligibility', 'BIT7AKvRmEVuGkx3SmhSov3Q2vIAGRWm', '2021-09-06 09:36:22', '2021-09-06 09:36:22', 0, 0),
(15, NULL, 'eligibility', 'CnM26f5NogzbUq5dIjVy5zms8If22Rbz', '2021-09-06 09:36:38', '2021-09-06 09:36:38', 0, 0),
(16, NULL, 'eligibility', 'ohdi7yPBP3pmE6SnYt6tn5VnyX73nRPd', '2021-09-06 09:37:26', '2021-09-06 09:37:26', 0, 0),
(17, NULL, 'eligibility', '4kvZBU6rqG9cxWjlknSdREOZP07XOqW6', '2021-09-06 09:38:18', '2021-09-06 09:38:18', 0, 0),
(18, NULL, 'eligibility', 'oYTPFIMmIgskxN7TZYNBIlpuliYFUErg', '2021-09-06 09:40:05', '2021-09-06 09:40:05', 0, 0),
(19, NULL, 'eligibility', 'YQ98IZcDvHC3HQpOdXt9eHBLsc0MA6SH', '2021-09-06 09:52:45', '2021-09-06 09:52:45', 0, 0),
(20, NULL, 'eligibility', 'QezhBs9MACn0LOPVHEqOskzv6kM3MISe', '2021-09-06 10:08:31', '2021-09-06 10:08:31', 0, 0),
(21, NULL, 'eligibility', 'vYiOQUpQKQbEDnJDE1EuWAvCm2wy3MQ9', '2021-09-06 10:44:18', '2021-09-10 14:31:56', 0, 0),
(22, NULL, 'eligibility', 'xEeYSRDxY6kQmNWUR7NtaFjCfKxltHhS', '2021-09-06 14:46:21', '2021-09-06 14:46:21', 0, 0),
(23, NULL, 'eligibility', 'xm3rRs7b4gJZToUmdsenETKIYTnVVZTh', '2021-09-06 14:46:26', '2021-09-06 14:46:26', 0, 0),
(24, NULL, 'eligibility', 'UWzGUyH1qdH3Dk9ZZlL4VZbICQF20qbo', '2021-09-06 14:50:28', '2021-09-10 14:31:58', 0, 0),
(25, NULL, 'eligibility', 'llxyVICkTyOSgS7ipDuCBsE3bwTHHCYV', '2021-09-06 15:03:12', '2021-09-06 15:03:12', 0, 0),
(26, NULL, 'eligibility', 'Gw4F5M1z7sNJziT9duJG38BvqRYGDCVY', '2021-09-07 08:29:59', '2021-09-10 14:32:01', 0, 0),
(27, NULL, 'eligibility', 'Fkv49rMsc0gyp6W8JDZ2nPa2KVDDOwOO', '2021-09-07 11:23:40', '2021-09-07 11:23:40', 0, 0),
(28, NULL, 'eligibility', 'cQCqIGVbDiq3QN9cDILFMJGPHEiB1blI', '2021-09-07 13:02:30', '2021-09-10 14:32:03', 0, 0),
(29, NULL, 'eligibility', '0NDTy7LwZB3FuHUptlGU7HGb9AsWtYRS', '2021-09-07 14:28:48', '2021-09-07 14:28:48', 0, 0),
(30, NULL, 'eligibility', 'HRsk0I8lLaMmHHofb3Xuq4ZiXFoxzVDX', '2021-09-07 15:49:48', '2021-09-07 15:49:48', 0, 0),
(31, NULL, 'eligibility', 'ovl63Ad8bVHymtDugvK814geZ07N8FKX', '2021-09-08 09:50:04', '2021-09-08 09:50:04', 0, 0),
(32, NULL, 'eligibility', 'VNZbsOLFd1U8Xmr91h1XHrG08ezCGchP', '2021-09-08 13:06:37', '2021-09-09 12:43:00', 1, 0),
(33, NULL, 'eligibility', 'tx0K3E5OTtlB76wQrXILGbOz95mg4CdU', '2021-09-08 13:49:41', '2021-09-08 13:49:41', 0, 0),
(34, NULL, 'eligibility', 'oybo4v3VUXcXjzyxxWiuHj14lTzCibtH', '2021-09-08 14:37:34', '2021-09-09 10:17:31', 1, 0),
(35, NULL, 'eligibility', '2V2HktIPjjpTRGHAnDkdEBuKjopHGwvP', '2021-09-08 15:31:54', '2021-09-08 15:31:54', 0, 0),
(36, NULL, 'eligibility', 'liPBhQKncF8q5ll1dXGCZbELOhunEGxs', '2021-09-08 15:31:58', '2021-09-08 15:31:58', 0, 0),
(37, NULL, 'eligibility', 'MCTYPjZ4siIJC9Y9T21hGlwfnqLFxfwf', '2021-09-08 15:32:10', '2021-09-08 15:32:10', 0, 0),
(38, NULL, 'eligibility', 'gWh6GnIETXcgBVsTtGRoxFD0FZoPOlax', '2021-09-08 15:32:14', '2021-09-08 15:32:14', 0, 0),
(39, NULL, 'eligibility', 'YamxpqKRlXSHriTEsvxbk5YAZCx6DL3G', '2021-09-08 15:32:23', '2021-09-08 15:32:23', 0, 0),
(40, NULL, 'eligibility', 'xI5cLp93nS88ui9kfl4x3UZLve1aVJE2', '2021-09-08 15:32:30', '2021-09-08 15:32:30', 0, 0),
(41, NULL, 'eligibility', 'GKufYaU1r0oa0aGw0YJa1badWVSetxAn', '2021-09-08 15:32:41', '2021-09-08 15:32:41', 0, 0),
(42, NULL, 'eligibility', 'eggxvJmFAMAMYUV7Z1UdDvcwU6l4r7Rh', '2021-09-08 15:33:16', '2021-09-08 15:33:16', 0, 0),
(43, NULL, 'eligibility', 'nE1rT8TrWxCVjpIvJ3x3aJxB5AUHvtnc', '2021-09-08 15:41:27', '2021-09-08 15:41:27', 0, 0),
(44, NULL, 'eligibility', 'atZILvpBbt1feE7NuMSpxca3jZWGL50c', '2021-09-09 08:40:29', '2021-09-09 09:40:33', 1, 0),
(45, NULL, 'eligibility', 'y038VAmdo2VUGnZzVZyGVRQDJ4AkbjAe', '2021-09-09 08:47:32', '2021-09-09 08:47:32', 0, 0),
(46, NULL, 'eligibility', 'YnewqjyLdXZnFCFWtJLIJ5rE4q4Sg2ao', '2021-09-09 12:42:18', '2021-09-09 12:42:18', 0, 0),
(47, NULL, 'eligibility', 'Sk93k3SAo6X86qUvv7mNNOCfytwsry59', '2021-09-09 12:51:46', '2021-09-09 12:51:46', 0, 0),
(48, NULL, 'eligibility', 'Kcte0neKbfU9zkU4GQpDm9gsYU1Feszb', '2021-09-09 12:52:01', '2021-09-09 12:52:01', 0, 0),
(49, NULL, 'eligibility', 'fVdrXXcZfdMaewh4UgUMDbKnULrnsglF', '2021-09-09 12:53:28', '2021-09-09 12:53:28', 0, 0),
(50, NULL, 'eligibility', '36t1XRrR0Kn9juABDvCzak0pvPciKLha', '2021-09-09 14:57:29', '2021-09-09 14:57:29', 0, 0),
(51, NULL, 'eligibility', 'InOzYHSTzGhUQ8KgLai6UbrGKCsyKhQc', '2021-09-10 08:45:59', '2021-09-10 08:45:59', 0, 0),
(52, NULL, 'eligibility', 'p1NH3fvCtm3s1e4N33PDE5ffbPuZxPgb', '2021-09-10 10:57:29', '2021-09-10 10:57:29', 0, 0),
(53, NULL, 'eligibility', 'phL1AnR7Uq9t6HlPsu7GhtqvLIkiUVSr', '2021-09-10 11:26:34', '2021-09-10 11:29:13', 1, 0),
(54, NULL, 'eligibility', 'Yfw7q9Ar815ZRoxS9v8iGJAGnCbkMzUT', '2021-09-10 13:10:58', '2021-09-10 16:15:35', 1, 0),
(55, NULL, 'eligibility', 'MpDSyTVxylHuwzVeRn7q8ZGdgorR7A4l', '2021-09-10 15:04:03', '2021-09-10 15:04:03', 0, 0),
(56, NULL, 'eligibility', 'Dvb5lvLYVgukw8jmCqcYUIPPIIu7dNCG', '2021-09-10 15:04:14', '2021-09-10 15:04:14', 0, 0),
(57, NULL, 'eligibility', '9ZxNsTBKcsxPZRNIBUHOAN7BzKSPR3jH', '2021-09-10 15:04:30', '2021-09-10 15:04:30', 0, 0),
(58, NULL, 'eligibility', 'elaORdYx01ovFvDxAS5vuvmjXWCjuUEJ', '2021-09-10 15:04:50', '2021-09-10 15:04:50', 0, 0),
(59, NULL, 'eligibility', '3sx5VDihpzI9SbI2UeWtbQLY4yqnGEe0', '2021-09-10 15:05:19', '2021-09-10 15:05:19', 0, 0),
(60, NULL, 'eligibility', 'W4USHGxI1ridrjcDSk37WDuoF0FFYuSD', '2021-09-10 15:21:31', '2021-09-10 15:21:31', 0, 0),
(61, NULL, 'eligibility', 'kaHgNZtQ88VeMYiSDoQU1PWzCsVxR1LL', '2021-09-10 15:21:34', '2021-09-10 15:21:34', 0, 0),
(62, NULL, 'eligibility', 'YiyTtMQyNiEwIlhQxyLBUxwdc8Yzq0Af', '2021-09-10 15:22:10', '2021-09-10 15:22:10', 0, 0),
(63, NULL, 'eligibility', 'Uf31zIpEoQFNZu3XPLWXT6xQQW4FQsMa', '2021-09-10 15:22:53', '2021-09-10 15:22:53', 0, 0),
(64, NULL, 'eligibility', 'NGRB1Q9sCX4MgquRyvv0rZWJiBQF0ljZ', '2021-09-10 15:27:40', '2021-09-10 15:27:40', 0, 0),
(65, NULL, 'eligibility', '6a4opYvuKoX6uZyUGUPtUeYzQPPswbV5', '2021-09-13 08:12:36', '2021-09-13 09:11:53', 1, 0),
(66, NULL, 'eligibility', 'U9VsOFsPgntJYtAVEx2t7pxBnIh6DmPo', '2021-09-13 08:12:52', '2021-09-13 08:12:52', 0, 0),
(67, NULL, 'eligibility', 'S1a2ZQSSjK3l0pD59HFRGqY30tUL45yp', '2021-09-13 08:39:44', '2021-09-13 08:39:44', 0, 0),
(68, NULL, 'eligibility', 'sx1OG856scPuGe0o4FE0cFqmgpqlchOf', '2021-09-13 08:56:08', '2021-09-20 09:21:16', 1, 0),
(69, NULL, 'eligibility', 'lBzYPiTMgKbj6R9xRRqPfczSfzRMvulP', '2021-09-13 09:41:53', '2021-09-13 09:41:53', 0, 0),
(70, NULL, 'eligibility', 'LKZ6gDSFJvqKlSFuv7ERo9RoMwrGakLU', '2021-09-13 09:42:00', '2021-09-13 09:42:00', 0, 0),
(71, NULL, 'eligibility', 'qKVC247yG8WxiBPRQcMK3j7JonINgWoZ', '2021-09-13 13:55:32', '2021-09-13 13:55:32', 0, 0),
(72, NULL, 'eligibility', 'PLoOJMsjbgKxNwqhUMZx2U2hWysSC4dc', '2021-09-13 13:55:43', '2021-09-13 13:55:43', 0, 0),
(73, NULL, 'eligibility', '2AfRy3O72O6FIDwXHEIiMOp4HTNjUi87', '2021-09-13 13:55:53', '2021-09-13 13:55:53', 0, 0),
(74, NULL, 'eligibility', 'x801oKhifjd5nos8dQm1vhzhhpWwsTyc', '2021-09-13 14:13:40', '2021-09-13 15:00:26', 1, 0),
(75, NULL, 'eligibility', 'CihdOIHEidfAsDrJIGXTmnV1RO6VgVvf', '2021-09-13 14:53:56', '2021-09-13 14:53:56', 0, 0),
(76, NULL, 'eligibility', 'qWJfRH09MF0o8Kihx4tyn8ta3aBByT68', '2021-09-13 14:59:27', '2021-09-13 14:59:27', 0, 0),
(77, NULL, 'eligibility', 'MvCZLW5a74ixr59tmv6FkBVRQldGRMI3', '2021-09-13 15:42:40', '2021-09-13 15:42:40', 0, 0),
(78, NULL, 'eligibility', '04QqqMuTlOiQ8IGl1Os4BOXyJjKyzbtI', '2021-09-14 09:09:05', '2021-09-14 09:09:05', 0, 0),
(79, NULL, 'eligibility', 'd3mIVAgtCHeGNWWHJs98SH3gLjtD3x9G', '2021-09-14 09:41:54', '2021-09-14 09:42:17', 1, 0),
(80, NULL, 'eligibility', 'nkORWLNSumRWFbN7Ue7inaMkTY7lFKTp', '2021-09-14 11:08:55', '2021-09-14 11:08:55', 0, 0),
(81, NULL, 'eligibility', 'D7bjC4ORKFl5qJNvxFAsk05r5R7LIIuS', '2021-09-14 13:23:36', '2021-09-14 13:23:36', 0, 0),
(82, NULL, 'eligibility', 'lXyNXPkXqok3cnnIQASs4oknAWWXYo3N', '2021-09-14 13:23:57', '2021-09-14 13:23:57', 0, 0),
(83, NULL, 'eligibility', 'q0fkfv0RXyiq0Lh7NcHxtuiMORYKoIni', '2021-09-14 13:25:01', '2021-09-14 13:25:01', 0, 0),
(84, NULL, 'eligibility', 'hzMcrrZTNXx9McKmWhB89aKQbHuvAw8J', '2021-09-14 13:30:33', '2021-09-14 13:30:33', 0, 0),
(85, NULL, 'eligibility', 'ekmbnmeRkoqdFPVOGVIrZCdr0M8DM9gD', '2021-09-14 13:32:46', '2021-09-14 13:32:46', 0, 0),
(86, NULL, 'eligibility', 'L3gHsvVxuFJWKdANyjnaH3OUohAhYUVr', '2021-09-14 13:34:10', '2021-09-14 13:34:10', 0, 0),
(87, NULL, 'eligibility', 'XrnwRKidgjNgChZcqzTRIvX3FWcK2Hb5', '2021-09-14 13:36:19', '2021-09-14 13:37:42', 1, 0),
(88, NULL, 'eligibility', 'XojqHpy738488EigSGFZBAvQJdSEQnHc', '2021-09-14 13:38:48', '2021-09-14 13:38:48', 0, 0),
(89, NULL, 'eligibility', 'ZAcLEKIWqT12jlz0CULqe9q52s50fvpn', '2021-09-14 13:43:12', '2021-09-14 13:43:12', 0, 0),
(90, NULL, 'eligibility', 'OQJx0l4F8ADFU2EtTNVWkPNbvSoRcfCL', '2021-09-14 13:51:02', '2021-09-14 13:53:48', 1, 0),
(91, NULL, 'eligibility', 'OghI1aJazOlA2C6E5iJKb7420DsSGWpx', '2021-09-15 08:46:57', '2021-09-15 08:46:57', 0, 0),
(92, NULL, 'eligibility', 'PDWv7KX3Wj8ya2XNqI2IiaDGiKkPxCo3', '2021-09-15 10:11:32', '2021-09-15 10:11:51', 1, 0),
(93, NULL, 'eligibility', 'FX1zctL4MserQpC1xNRcwv2RFYc1lwE5', '2021-09-15 10:19:03', '2021-09-15 12:07:47', 1, 0),
(94, NULL, 'eligibility', 'JxxRGi8RzVD5PK1F9ltsmwi1RBxSwXPM', '2021-09-15 14:46:02', '2021-09-15 14:46:02', 0, 0),
(95, NULL, 'eligibility', 'sCLwIsXn5KzVE1ajg2ZGbSo9wdtSExin', '2021-09-15 14:59:29', '2021-09-15 14:59:29', 0, 0),
(96, NULL, 'eligibility', 'vexC7YcGI1XtcqsdLfJiPJbh53tHRfnh', '2021-09-15 14:59:42', '2021-09-15 14:59:42', 0, 0),
(97, NULL, 'eligibility', 'cGeZivisJ1k23Zxt397beOxFmQe4NILo', '2021-09-15 15:08:29', '2021-09-16 08:46:37', 1, 0),
(98, NULL, 'eligibility', 'ZCHbr8lmBVko534DZgzbfCeBxD0OmPQU', '2021-09-15 15:45:21', '2021-09-15 15:45:21', 0, 0),
(99, NULL, 'eligibility', 'aI7R1heGqOno94sPUBZSO7lh1zb8YP0D', '2021-09-15 15:52:19', '2021-09-15 15:53:15', 1, 0),
(100, NULL, 'eligibility', 'twmNdLIV3OpRWvqxsPblgLmSutfU4Iip', '2021-09-16 08:57:55', '2021-09-16 08:59:32', 1, 0),
(101, NULL, 'eligibility', 'x7ls23ftbwgERyNQGmrg3OWjlZ63w2yo', '2021-09-16 10:37:23', '2021-09-16 10:37:23', 0, 0),
(102, NULL, 'eligibility', 'TW0wdxZ0wMFUAvnkyUmswS4dX8IE1fn0', '2021-09-17 08:45:45', '2021-09-17 13:19:43', 1, 0),
(103, NULL, 'eligibility', 'eyyYwHXBc1HIWjxcbpu6ydKGaM2zJsxf', '2021-09-17 14:12:43', '2021-09-17 14:12:43', 0, 0),
(104, NULL, 'eligibility', 'MEvcZiN9ObHUTdiWqgrbiK3D141wno2P', '2021-09-17 14:13:51', '2021-09-17 14:13:51', 0, 0),
(105, NULL, 'eligibility', 'UvgImd0f3WHAOTYD9LUt8s93mMDcuPUB', '2021-09-21 08:30:38', '2021-09-21 08:30:38', 0, 0),
(106, NULL, 'eligibility', 'IjWu5yrHeigK1qJBlOqnmS8KCpfxI67x', '2021-09-21 08:30:44', '2021-09-21 08:30:44', 0, 0),
(107, NULL, 'eligibility', 'vfZbp22ezZm9CkSvetpsaw6cE5djD9uK', '2021-09-21 08:32:10', '2021-09-21 08:32:10', 0, 0),
(108, NULL, 'eligibility', 'a1xQ8RAVmZCnC54jf3v4YLBkTZyO4w95', '2021-09-21 08:32:25', '2021-09-21 08:32:25', 0, 0),
(109, NULL, 'eligibility', '5y4rBSGMmRjsJufr1VzRmHCAfCummyjf', '2021-09-21 08:52:53', '2021-09-21 08:52:53', 0, 0),
(110, NULL, 'eligibility', 'TBaoeVlUH4S6pqiLPDrzfq6ktFwtInj9', '2021-09-21 08:53:07', '2021-09-21 08:53:07', 0, 0),
(111, NULL, 'eligibility', 'smOJurgY5jKRqHe543NeAZJYd3qA1TN8', '2021-09-21 08:53:35', '2021-09-21 08:53:35', 0, 0),
(112, NULL, 'eligibility', 'kHYNSwNlmgWIzVSPOByBth0PdRovVWhV', '2021-09-21 08:53:54', '2021-09-21 08:53:54', 0, 0),
(113, NULL, 'eligibility', 'm1ScrF4rKmbD7JdfmWCBKiBTVNq5YHKL', '2021-09-21 09:02:34', '2021-09-21 09:02:34', 0, 0),
(114, NULL, 'eligibility', 'RhfhrdrrKskM1QxWxpwztjZDfsTivj7T', '2021-09-21 09:23:15', '2021-09-21 09:23:15', 0, 0),
(115, NULL, 'eligibility', 'Mpio4afxAZdJH1zOxjzu9hecQIHOBiAS', '2021-09-21 13:36:51', '2021-09-21 13:36:51', 0, 0),
(116, NULL, 'eligibility', 'CZlXMn9PnBGBqdQnP66lj1KZx2ufPr6c', '2021-09-21 14:46:37', '2021-09-21 14:46:37', 0, 0),
(117, NULL, 'eligibility', 'wuEnEFTTfcC0h5eJwbEGLR4ZVBtgvLZy', '2021-09-22 10:45:16', '2021-09-22 10:45:16', 0, 0),
(118, NULL, 'eligibility', 'GKghtMdG2sQ3TIhr4I9herbOv72LplCi', '2021-09-22 12:26:59', '2021-09-22 12:26:59', 0, 0),
(119, NULL, 'eligibility', 'c6KDo85b1sCfYFRpU46tpDfpIxQ2TInz', '2021-09-23 08:19:23', '2021-09-24 06:32:43', 1, 0),
(120, NULL, 'eligibility', 'kP2kqT45q9cR73o2Mp1vZwIst7yx4dLo', '2021-09-23 12:58:54', '2021-09-24 15:24:51', 1, 0),
(121, NULL, 'eligibility', 'MecZuZlQf2d9TAtyOHhJDVcCE18B3noc', '2021-09-24 09:35:47', '2021-09-24 09:35:47', 0, 0),
(122, NULL, 'eligibility', 'LFAdKjTEb96VitbNk1oyg1qhjghHVSeG', '2021-09-24 10:40:19', '2021-09-24 10:40:19', 0, 0),
(123, NULL, 'eligibility', 'FBMASGw0MqiLeuZiWUsOlczc7TKHmz7E', '2021-09-24 14:21:15', '2021-09-24 14:22:05', 1, 0),
(124, NULL, 'eligibility', 'z4PjpUigmvbDG1MAwsRaHNYSRer6ieYs', '2021-09-27 08:43:40', '2021-09-27 08:43:40', 0, 0),
(125, NULL, 'eligibility', 'k3jLtcQbEEtQlCT95kiRyUxaYxlHvZgt', '2021-09-27 08:45:12', '2021-09-27 08:45:12', 0, 0),
(126, NULL, 'eligibility', 'Z2ROc1UtG52AYKq0q0iXX0zLh1yGiZTo', '2021-09-27 08:49:04', '2021-09-27 08:49:04', 0, 0),
(127, NULL, 'eligibility', 'NhaMDN0P0rfbAwVAfFKTWJlhFnQh1pg0', '2021-09-27 09:53:02', '2021-09-27 09:53:02', 0, 0),
(128, NULL, 'eligibility', 'y2MTwTtUmxSdLGLGa4j7ZANK5afdYuk9', '2021-09-27 11:00:46', '2021-09-29 07:32:11', 1, 0),
(129, NULL, 'eligibility', 'BS8ZLbfPAJwIf1La92vt0DDrtAZd2SUt', '2021-09-27 13:13:33', '2021-09-27 13:17:07', 1, 0),
(130, NULL, 'eligibility', 'XggkO8iLUjgQnQ7wg5ZnHVEAyVqOKynz', '2021-09-27 13:22:24', '2021-09-27 13:22:24', 0, 0),
(131, NULL, 'eligibility', 'Nq9dizXRBhlKyBJ3uffhH2biYIIp7GM2', '2021-09-27 13:23:37', '2021-09-27 13:23:37', 0, 0),
(132, NULL, 'eligibility', 'dbTvDMvKe4kkNHIuY2RX1O2RQjgpo338', '2021-09-27 14:55:16', '2021-09-27 14:55:16', 0, 0),
(133, NULL, 'eligibility', 'olEjdYLvqXIYR74UeOQLDhFb99hRL13Y', '2021-09-28 09:29:58', '2021-09-28 09:29:58', 0, 0),
(134, NULL, 'eligibility', 'PvgIk8s1LDnk3dBpyQo0b0f6HLe0sNYJ', '2021-09-28 10:06:29', '2021-09-28 10:06:29', 0, 0),
(135, NULL, 'eligibility', '8ukoc7rbkYC5qm6QKobSV8k77YQkTAej', '2021-09-28 10:24:29', '2021-09-28 10:24:29', 0, 0),
(136, NULL, 'eligibility', 'UxFyFtcU18W23zUphbKikJ4ZQU0TV4Zk', '2021-09-28 12:41:49', '2021-09-28 12:41:49', 0, 0),
(137, NULL, 'eligibility', 'JIJdiiOYRpNXCNCbkAFznZseFhNt0Vb8', '2021-09-28 14:52:49', '2021-09-29 13:33:51', 1, 0),
(138, NULL, 'eligibility', '3oclhqVkyR9SWUqC9QZohPUvirZalBqE', '2021-09-28 15:24:58', '2021-09-28 15:24:58', 0, 0),
(139, NULL, 'eligibility', 'XF1xSMqfbV3BxD4DBGqySPkO24wbtYX7', '2021-09-29 09:08:02', '2021-09-29 12:21:51', 1, 0),
(140, NULL, 'eligibility', '4Vp1swBGAjVTVegfazegFCDvGtoiTKCx', '2021-09-29 09:14:42', '2021-09-29 09:14:42', 0, 0),
(141, NULL, 'eligibility', '1eX8tALb2SOYeOZtSCmCvsLB79HOk0zh', '2021-09-29 09:49:38', '2021-09-29 09:49:38', 0, 0),
(142, NULL, 'eligibility', '2r4hjmq1BDKZoOkxBabgUjc3h1cThnT4', '2021-09-29 10:52:30', '2021-09-29 15:30:29', 1, 0),
(143, NULL, 'eligibility', '6oBS9Q6CldKFUvJ6X6T7TsrjvrmRPsyX', '2021-09-29 12:15:24', '2021-09-29 12:15:24', 0, 0),
(144, NULL, 'eligibility', '7xlFM47TAJhQvNff5bwuCf3VbJ7LCOxC', '2021-09-29 13:48:16', '2021-09-29 15:27:04', 1, 0),
(145, NULL, 'eligibility', 'pSAOwsdY68OsmOPSXogClnQicFgf3OkS', '2021-09-29 15:25:13', '2021-09-29 15:25:13', 0, 0),
(146, NULL, 'eligibility', 'Mh2fhhxsv0plCkvPhGYK2l7mtJSOQFAU', '2021-09-29 15:26:28', '2021-09-29 15:26:28', 0, 0),
(147, NULL, 'eligibility', 'jftbXcFXxJenaZSVX41ByOEAJXRcMIqU', '2021-09-29 15:29:43', '2021-09-29 15:29:43', 0, 0),
(148, NULL, 'eligibility', 'Lxn4pqismNQ6Fzqzf8GaVuizXgJkddQz', '2021-09-30 08:27:40', '2021-09-30 08:27:40', 0, 0),
(149, NULL, 'eligibility', 'yDnurJEC2yFysMP2ZG22Ld2cTWcRACPg', '2021-09-30 08:31:23', '2021-09-30 08:31:23', 0, 0),
(150, NULL, 'eligibility', '8y3zf1FyH5lM78jdhybsyJd3lROzVmZ6', '2021-09-30 12:49:14', '2021-09-30 12:49:14', 0, 0),
(151, NULL, 'eligibility', 'T85GcOMQFGBNnn8ipHEJM0tV2cqgDOPZ', '2021-09-30 12:49:33', '2021-09-30 12:49:33', 0, 0),
(152, NULL, 'eligibility', 'uulKcX8LQCy4aszMbjF7Qq3M3xpxDsor', '2021-09-30 12:50:41', '2021-09-30 12:50:41', 0, 0),
(153, NULL, 'eligibility', 'Vuof9Q4txbGCMbShAkGPKzlTG0Ac0IIV', '2021-09-30 14:50:50', '2021-09-30 14:50:50', 0, 0),
(154, NULL, 'eligibility', 'xTC9ElUr1bUHMxtkzDX7A1q7rVRskjZ3', '2021-09-30 14:51:49', '2021-09-30 14:51:49', 0, 0),
(155, NULL, 'eligibility', 'rMrAm262wdG4ExzYu4t0k9Eh5GwnEscd', '2021-09-30 14:57:32', '2021-09-30 14:58:40', 1, 0),
(156, NULL, 'eligibility', 'ru734n2yyMLXMg5CJY8vKTBjjAPSIEOb', '2021-10-01 09:54:19', '2021-10-01 09:54:19', 0, 0),
(157, NULL, 'eligibility', '0rm1htWWeEtChZMcIN7ulTrXQ8K59puo', '2021-10-01 09:55:43', '2021-10-01 09:55:43', 0, 0),
(158, NULL, 'eligibility', 'a4uxmjBoa8uil6r70KtTdqWuiEPPwlMD', '2021-10-01 09:59:32', '2021-10-01 09:59:32', 0, 0),
(159, NULL, 'eligibility', '8YFhmsKsfrhCnI0vMRfRsneB2Mxl7Vza', '2021-10-01 10:06:03', '2021-10-01 10:06:03', 0, 0),
(160, NULL, 'eligibility', 'e7rNiEv4p7MyJzrrEmYyJiOnU5PVATv7', '2021-10-01 10:06:18', '2021-10-01 10:06:18', 0, 0),
(161, NULL, 'eligibility', 'P0AK2ni4TRmDbq79cUT2iYqL4wu15IOl', '2021-10-01 10:07:11', '2021-10-01 10:07:11', 0, 0),
(162, NULL, 'eligibility', 'R20nVbSqChL4gaCVOI9XVU7xNaoxicYl', '2021-10-01 10:08:46', '2021-10-01 10:08:46', 0, 0),
(163, NULL, 'eligibility', 'i0N80hnIrWufsJOGBTwnEqDWtyBYLrtO', '2021-10-01 10:18:58', '2021-10-01 10:18:58', 0, 0),
(164, NULL, 'eligibility', '7QWARCy4Iq9ptn5yKRY5u3kTF9GVjaWj', '2021-10-01 11:22:59', '2021-10-01 11:22:59', 0, 0),
(165, NULL, 'eligibility', 'p5yjcncBJj0FvXkh5Oh34owEcmI7ov6u', '2021-10-01 13:10:11', '2021-10-01 13:10:11', 0, 0),
(166, NULL, 'eligibility', '0e7v2wo0B7ioorXOcPH7MS60kEERQ6HU', '2021-10-04 09:56:24', '2021-10-04 09:56:24', 0, 0),
(167, NULL, 'eligibility', 'U1TFxWvtDU1K0PDkghGs7pbhBN5lRdZh', '2021-10-04 10:06:29', '2021-10-04 10:06:29', 0, 0),
(168, NULL, 'eligibility', 'elcGezIfzab0ZVJffBTNrAHPEd1xOrwU', '2021-10-04 10:33:09', '2021-10-04 10:33:59', 1, 0),
(169, NULL, 'eligibility', 'B5nZGQVcFc4FrM5vnryZuejUSiIZ3z6m', '2021-10-04 10:36:10', '2021-10-04 10:36:10', 0, 0),
(170, NULL, 'eligibility', 'ahKLs0fl5CergXqH3hfyxQltWcE7pQnq', '2021-10-04 10:38:24', '2021-10-04 10:38:24', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activities_categories`
--
ALTER TABLE `activities_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_activities_categories_activities1_idx` (`activities_id`);

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_addresses_users1_idx` (`users_id`),
  ADD KEY `fk_addresses_contacts1_idx` (`contacts_id`),
  ADD KEY `fk_addresses_companies1_idx` (`companies_id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `calendar_events`
--
ALTER TABLE `calendar_events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_calendar_events_users1_idx` (`users_id`),
  ADD KEY `fk_calendar_events_contacts1_idx` (`contacts_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_companies_contacts1_idx` (`contacts_id`),
  ADD KEY `fk_companies_activities1_idx` (`activities_id`),
  ADD KEY `fk_companies_turnovers1_idx` (`turnovers_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_contacts_users1_idx` (`users_id`);

--
-- Indexes for table `counters`
--
ALTER TABLE `counters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_counters_users1_idx` (`users_id`);

--
-- Indexes for table `development`
--
ALTER TABLE `development`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_development_contacts1_idx` (`contacts_id`);

--
-- Indexes for table `eligibility`
--
ALTER TABLE `eligibility`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_eligibility_services_grants1_idx` (`cpn_id`),
  ADD KEY `fk_eligibility_contacts1_idx` (`contacts_id`),
  ADD KEY `fk_eligibility_regions_vouchers1_idx` (`regional_id`);

--
-- Indexes for table `experiences`
--
ALTER TABLE `experiences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investment`
--
ALTER TABLE `investment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_investment_contacts1_idx` (`contacts_id`),
  ADD KEY `fk_investment_transitions1_idx` (`service_id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_meetings_contacts1_idx` (`contacts_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pictures_users1_idx` (`users_id`),
  ADD KEY `fk_pictures_articles1_idx` (`articles_id`);

--
-- Indexes for table `privileges`
--
ALTER TABLE `privileges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_privileges_pages1_idx` (`pages_id`),
  ADD KEY `fk_privileges_levels1_idx` (`levels_id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regions_nafs`
--
ALTER TABLE `regions_nafs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_naf_codes_regions1_idx` (`regions_id`);

--
-- Indexes for table `regions_vouchers`
--
ALTER TABLE `regions_vouchers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vouchers_regions1_idx` (`regions_id`);

--
-- Indexes for table `services_grants`
--
ALTER TABLE `services_grants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_services_grants_transitions1_idx` (`transitions_id`),
  ADD KEY `fk_services_grants_turnovers1_idx` (`turnovers_id`);

--
-- Indexes for table `transitions`
--
ALTER TABLE `transitions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turnovers`
--
ALTER TABLE `turnovers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_turnovers_transitions1_idx` (`transitions_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_levels1_idx` (`levels_id`),
  ADD KEY `fk_users_experiences1_idx` (`experiences_id`);

--
-- Indexes for table `verifications`
--
ALTER TABLE `verifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_verifications_users1_idx` (`users_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `activities_categories`
--
ALTER TABLE `activities_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `calendar_events`
--
ALTER TABLE `calendar_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `counters`
--
ALTER TABLE `counters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=328;

--
-- AUTO_INCREMENT for table `development`
--
ALTER TABLE `development`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `eligibility`
--
ALTER TABLE `eligibility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `investment`
--
ALTER TABLE `investment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `meetings`
--
ALTER TABLE `meetings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `privileges`
--
ALTER TABLE `privileges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `regions_nafs`
--
ALTER TABLE `regions_nafs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=234;

--
-- AUTO_INCREMENT for table `regions_vouchers`
--
ALTER TABLE `regions_vouchers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `services_grants`
--
ALTER TABLE `services_grants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=267;

--
-- AUTO_INCREMENT for table `turnovers`
--
ALTER TABLE `turnovers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `verifications`
--
ALTER TABLE `verifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activities_categories`
--
ALTER TABLE `activities_categories`
  ADD CONSTRAINT `fk_activities_categories_activities1` FOREIGN KEY (`activities_id`) REFERENCES `activities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `calendar_events`
--
ALTER TABLE `calendar_events`
  ADD CONSTRAINT `fk_calendar_events_contacts1` FOREIGN KEY (`contacts_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_calendar_events_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
