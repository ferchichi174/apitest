<?php

namespace App\Http\Controllers\CRM;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CRM\Shared\GlobalController;
use App\Models\Calendar;
use Illuminate\Http\Request;
use App\Http\Resources\CRM\Calendar\CalendarEventsResources;

class CalendarController extends GlobalController
{
    public function getEvents()
    {
        $events = Calendar::get();
        $events = CalendarEventsResources::collection($events);
        return response()->json(["events"=>$events]);
    }

    public function callTester()
    {
        return $this->getAllEvents(3);
    }
    public function getEventsbyuser()
    {

        $events = Calendar::where('users_id', Auth()->user()->id)->get()->first();
        if ($events){
            $d = strval($events->date);
            $s =substr($d,0,10);
            $da = substr(date("20y-j-m"),0,10);

        if (  strcmp ($da,$s) ===0 ){
            return response()->json(["events"=>$events]);
        }
        else {
        //dd(date("20y-j-m"));
        return response()->json(["status"=>false,"message"=>"il n'y a pas de rendez vous aujourdhui"]);}
        }
        else {
            return response()->json(["status"=>false,"message"=>"il n'y a pas de rendez vous"]);
        }
    }



}
