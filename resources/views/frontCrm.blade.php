<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Real Estate Html Template">
    <meta name="author" content="">
    <meta name="generator" content="Jekyll v4.1.1">
  <title>FrontCrm</title>
  <base href="/">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

  <link rel="stylesheet" href="assets/vendors/fontawesome-pro-5/css/all.css">
  <link rel="stylesheet" href="assets/vendors/bootstrap-select/css/bootstrap-select.min.css">
  <link rel="stylesheet" href="assets/vendors/slick/slick.min.css">
  <link rel="stylesheet" href="assets/vendors/magnific-popup/magnific-popup.min.css">
  <link rel="stylesheet" href="assets/vendors/jquery-ui/jquery-ui.min.css">
  <link rel="stylesheet" href="assets/vendors/chartjs/Chart.min.css">
  <link rel="stylesheet" href="assets/vendors/dropzone/css/dropzone.min.css">
  <link rel="stylesheet" href="assets/vendors/animate.css">
  <link rel="stylesheet" href="assets/vendors/timepicker/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="assets/vendors/mapbox-gl/mapbox-gl.min.css">
  <link href="https://cdn.syncfusion.com/ej2/material.css" rel="stylesheet" />
  <link rel="stylesheet" href="assets/css/themes.css">
  <link rel="icon" href="assets/logo2.ico">




<link rel="stylesheet" href="/assets/frontCrm/styles.4154e53e924fd7329071.css"></head>

<body class="loading-overlay-showing" data-plugin-page-transition data-loading-overlay data-plugin-options="{'hideDelay': 500}">
  <app-root></app-root>


  <script src="asset('/vendors/jquery.min.js')"></script>
  <script src="asset('/vendors/bootstrap/bootstrap.bundle.js')"></script>
  <script src="asset('/vendors/slick/slick.min.js')"></script>
  <script src="asset('/vendors/waypoints/jquery.waypoints.min.js')"></script>
  <script src="asset('/vendors/magnific-popup/jquery.magnific-popup.min.js')"></script>

  <script src="asset('/vendors/dropzone/js/dropzone.min.js')"></script>
  <script src="asset('/vendors/mapbox-gl/mapbox-gl.js')"></script>


 
<script src="asset('/frontCrm/runtime.b62e76c708b4b7052402.js')" defer></script>
<script src="asset('/frontCrm/polyfills.a4021de53358bb0fec14.js')" defer>
</script><script src="asset('/frontCrm/main.911f006077fd5e5619a8.js')" defer></script>
</body>

<!-- Mirrored from www.okler.net/previews/porto/8.2.0/index-classic-color.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 29 Dec 2020 12:12:20 GMT -->

</html>